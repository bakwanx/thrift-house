package com.finalproject.thrifthouse.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.finalproject.thrifthouse.model.ProductModelRoom

@Database(entities = [ProductModelRoom::class], version = 1)
abstract class FavoriteProductDatabase: RoomDatabase() {
    abstract fun favoriteDao(): FavoriteDao

    companion object{
        private var INSTANCE: FavoriteProductDatabase? = null

        fun getInstance(context: Context): FavoriteProductDatabase? {
            if (INSTANCE == null){
                synchronized(FavoriteProductDatabase::class){
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        FavoriteProductDatabase::class.java,
                        "FavoriteProduct"
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }
}