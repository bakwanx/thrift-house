package com.finalproject.thrifthouse.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.finalproject.thrifthouse.model.ProductModelRoom

@Dao
interface FavoriteDao {
    @Insert(onConflict = REPLACE)
    fun addFavoriteProduct(productModelRoom: ProductModelRoom): Long

    @Query("SELECT * FROM ProductModelRoom")
    fun getAllFavoriteProduct(): List<ProductModelRoom>

    @Query("DELETE FROM ProductModelRoom WHERE id = :id")
    fun deleteFavoriteProduct(id: String)
}