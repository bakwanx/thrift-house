package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.databinding.ActivityDetailProductBinding
import com.finalproject.thrifthouse.databinding.ActivityLoginBinding
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.DetailProductActivityViewModel
import com.finalproject.thrifthouse.viewmodel.LoginActivityViewModel
import org.koin.android.ext.android.bind
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val loginViewModel: LoginActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {

            tvLoginBackNavigation.setOnClickListener {
                finish()
            }

            tvRegist.setOnClickListener {
                val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                startActivity(intent)
            }

            btnLogin.setOnClickListener {
                loginHandle()
            }
        }
    }

    private fun loginHandle(){
        binding.apply {
            pbLogin.visibility = View.VISIBLE
            val username = etUsernameEmail.text.toString()
            val password = etPassword.text.toString()

            loginViewModel.login(username, password)

            loginViewModel.getIsLogin.observe(this@LoginActivity){
                pbLogin.visibility = View.GONE
                if (it){
                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                    ToastMessage.showToast(this@LoginActivity,getString(R.string.welcome_toast))
                }else{
                    ToastMessage.showToast(this@LoginActivity,getString(R.string.username_and_password_wrong_toast))
                }
            }
        }

    }
}