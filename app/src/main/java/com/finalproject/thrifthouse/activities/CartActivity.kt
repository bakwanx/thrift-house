package com.finalproject.thrifthouse.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.CartAdapter
import com.finalproject.thrifthouse.adapter.CityAdapter
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.api.response.StoreCart
import com.finalproject.thrifthouse.databinding.ActivityCartBinding
import com.finalproject.thrifthouse.model.CartModel

import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.viewmodel.CartActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable


class CartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCartBinding
    private val cartViewModel: CartActivityViewModel by viewModel()
    private lateinit var cartAdapter: CartAdapter
    private val productIds: ArrayList<String> = ArrayList()
    private var productCart: List<ProductCart> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        initData()
        initAction()
        initObserver()

    }

    private fun initData(){
        cartViewModel.getCart()
        binding.pbCart.visibility = View.VISIBLE
    }

    private fun init(){
        cartAdapter = CartAdapter(object: CartAdapter.Listener{
            override fun selectedStore(cartModel: CartModel) {
                binding.ivTrash.visibility = View.VISIBLE
                binding.btnCheckout.setBackgroundResource(R.drawable.custom_button)
                cartModel.product.forEach {
                    cartViewModel.removeSelected(it)
                    cartViewModel.setSelected(it)
                }
            }

            override fun unselectedStore(cartModel: CartModel) {
                binding.ivTrash.visibility = View.GONE
                binding.btnCheckout.setBackgroundResource(R.drawable.custom_button_default)
                binding.cbSelectAll.isChecked = false
                cartModel.product.forEach {
                    cartViewModel.removeSelected(it)
                }
            }

            override fun selectedProduct(cartModel: CartModel, productCart: ProductCart) {
                binding.ivTrash.visibility = View.VISIBLE
                binding.btnCheckout.setBackgroundResource(R.drawable.custom_button)
                cartViewModel.setSelected(productCart)
            }

            override fun unselectedProduct(productCart: ProductCart) {
                binding.ivTrash.visibility = View.GONE
                binding.btnCheckout.setBackgroundResource(R.drawable.custom_button_default)
                binding.cbSelectAll.isChecked = false
                cartViewModel.removeSelected(productCart)
            }
        })
        binding.rvCartStore.adapter = cartAdapter
    }

    private fun initObserver(){
        binding.apply {
            cartViewModel.getCartModel.observe(this@CartActivity){
                binding.pbCart.visibility = View.GONE
                cartAdapter.setItem(it)
                cartAdapter.notifyDataSetChanged()
            }
            cartViewModel.getIsDelete.observe(this@CartActivity){
                binding.pbCart.visibility = View.GONE
                if(it){
                    initData()
                    cartAdapter.notifyDataSetChanged()
                }
            }
            cartViewModel.getTotal.observe(this@CartActivity){
                tvPcs.text = "${it.size.toString()} pcs"
                var total = 0
                for (i in it.indices){
                    productIds.add(it[i].id)
                    total += it[i].price.toInt()
                }
                tvNominal.text = NumberConversion.conversionToRupiah(total.toDouble())
                productCart = it
            }
        }
    }
    
    private fun initAction(){
        binding.apply {
            btnCheckout.setOnClickListener {
                if(productCart.isNotEmpty()){
                    val intent = Intent(this@CartActivity, CheckoutActivity::class.java)
                    intent.putExtra(KEY_CART, productCart as Serializable)
                    startActivity(intent)
                }
            }
            ivTrash.setOnClickListener {
                cartViewModel.deleteCart(productIds)
            }
            ivNavigationBackCart.setOnClickListener {
                finish()
            }
        }
    }

    fun isSelectAll(view: View) {
        if (binding.cbSelectAll.isChecked) {
            cartAdapter.setAllChecked(true)
            cartAdapter.notifyDataSetChanged()
        }else{
            cartAdapter.setAllChecked(false)
            cartAdapter.notifyDataSetChanged()
        }
    }

    companion object{
        val KEY_CART = "key cart"
    }
}