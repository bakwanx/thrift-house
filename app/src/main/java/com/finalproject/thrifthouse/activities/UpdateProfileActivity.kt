package com.finalproject.thrifthouse.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.finalproject.thrifthouse.activities.BiodataActivity.Companion.KEY_UPDATE_PROFILE
import com.finalproject.thrifthouse.databinding.ActivityBiodataBinding
import com.finalproject.thrifthouse.databinding.ActivityUpdateProfileBinding

class UpdateProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUpdateProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val getIntent = intent.extras?.getString(KEY_UPDATE_PROFILE)

        if(getIntent.equals("username")){
            updateUsername()
        }else if(getIntent.equals("email")){
            updateEmail()
        }else if(getIntent.equals("phone")){
            updatePhone()
        }else if(getIntent.equals("gender")){
            updateGender()
        }else if(getIntent.equals("birth")){
            updateBirth()
        }
    }

    private fun updateUsername(){
        binding.apply {

        }
    }

    private fun updateEmail(){
        binding.apply {

        }
    }

    private fun updatePhone(){
        binding.apply {

        }
    }

    private fun updateGender(){
        binding.apply {

        }
    }

    private fun updateBirth(){
        binding.apply {

        }
    }
}