package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.finalproject.thrifthouse.adapter.CartAdapter
import com.finalproject.thrifthouse.adapter.CityAdapter
import com.finalproject.thrifthouse.adapter.CityAdapter.Companion.KEY_ADDRESS
import com.finalproject.thrifthouse.adapter.ProvinceAdapter
import com.finalproject.thrifthouse.api.response.City
import com.finalproject.thrifthouse.api.response.Province
import com.finalproject.thrifthouse.api.response.ResultProvinceCity
import com.finalproject.thrifthouse.databinding.ActivityAddProvinceCityBinding
import com.finalproject.thrifthouse.viewmodel.AddressViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddProvinceCityActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddProvinceCityBinding
    private lateinit var provinceAdapter: ProvinceAdapter
    private lateinit var cityAdapter: CityAdapter
    private val addressViewModel: AddressViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddProvinceCityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        fetchProvince()
        initObserver()
        initAction()

    }

    private fun initAction(){
        binding.ivAddProvinceCityBackNavigation.setOnClickListener {
            finish()
        }
    }

    private fun initObserver() {
        binding.apply {
            addressViewModel.getProvince.observe(this@AddProvinceCityActivity) {
                provinceAdapter.setProvince(it.rajaongkir.results)
                provinceAdapter.notifyDataSetChanged()
            }

            addressViewModel.getCities.observe(this@AddProvinceCityActivity) {
                cityAdapter.setCity(it.rajaongkir.results)
                cityAdapter.notifyDataSetChanged()
            }

            addressViewModel.getProvinceSelected.observe(this@AddProvinceCityActivity) {
                if (it != null) {
                    llOne.visibility = View.VISIBLE
                    rvCity.visibility = View.VISIBLE
                    tvProvince.text = it.province
                    rvProvince.visibility = View.GONE
                    fetchCity(it.province_id)
                } else {
                    llOne.visibility = View.GONE
                    rvCity.visibility = View.GONE
                }
            }

            addressViewModel.getCitySelected.observe(this@AddProvinceCityActivity) {
                tvCity.text = it.city_name
            }
        }
    }

    private fun init() {
        provinceAdapter = ProvinceAdapter(object : ProvinceAdapter.Listener {
            override fun selectedProvince(province: Province) {
                addressViewModel.setProvince(province)
            }
        })
        cityAdapter = CityAdapter(object : CityAdapter.Listener {
            override fun selectedCity(city: City) {
                addressViewModel.setCity(city)
                val resultProvinceCity = ResultProvinceCity(
                    city_id = city.city_id,
                    province_id = city.province_id,
                    province = city.province,
                    type = city.type,
                    city_name = city.city_name,
                    postal_code = city.postal_code
                )
                val intent = Intent(this@AddProvinceCityActivity, AddAddressActivity::class.java)

                intent.putExtra(KEY_ADDRESS, resultProvinceCity)
                setResult(1, intent)
                finish()
            }
        })
        binding.rvProvince.adapter = provinceAdapter
        binding.rvCity.adapter = cityAdapter
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    private fun fetchProvince() {
        addressViewModel.getProvince()
    }

    private fun fetchCity(provinceId: String) {
        addressViewModel.getCities(provinceId)
    }
}