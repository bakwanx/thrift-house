package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.CityAdapter
import com.finalproject.thrifthouse.api.response.City
import com.finalproject.thrifthouse.api.response.PostAddress
import com.finalproject.thrifthouse.api.response.ResultProvinceCity
import com.finalproject.thrifthouse.databinding.ActivityAddAddressBinding
import com.finalproject.thrifthouse.databinding.ActivityPaymentGuideBinding
import com.finalproject.thrifthouse.databinding.ActivityRegisterBinding
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.AddAddressActivityViewModel
import com.finalproject.thrifthouse.viewmodel.AddressViewModel
import com.finalproject.thrifthouse.viewmodel.StoreFavoriteFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class AddAddressActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddAddressBinding
    private val addAddressViewModel: AddAddressActivityViewModel by viewModel()
    private lateinit var resultProvinceCity: ResultProvinceCity
    private var checkMainAddress = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddAddressBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAction()

    }

    private fun initAction() {
        binding.apply {

            switchAddAddressSetToMain.setOnCheckedChangeListener { buttonView, isChecked ->
                checkMainAddress = isChecked
            }

            rlOne.setOnClickListener {
                val intent = Intent(this@AddAddressActivity, AddProvinceCityActivity::class.java)
                startActivityForResult(intent, 1)
            }

            btnAddAddressSaveAddress.setOnClickListener {

                val fullname = etAddAddressFullname.text.toString()
                val phone = etAddAddressPhone.text.toString()
                val fullAddress = etAddFullAddress.text.toString()
                val distric = etAddAddressDistrict.text.toString()
                val village = etAddAddressVillage.text.toString()
                val zipCode = etAddAddressZipCode.text.toString()
                val detail = etAddAddressMoreDetail.text.toString()

                val postAddress = PostAddress(
                    addressLabel = checkMainAddress,
                    city = resultProvinceCity.city_name.toString(),
                    detail = detail,
                    district = distric,
                    fullAddress = fullAddress,
                    idCity = resultProvinceCity.city_id?.toLong() as Long,
                    idProvince = resultProvinceCity.province_id?.toLong() as Long,
                    postalCode = zipCode,
                    province = resultProvinceCity.province.toString(),
                    recipientName = fullname,
                    recipientPhone = phone,
                    village = village
                )
                addAddressViewModel.postAddress(
                    postAddress
                )
                isSuccess()
            }

            ivAddAddressBackNavigation.setOnClickListener {
                finish()
            }
        }
    }

    private fun isSuccess() {
        addAddressViewModel.isSuccess.observe(this@AddAddressActivity) {
            if (it) {
                intent.putExtra(AddressActivity.KEY_ADD_ADDRESS, true)
                setResult(1, intent)
                finish()
            } else {
                ToastMessage.showToast(this@AddAddressActivity, "Gagal menambahkan alamat")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1 && data != null) {
            resultProvinceCity =
                data.getSerializableExtra(CityAdapter.KEY_ADDRESS) as ResultProvinceCity
            binding.tvAddAddressProvinceCity.text =
                "${resultProvinceCity?.province}, ${resultProvinceCity?.city_name}"
        }
    }

}