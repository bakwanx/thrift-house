package com.finalproject.thrifthouse.activities

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.DetailProductActivity.Companion.KEY_IMAGE
import com.finalproject.thrifthouse.adapter.ProductAdapter
import com.finalproject.thrifthouse.databinding.ActivityImagePreviewBinding
import com.finalproject.thrifthouse.databinding.ActivityProductBinding

class ImagePreviewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityImagePreviewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImagePreviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val image:String = intent.getStringExtra(KEY_IMAGE) as String

        var uriImage = Uri.parse(image)
        Glide.with(this)
            .load(image)
            .into(binding.pvProduct);
    }
}