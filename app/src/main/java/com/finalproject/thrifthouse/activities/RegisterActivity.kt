package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.View
import android.widget.Toast
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.databinding.ActivityRegisterBinding
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.RegisterActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private val registerViewModel: RegisterActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //format phone number input to xxxx-xxxx-xxxx
//        binding.etRegisterPhoneNumber.addTextChangedListener(PhoneNumberFormattingTextWatcher())

        binding.apply {
            tvRegisterLogin.setOnClickListener {
                val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                startActivity(intent)
            }
            tvRegisterBackNavigation.setOnClickListener {
                finish()
            }
            btnRegister.setOnClickListener {
               registerHandle()
            }
        }
    }

    private fun registerHandle(){
        binding.apply {
            pbRegister.visibility = View.VISIBLE
            val username: String = etRegisterUsername.text.toString().trim()
            val email: String = etRegisterEmail.text.toString().trim()
            val phoneNumber: String = etRegisterPhoneNumber.text.toString().trim()
            val password: String = etRegisterInputPassword.text.toString()
            val termCondition = cbRegisterTermConditions.isChecked
            if (validateTextInput(username, email, phoneNumber, password, termCondition)) {
                registerViewModel.register(
                    username = username,
                    email = email,
                    password = password,
                    phone = phoneNumber
                )
                registerViewModel.getIsRegister.observe(this@RegisterActivity){
                    pbRegister.visibility = View.GONE
                    if (it){
                        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                        ToastMessage.showToast(this@RegisterActivity,getString(R.string.welcome_toast))
                    }else{
                        ToastMessage.showToast(this@RegisterActivity,getString(R.string.username_registered_toast))
                    }
                }
            }else{
                pbRegister.visibility = View.GONE
            }
        }
    }

    private fun validateTextInput(
        username: String,
        email: String,
        phoneNumber: String,
        password: String,
        termCondition: Boolean
    ): Boolean {
        return when {
            username.isEmpty() -> {
                Toast.makeText(this, "Username kosong", Toast.LENGTH_SHORT).show()
                binding.etRegisterUsername.requestFocus()
                false
            }
            email.isEmpty() -> {
                Toast.makeText(this, "Email kosong", Toast.LENGTH_SHORT).show()
                binding.etRegisterEmail.requestFocus()
                false
            }
            phoneNumber.isEmpty() -> {
                Toast.makeText(this, "Nomor Hp kosong", Toast.LENGTH_SHORT).show()
                binding.etRegisterPhoneNumber.requestFocus()
                false
            }
            password.isEmpty() -> {
                Toast.makeText(this, "Password kosong", Toast.LENGTH_SHORT).show()
                binding.etRegisterPassword.requestFocus()
                false
            }
            !termCondition -> {
                Toast.makeText(this, "Setujui syarat dan ketentuan", Toast.LENGTH_SHORT).show()
                binding.cbRegisterTermConditions.requestFocus()
                false
            }
            else -> {
                true
            }
        }
    }

    
}

