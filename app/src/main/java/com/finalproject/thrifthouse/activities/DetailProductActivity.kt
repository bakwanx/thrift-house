package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.ImageProductDetailAdapter
import com.finalproject.thrifthouse.adapter.ProductAdapter.Companion.KEY_ID_PRODUCT
import com.finalproject.thrifthouse.adapter.ProductStoreAdapter
import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.databinding.ActivityDetailProductBinding
import com.finalproject.thrifthouse.fragment.dialog.DialogInfoDetailProduct
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.DetailProductActivityViewModel
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable

class DetailProductActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailProductBinding
    private val productViewModel: DetailProductActivityViewModel by viewModel()
    private lateinit var imageProductDetailAdapter: ImageProductDetailAdapter
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    private lateinit var productStoreAdapter: ProductStoreAdapter
    private lateinit var storeId: String
    private lateinit var productId: String
    private var isFavorite = false
    private var productCarts: ArrayList<ProductCart> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        fetchDetailProduct()
        initObserver()
        initAction()

    }

    private fun init() {
        imageProductDetailAdapter = ImageProductDetailAdapter()
        productStoreAdapter = ProductStoreAdapter("horizontal")
    }

    private fun initObserver() {
        binding.apply {
            productViewModel.getDetailProduct.observe(this@DetailProductActivity) {
                binding.shimmerDetailProduct.visibility = View.GONE
                binding.llOne.visibility = View.VISIBLE
                setProduct(it)
                setStore(it)
                storeId = it.data.store.id
                productId = it.data.product.id
            }

            productViewModel.getIdStore.observe(this@DetailProductActivity) {
                fetchStoreProduct(it)
            }

            productViewModel.getStoreProducts.observe(this@DetailProductActivity) {
                productStoreAdapter.setItem(favoriteViewModel, it)
                //fetchRecomendationProduct(it.get(0))
                binding.rvStoreProducts.adapter = productStoreAdapter
                productStoreAdapter.notifyDataSetChanged()
            }

            productViewModel.getRecomendationProducts.observe(this@DetailProductActivity) {
                productStoreAdapter.setItem(favoriteViewModel, it)
                binding.rvRecomendationProducts.adapter = productStoreAdapter
                productStoreAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun initAction() {
        binding.apply {
            val dialogInfo = DialogInfoDetailProduct()

            ivInfoProductDetail.setOnClickListener {
                dialogInfo.show(supportFragmentManager, null)
            }

            btnStoreDetail.setOnClickListener {
                if (storeId.isNotEmpty()) {
                    val intent = Intent(this@DetailProductActivity, DetailStoreActivity::class.java)
                    intent.putExtra(KEY_STORE, storeId)
                    startActivity(intent)
                }

            }

            ivBack.setOnClickListener {
                finish()
            }

            ivFavorite.setOnClickListener {
                if (CheckAuth().checkAuth(application)) {
                    productViewModel.postProductFavorite(productId)
                    isFavorite = !isFavorite
                    if (isFavorite) {
                        ivFavorite.setImageResource(R.drawable.ic_favorite_checked)
                    } else {
                        ivFavorite.setImageResource(R.drawable.ic_favorite)
                    }
                } else {
                    val intent = Intent(this@DetailProductActivity, LoginActivity::class.java)
                    startActivity(intent)
                }
            }

            ivCart.setOnClickListener {
                if (CheckAuth().checkAuth(application)) {
                    productViewModel.postProductCard(productId)
                    ToastMessage.showToast(
                        this@DetailProductActivity,
                        "Berhasil menambah ke keranjang"
                    )
                } else {
                    val intent = Intent(this@DetailProductActivity, LoginActivity::class.java)
                    startActivity(intent)
                }
            }

            btnAddToCart.setOnClickListener {
                if (CheckAuth().checkAuth(application)) {
                    productViewModel.postProductCard(productId)
                    ToastMessage.showToast(
                        this@DetailProductActivity,
                        "Berhasil menambah ke keranjang"
                    )
                    val intent = Intent(this@DetailProductActivity, CartActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this@DetailProductActivity, LoginActivity::class.java)
                    startActivity(intent)
                }
            }

            btnCheckout.setOnClickListener {
                if (productCarts.isNotEmpty()) {
                    val intent = Intent(this@DetailProductActivity, CheckoutActivity::class.java)
                    intent.putExtra(CartActivity.KEY_CART, productCarts as Serializable)
                    startActivity(intent)
                }
            }
        }
    }

    private fun setStore(productDetailResponse: ProductDetailResponse) {
        val nameStore = productDetailResponse.data.store.name
        val provinceStore = productDetailResponse.data.store.province
        val cityStore = productDetailResponse.data.store.city
        val photoStore = productDetailResponse.data.store.logo
        val totalProduct = productDetailResponse.data.store.totalProduct
        val totalReview = productDetailResponse.data.store.totalReview
        val averageReview = productDetailResponse.data.store.averageReview
        val favoriteStore = productDetailResponse.data.store.favoriteStore

        binding.apply {
            tvStoreName.text = nameStore
            val address = "$cityStore, $provinceStore"
            Log.d("TAG", "address: $address")
            tvStoreAddress.text = address
            tvTotalProduct.text = totalProduct.toString()
            tvTotalReview.text = totalReview.toString()
            tvAverageReview.text = averageReview.toString()
            tvFavoriteStore.text = favoriteStore.toString()
            Glide
                .with(this@DetailProductActivity)
                .load(photoStore)
                .centerCrop()
                .into(ivStore)
        }
    }

    private fun setProduct(productDetailResponse: ProductDetailResponse) {
        //Mapping
        productCarts.add(
            ProductCart(
                id = productDetailResponse.data.product.id,
                name = productDetailResponse.data.product.name,
                brand = productDetailResponse.data.product.brand,
                size = productDetailResponse.data.product.size,
                condition = productDetailResponse.data.product.condition,
                photo = productDetailResponse.data.product.photos.get(0),
                price = productDetailResponse.data.product.price.toLong(),
                weight = productDetailResponse.data.product.height,
                store = StoreCart(
                    id = productDetailResponse.data.store.id,
                    name = productDetailResponse.data.store.name,
                    province = productDetailResponse.data.store.province,
                    province_id = productDetailResponse.data.store.province_id,
                    city = productDetailResponse.data.store.city,
                    city_id = productDetailResponse.data.store.city_id,
                    photo = productDetailResponse.data.store.logo,
                ),
            )
        )

        binding.apply {
            val nameProduct = productDetailResponse.data.product.name
            val brandProduct = productDetailResponse.data.product.brand
            val sizeProduct = productDetailResponse.data.product.size
            val priceProduct = productDetailResponse.data.product.price
            val conditionProduct = productDetailResponse.data.product.condition
            val heightProduct = productDetailResponse.data.product.height
            val widthProduct = productDetailResponse.data.product.width
            val materialProduct = productDetailResponse.data.product.material
            val descriptionProduct = productDetailResponse.data.product.description
            val listPhoto = productDetailResponse.data.product.photos

            tvNameProduct.text = nameProduct
            tvBrandProduct.text = brandProduct
            tvSizeProduct.text = sizeProduct
            tvPriceProduct.text = NumberConversion.conversionToRupiah(priceProduct)
            tvConditionProduct.text = conditionProduct
            tvConditionProductTwo.text = conditionProduct
            tvHeightProduct.text = heightProduct.toString()
            tvWidthProduct.text = widthProduct.toString()
            tvMaterialProduct.text = materialProduct
            tvDescriptionProduct.text = descriptionProduct

            val layoutManager = LinearLayoutManager(
                this@DetailProductActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            rvImageProduct.layoutManager = layoutManager
            imageProductDetailAdapter.setItem(listPhoto)
            imageProductDetailAdapter.notifyDataSetChanged()
            rvImageProduct.adapter = imageProductDetailAdapter

            setImage(listPhoto[0])
        }

    }

    fun setImage(image: String) {
        Glide
            .with(this@DetailProductActivity)
            .load(image)
            .centerCrop()
            .into(binding.ivProduct)

        binding.ivProduct.setOnClickListener {
            val intent = Intent(this, ImagePreviewActivity::class.java)
            intent.putExtra(KEY_IMAGE, image)
            startActivity(intent)
        }
    }

    private fun fetchDetailProduct() {
        binding.shimmerDetailProduct.visibility = View.VISIBLE
        binding.llOne.visibility = View.GONE

        val idProduct = intent.getSerializableExtra(KEY_ID_PRODUCT) as String
        productViewModel.getDetailProduct(idProduct)
    }

    private fun fetchStoreProduct(idStore: String) {
        productViewModel.getStoreProducts(idStore)
    }

    private fun fetchRecomendationProduct(productModel: ProductModel) {
        productViewModel.getRecomendationProducts(category = productModel.category)
    }

    companion object {
        val KEY_IMAGE = "key_image"
        val KEY_STORE = "key_id_store"
    }

}