package com.finalproject.thrifthouse.activities

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.StoreCategoryAdapter
import com.finalproject.thrifthouse.adapter.StoreMenuAdapter
import com.finalproject.thrifthouse.api.response.Store
import com.finalproject.thrifthouse.databinding.ActivityDetailProductBinding
import com.finalproject.thrifthouse.databinding.ActivityDetailStoreBinding
import com.finalproject.thrifthouse.model.MenuStoreModel
import com.finalproject.thrifthouse.utilities.DataDummy
import com.finalproject.thrifthouse.viewmodel.DetailStoreActivityViewModel
import com.finalproject.thrifthouse.viewmodel.ProductActivityViewModel
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailStoreActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailStoreBinding
    private val storeViewModel: DetailStoreActivityViewModel by viewModel()
    private lateinit var storeMenuAdapter: StoreMenuAdapter
    private var idStore = ""
    private var isClickFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailStoreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initData()
        init()
        fetchStore(idStore)
        initObserver()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            initAction()
        }

    }

    private fun init() {
        binding.apply {
            emptyDataVisibility()
            vpCategoryStore.adapter = StoreCategoryAdapter(idStore, this@DetailStoreActivity)
            TabLayoutMediator(tabCategoryStore, vpCategoryStore) { tab, position ->
                when (position) {
                    0 -> tab.text = "Semua Produk"
                    1 -> tab.text = "Pria"
                    2 -> tab.text = "Wanita"
                    3 -> tab.text = "Anak-anak"
                    4 -> tab.text = "Ulasan"
                    else -> tab.text = "Tentang Toko"
                }
            }.attach()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initAction() {
        binding.apply {
            cvFavoriteStore.setOnClickListener {
                storeViewModel.postFavoriteStore(idStore)
                if (!isClickFavorite) {
                    cvFavoriteStore.backgroundTintList = getColorStateList(R.color.white)
                    tvFavoriteStore.setTextColor(resources.getColor(R.color.green_500))
                    tvFavoriteStore.text = "Hapus Favorit"
                    isClickFavorite = !isClickFavorite
                } else {
                    cvFavoriteStore.backgroundTintList = getColorStateList(R.color.green_500)
                    tvFavoriteStore.setTextColor(resources.getColor(R.color.white))
                    tvFavoriteStore.text = "Jadikan Favorit"
                    isClickFavorite = !isClickFavorite
                }

            }
            ivBack.setOnClickListener {
                finish()
            }
        }

    }

    private fun initData() {
        idStore = intent.getSerializableExtra(DetailProductActivity.KEY_STORE) as String
    }

    private fun initObserver() {
        binding.apply {
            storeViewModel.getDetailStore.observe(this@DetailStoreActivity) {
                onLoadDataVisibility()
                setBanner(idStore)
                setData(it.data)
                setMenu(it.data)
            }

            storeViewModel.getBanner.observe(this@DetailStoreActivity) {
                Glide
                    .with(this@DetailStoreActivity)
                    .load(it.data.banner)
                    .centerCrop()
                    .into(ivBackgroundStore)
            }
        }
    }


    private fun emptyDataVisibility() {
        binding.apply {
            shimmerDetailStore.visibility = View.VISIBLE
            shimmerDetailStore.startShimmer()
            llOne.visibility = View.GONE
            appBarOutlet.visibility = View.GONE
        }
    }

    private fun onLoadDataVisibility() {
        binding.apply {
            shimmerDetailStore.visibility = View.GONE
            shimmerDetailStore.stopShimmer()
            llOne.visibility = View.VISIBLE
            appBarOutlet.visibility = View.VISIBLE
        }
    }

    fun setMenu(store: Store) {
        val dataMenuStore = DataDummy().dataMenuStore
        val dataIcMenuStore = DataDummy().dataIcMenuStore
        val listMenu =
            listOf(store.totalReview, store.averageReview, store.totalProduct, store.favoriteStore)
        val menuStoreModels: ArrayList<MenuStoreModel> = ArrayList()
        for (i in dataMenuStore.indices) {
            val menuStoreModel: MenuStoreModel = MenuStoreModel(
                icImg = dataIcMenuStore[i],
                desc = dataMenuStore[i],
                number = listMenu[i].toString()
            )
            menuStoreModels.add(menuStoreModel)
        }
        storeMenuAdapter = StoreMenuAdapter(menuStoreModels)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.rvMenu.layoutManager = layoutManager
        binding.rvMenu.adapter = storeMenuAdapter
        // storeMenuAdapter = StoreMenuAdapter()
    }

    private fun fetchStore(idStore: String) {
        storeViewModel.getDetailStore(idStore)

    }

    private fun setBanner(idStore: String) {
        storeViewModel.getBanner(idStore)
    }

    private fun setData(store: Store) {
        binding.apply {
            tvStoreName.text = store.name
            tvStoreAddress.text = "${store.city}, ${store.province}"
            Glide
                .with(this@DetailStoreActivity)
                .load(store.logo)
                .centerCrop()
                .into(binding.ivStore)
        }
    }
}