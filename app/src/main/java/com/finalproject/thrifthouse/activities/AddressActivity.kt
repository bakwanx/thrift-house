package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.finalproject.thrifthouse.activities.CheckoutActivity.Companion.KEY_DEFAULT_ADDRESS
import com.finalproject.thrifthouse.adapter.AddressAdapter
import com.finalproject.thrifthouse.adapter.CityAdapter
import com.finalproject.thrifthouse.api.response.Address
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.api.response.ResultProvinceCity
import com.finalproject.thrifthouse.databinding.ActivityAddressBinding
import com.finalproject.thrifthouse.fragment.AccountFragment
import com.finalproject.thrifthouse.viewmodel.AddressActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable

class AddressActivity : AppCompatActivity() {
    private var _binding: ActivityAddressBinding? = null
    private val binding get() = _binding!!
    private lateinit var addressAdapter: AddressAdapter
    private val addresViewModel: AddressActivityViewModel by viewModel()
//    private var tempProductCart: List<ProductCart>? = ArrayList()
    private var fromCheckout: String? = ""
    private var fromMainActivity: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityAddressBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initData()
        init()
        initObserver()

    }

    private fun initData(){
        fromCheckout = intent.getStringExtra(CheckoutActivity.KEY_FROM)
        fromMainActivity = intent.getStringExtra(AccountFragment.KEY_FROM)
    }

    private fun init(){
        addressAdapter = AddressAdapter(object: AddressAdapter.Listener{
            override fun selectedAddress(address: Address) {
                if(fromCheckout.equals("checkout")){
                    val intent = Intent(this@AddressActivity, CartActivity::class.java)
                    startActivity(intent)
                    finish()
                }else if(fromMainActivity.equals("account")){
                    val intent = Intent(this@AddressActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }else{
                    val intent = Intent(this@AddressActivity, CheckoutActivity::class.java)
                    intent.putExtra(KEY_DEFAULT_ADDRESS, address)
                    setResult(1, intent)
                    finish()
                }

            }
        })
        binding.rvAddress.adapter = addressAdapter
        addresViewModel.getListAddress()
    }

    private fun initObserver(){
        binding.apply {
            ivAddAddressArrow.setOnClickListener {
                intent = Intent(this@AddressActivity, AddAddressActivity::class.java)
                startActivityForResult(intent, 1)
            }

            addresViewModel.getListAddress.observe(this@AddressActivity){
                addressAdapter.setItem(it.data)
                addressAdapter.notifyDataSetChanged()
            }

            ivAddressBackNavigation.setOnClickListener {
                finish()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == 1 && data != null){
            addresViewModel.getListAddress()
        }
    }

    companion object {
        val KEY_ADD_ADDRESS = "add key address"
    }
}