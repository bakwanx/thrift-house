package com.finalproject.thrifthouse.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.databinding.ActivityMainBinding
import com.finalproject.thrifthouse.fragment.AccountFragment
import com.finalproject.thrifthouse.fragment.FavoriteFragment
import com.finalproject.thrifthouse.fragment.HomeFragment
import com.finalproject.thrifthouse.fragment.NotificationFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.navigation_home -> {
                loadFragment(HomeFragment())
                return@OnNavigationItemSelectedListener  true
            }
            R.id.navigation_favorite -> {
                loadFragment(FavoriteFragment())
                return@OnNavigationItemSelectedListener  true
            }
            R.id.navigation_notification -> {
                loadFragment(NotificationFragment())
                return@OnNavigationItemSelectedListener  true
            }
            R.id.navigation_account -> {
                loadFragment(AccountFragment())
                return@OnNavigationItemSelectedListener  true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding
            .bottomNavigationMain
            .setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        loadFragment(HomeFragment())
    }

    private fun loadFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fl_main, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun finishActivity(){
        finish()
    }
}