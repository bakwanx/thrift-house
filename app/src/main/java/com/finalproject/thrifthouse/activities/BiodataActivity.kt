package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.databinding.ActivityBiodataBinding
import com.finalproject.thrifthouse.databinding.ActivityMainBinding
import com.finalproject.thrifthouse.viewmodel.AccountFragmentViewModel
import com.finalproject.thrifthouse.viewmodel.BiodataActivityViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class BiodataActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBiodataBinding
    private val biodataActivityViewModel: BiodataActivityViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBiodataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        biodataActivityViewModel.getUserDetail()

        binding.apply {
            biodataActivityViewModel.getUserDetail.observe(this@BiodataActivity) {
                Glide
                    .with(this@BiodataActivity)
                    .load(it.data.profileImg)
                    .centerCrop()
                    .into(ivAccountProfile)

                tvUsername.text = it.data.username
                tvEmail.text = it.data.email
                tvPhone.text = it.data.phone
                if(it.data.gender.isNullOrBlank() || it.data.birth.isNullOrBlank()){
                    tvGender.text = "Tidak tersedia"
                    tvBirth.text = "Tidak tersedia"
                }else{
                    tvGender.text = it.data.gender
                    tvBirth.text = it.data.birth
                }
            }

            rlUsername.setOnClickListener {
                val intent = Intent(this@BiodataActivity, UpdateProfileActivity::class.java)
                intent.putExtra(KEY_UPDATE_PROFILE, "username")
            }

            rlEmail.setOnClickListener {
                val intent = Intent(this@BiodataActivity, UpdateProfileActivity::class.java)
                intent.putExtra(KEY_UPDATE_PROFILE, "email")
            }

            rlPhone.setOnClickListener {
                val intent = Intent(this@BiodataActivity, UpdateProfileActivity::class.java)
                intent.putExtra(KEY_UPDATE_PROFILE, "phone")
            }

            rlGender.setOnClickListener {
                val intent = Intent(this@BiodataActivity, UpdateProfileActivity::class.java)
                intent.putExtra(KEY_UPDATE_PROFILE, "gender")
            }

            rlBirth.setOnClickListener {
                val intent = Intent(this@BiodataActivity, UpdateProfileActivity::class.java)
                intent.putExtra(KEY_UPDATE_PROFILE, "birth")
            }

            ivBack.setOnClickListener {
                finish()
            }
        }
    }

    companion object{
        val KEY_UPDATE_PROFILE = " key update profile"
    }
}