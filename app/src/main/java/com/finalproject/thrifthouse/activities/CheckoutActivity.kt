package com.finalproject.thrifthouse.activities

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.finalproject.thrifthouse.adapter.BankAdapter
import com.finalproject.thrifthouse.adapter.CheckoutAdapter
import com.finalproject.thrifthouse.adapter.DeliveryServiceAdapter
import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.databinding.ActivityCheckoutBinding
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.model.CheckOutModel
import com.finalproject.thrifthouse.utilities.GroupingCart
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.CheckoutActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable
import java.util.logging.Logger

class CheckoutActivity : AppCompatActivity() {

    private var _binding: ActivityCheckoutBinding? = null
    private val binding get() = _binding!!
    private lateinit var address: Address
    private lateinit var checkoutAdapter: CheckoutAdapter
    private lateinit var deliveryServiceAdapter: DeliveryServiceAdapter
    private lateinit var bankAdapter: BankAdapter
    private val checkoutActivityViewModel: CheckoutActivityViewModel by viewModel()
    private var productCarts: List<ProductCart> = ArrayList()
    private var cartModels: ArrayList<CartModel> = ArrayList()
    private var storeId: ArrayList<String> = ArrayList()
    private var expedition: ArrayList<String> = ArrayList()
    private var checkOutModels: ArrayList<CheckOutModel> = ArrayList()
    private var myCityId: String = ""
    private var addressId: String = ""
    private var costExpedition: Int = 0
    private var bankId = ""
    private var lastService = ""
    private var isChooseServiceExpedition = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityCheckoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initData()
        init()
        initAction()
        getDeliveryService()
        setTotalBuy(productCarts)
        initObserve()
        initSwipeRefresh()

    }

    private fun initSwipeRefresh(){
        binding.apply {
            swipeRefresh.setOnRefreshListener {
                storeId.clear()
                expedition.clear()
                checkOutModels.clear()
                init()
                getDeliveryService()
                setTotalBuy(productCarts)
                swipeRefresh.isRefreshing = false
            }
        }
    }

    private fun initData() {
        productCarts = intent.getSerializableExtra(CartActivity.KEY_CART) as List<ProductCart>
        cartModels.addAll(GroupingCart.grouping(productCarts))
    }

    private fun initAction() {
        binding.apply {
            ivCheckoutToPageAddress.setOnClickListener {
                intent = Intent(this@CheckoutActivity, AddressActivity::class.java)
                intent.putExtra(CartActivity.KEY_CART, productCarts as Serializable)
                intent.putExtra(KEY_FROM, "checkoutHasData")
                startActivityForResult(intent, 1)
            }
            ivCheckoutBackNavigation.setOnClickListener {
                cartModels.clear()
                finish()
            }
            rlShadow.setOnClickListener {
                rlShadow.visibility = View.GONE
                binding.clDeliveryService.visibility = View.GONE
                isChooseServiceExpedition = !isChooseServiceExpedition
            }
            btnCheckoutToPayment.setOnClickListener {
                if (addressId.isEmpty() || addressId.isNullOrEmpty()) {
                    ToastMessage.showToast(this@CheckoutActivity, "Atur alamat")
                } else if (bankId.isEmpty()) {
                    ToastMessage.showToast(this@CheckoutActivity, "Pilih metode pembayaran")
                } else {
                    binding.pbCheckout.visibility = View.VISIBLE
                    checkoutActivityViewModel.checkOut(
                        checkOutModels,
                        bankId = bankId,
                        addressId
                    )
                }
            }
        }
    }

    private fun init() {
        binding.shimerCheckout.visibility = View.VISIBLE
        binding.shimerCheckout.startShimmer()
        binding.shimerAddressCheckout.visibility = View.VISIBLE
        binding.shimerAddressCheckout.startShimmer()
        checkoutActivityViewModel.getListAddress()
        checkoutActivityViewModel.getBank()
        checkoutAdapter = CheckoutAdapter(object : CheckoutAdapter.Listener {
            override fun chooseDeliveryService(
                position: Int,
                resultDeliveryService: ResultDeliveryService
            ) {
                //show shadow
                binding.rlShadow.visibility = View.VISIBLE
                isChooseServiceExpedition = !isChooseServiceExpedition

                binding.clDeliveryService.visibility = View.VISIBLE
                deliveryServiceAdapter.setItem(position, resultDeliveryService.costs)
                deliveryServiceAdapter.notifyDataSetChanged()
            }
        })
        bankAdapter = BankAdapter(object : BankAdapter.Listener {
            override fun selectedBank(bank: Bank) {
                bankId = bank.id
            }
        })
        deliveryServiceAdapter = DeliveryServiceAdapter(object : DeliveryServiceAdapter.Listener {
            override fun selectDeliveryService(
                positionStore: Int,
                resultCostDeliveryService: ResultCostDeliveryService
            ) {
                checkoutAdapter.setDeliveryService(resultCostDeliveryService)
                checkoutAdapter.notifyItemChanged(positionStore)
                binding.clDeliveryService.visibility = View.GONE

                if(!lastService.contains(resultCostDeliveryService.service)){
                    costExpedition = 0
                    costExpedition += resultCostDeliveryService.cost.get(0).value.toInt()
                    setTotalBuy(productCarts, costExpedition)
                }

                //remove shadow
                binding.rlShadow.visibility = View.GONE
                isChooseServiceExpedition = !isChooseServiceExpedition

                lastService = resultCostDeliveryService.service
                binding.tvCheckoutDeliveryFee.text = NumberConversion.conversionToRupiah(costExpedition.toDouble())
            }
        })
        binding.rvCheckout.adapter = checkoutAdapter
        binding.rvPaymentBank.adapter = bankAdapter
        binding.includeDeliveryService.rvDeliveryService.adapter = deliveryServiceAdapter
        binding.rvCheckout.isNestedScrollingEnabled = false
        binding.rvPaymentBank.isNestedScrollingEnabled = false
    }

    private fun getDeliveryService() {
        productCarts.forEach {
            storeId.add(it.store.id)
        }
        for (i in storeId.indices) {
            checkoutActivityViewModel.getDeliveryService(storeId.get(i))
        }
    }

    private fun initObserve() {
        checkoutActivityViewModel.isError.observe(this) {
            if (it) {
                ToastMessage.showToast(this, "Atur alamat terlebih dahulu")
                val intent = Intent(this@CheckoutActivity, AddressActivity::class.java)
                intent.putExtra(KEY_FROM, "checkout")
                startActivity(intent)
                finish()

                binding.tvCheckoutPrimaryAddress.text = "Belum mengatur alamat"
                binding.tvCheckoutAddressPhone.visibility = View.GONE
                binding.tvCheckoutDetailAddres.visibility = View.GONE
                binding.shimerCheckout.visibility = View.GONE
                binding.shimerCheckout.stopShimmer()
                binding.shimerAddressCheckout.visibility = View.GONE
                binding.shimerAddressCheckout.stopShimmer()
            }
        }
        checkoutActivityViewModel.getListAddress.observe(this@CheckoutActivity) {
            if (it.data.isNotEmpty()) {
                myCityId = it.data.get(0).idCity.toString()
                addressId = it.data.get(0).id
                Log.d("TAG", "initObserve city: $myCityId")
                binding.tvCheckoutPrimaryAddress.text = "${it.data.get(0).recipientName} (Utama)"
                binding.tvCheckoutAddressPhone.text = "${it.data.get(0).recipientPhone}"
                binding.tvCheckoutDetailAddres.text = it.data.get(0).fullAddress
                binding.tvCheckoutAddressPhone.visibility = View.VISIBLE
                binding.tvCheckoutDetailAddres.visibility = View.VISIBLE
                binding.shimerAddressCheckout.visibility = View.GONE
                binding.shimerAddressCheckout.stopShimmer()
            }
        }
        checkoutActivityViewModel.getDeliveryService.observe(this@CheckoutActivity) {
            expedition.add(it.data.deliveryService)
            Log.d("TAG", "initObserve size expedition: ${expedition.size}")
            if (cartModels.size == expedition.size) {
                Log.d("TAG", "initObserve size expedition 2: ${expedition.size}")
                fetchCostDeliveryService(expedition)
            }
        }
        checkoutActivityViewModel.getCheckOutModel.observe(this@CheckoutActivity) {
            binding.shimerCheckout.visibility = View.GONE
            binding.shimerCheckout.stopShimmer()
            checkOutModels.addAll(it)
            checkoutAdapter.setItem(it)
            checkoutAdapter.notifyDataSetChanged()
        }
        checkoutActivityViewModel.getListBank.observe(this@CheckoutActivity) {
            bankAdapter.setItem(it.data)
            bankAdapter.notifyDataSetChanged()
        }
        checkoutActivityViewModel.getIsSuccessOrder.observe(this@CheckoutActivity) {
            binding.pbCheckout.visibility = View.GONE
            if (!it.isNullOrEmpty()) {
                val intent = Intent(this@CheckoutActivity, PaymentActivity::class.java)
                intent.putExtra(KEY_PAYMENT_ID, it)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun setTotalBuy(productCarts: List<ProductCart>, deliveryCost: Int = 0) {
        binding.apply {
            var total = 0
            var totalPay = 0

            for (i in productCarts.indices) {
                total += productCarts[i].price.toInt()
            }
            tvCheckoutPurchaseAmount.text = "${productCarts.size.toString()} pcs"
            tvCheckoutPurchasePrice.text = NumberConversion.conversionToRupiah(total.toDouble())
            totalPay = deliveryCost + total
            binding.tvCheckoutTotalPay.text =
                NumberConversion.conversionToRupiah(totalPay.toDouble())
        }
    }

    private fun fetchCostDeliveryService(expedition: List<String>) {

        if (myCityId.isNotEmpty() && cartModels.size != 0) {
            checkoutActivityViewModel.getCostDeliveryService(
                expedition = expedition,
                cartModels = cartModels,
                myCityId
            )
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1 && data != null) {
            address = data.getSerializableExtra(KEY_DEFAULT_ADDRESS) as Address
            myCityId = address.idCity.toString()
            binding.tvCheckoutPrimaryAddress.text = "${address.recipientName} (Utama)"
            binding.tvCheckoutAddressPhone.text = address.recipientPhone
            binding.tvCheckoutDetailAddres.text = address.fullAddress
        }
    }

    companion object {
        val KEY_FROM = "key checkout"
        val KEY_TEMP_CART = "key default temp data cart"
        val KEY_DEFAULT_ADDRESS = "key default address"
        val KEY_PAYMENT_ID = "key payment id"
    }

}