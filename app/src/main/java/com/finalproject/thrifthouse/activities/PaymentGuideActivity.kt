package com.finalproject.thrifthouse.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.SubCategoryAdapter
import com.finalproject.thrifthouse.databinding.ActivityMainBinding
import com.finalproject.thrifthouse.databinding.ActivityPaymentBinding
import com.finalproject.thrifthouse.databinding.ActivityPaymentGuideBinding
import com.finalproject.thrifthouse.utilities.DataDummy

class PaymentGuideActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPaymentGuideBinding
    private lateinit var subCategoryAdapter: SubCategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentGuideBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val dataSubCategory = DataDummy().dataGuidePayment

        binding.apply {
            subCategoryAdapter = SubCategoryAdapter(object : SubCategoryAdapter.Listener{
                override fun selectedItem(subCategory: String, subCategoryTwo: String) {
                    Log.d("TAG", "subCategory: $subCategory, subCategoryTwo: $subCategoryTwo")
                }
            })
            rvPaymentGuide.layoutManager = LinearLayoutManager(this@PaymentGuideActivity, LinearLayoutManager.VERTICAL, false)
            rvPaymentGuide.adapter = subCategoryAdapter
            subCategoryAdapter.setItem(dataSubCategory, "payment")
        }
    }
}