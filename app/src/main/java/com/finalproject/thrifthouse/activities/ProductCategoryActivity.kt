package com.finalproject.thrifthouse.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.adapter.ProductAdapter
import com.finalproject.thrifthouse.databinding.ActivityProductBinding
import com.finalproject.thrifthouse.databinding.ActivityProductCategoryBinding
import com.finalproject.thrifthouse.fragment.HomeFragment
import com.finalproject.thrifthouse.fragment.category.ProductFragment
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.ProductActivityViewModel
import com.finalproject.thrifthouse.viewmodel.ProductCategoryActivityViewModel
import com.she.sehatq.base.utils.EndlessScrollListener
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductCategoryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductCategoryBinding
    private var category: String? = null
    private var subCategoryOne: String? = null
    private var subCategoryTwo: String? = null
    private lateinit var productAdapter: ProductAdapter
    var page = 0
    var totalPage = 0
    var isLoading = false
    private lateinit var scrollListener: EndlessScrollListener
    private val productViewModel: ProductCategoryActivityViewModel by viewModel()
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    private var productList = mutableListOf<ProductModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        initData()
        initObserver()
        initScrollListener()
        initAction()

    }

    private fun initAction(){
        binding.apply {
            ivBack.setOnClickListener {
                finish()
            }
            ivBag.setOnClickListener {
                val intent = Intent(this@ProductCategoryActivity, CartActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun init(){
        productAdapter = ProductAdapter("vertical")
        binding.apply {
            rvProduct.adapter = productAdapter
        }
    }

    private fun initData() {
        category = intent.getStringExtra(KEY_CATEGORY)?.toLowerCase()?.replace(" ","")
        subCategoryOne = intent.getStringExtra(KEY_SUB_CATEGORY)?.toLowerCase()?.replace(" ","")
        subCategoryTwo = intent.getStringExtra(KEY_SUB_CATEGORY_TWO)?.toLowerCase()?.replace(" ","")
        fetchData()
    }

    private fun initScrollListener() {
        scrollListener =
            object : EndlessScrollListener(binding.rvProduct.layoutManager as GridLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    if (page <= totalPage) {
                        productViewModel.isLoading(true)
                        fetchData()
                    } else {
                        productViewModel.isLoading(false)
                    }
                }

            }
        binding.rvProduct.addOnScrollListener(scrollListener)
    }

    private fun fetchData() {
        productViewModel.isLoading(true)
        productViewModel.getProduct(
            category = category,
            subCategory = subCategoryOne,
            subCategoryTwo = subCategoryTwo,
            page = page
        )
        binding.rvProduct.post {
            productAdapter.notifyDataSetChanged()
        }
        page += 1
    }

    private fun initObserver(){
        productViewModel.getIsLoading.observe(this@ProductCategoryActivity) {
            if (it) {
                binding.pbProduct.visibility = View.VISIBLE
            } else {
                binding.pbProduct.visibility = View.GONE
            }
        }
        productViewModel.getProducts.observe(this@ProductCategoryActivity) {
            productList.addAll(it)
            productAdapter.setItem(
                favoriteViewModel,
                productList.toCollection(ArrayList()),
                CheckAuth().checkAuth(this.application)
            )
            productAdapter.notifyDataSetChanged()
            productViewModel.isLoading(false)
        }
    }

    companion object {
        val KEY_CATEGORY  = "key category"
        val KEY_SUB_CATEGORY  = "key sub category"
        val KEY_SUB_CATEGORY_TWO  = "key sub category two"
    }
}