package com.finalproject.thrifthouse.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.finalproject.thrifthouse.adapter.MyOrderCategoryAdapter
import com.finalproject.thrifthouse.databinding.ActivityMyOrderBinding
import com.finalproject.thrifthouse.fragment.AccountFragment
import com.google.android.material.tabs.TabLayoutMediator

class MyOrderActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMyOrderBinding
    private var positionOrder = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyOrderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        initData()
        initAction()

    }

    private fun initData(){
        positionOrder = intent.getIntExtra(AccountFragment.KEY_POSITION_ORDER, 0)
        binding.tabMyOrder.getTabAt(positionOrder)?.select()
    }

    private fun init(){
        binding.apply {
            vpMyOrder.adapter = MyOrderCategoryAdapter(this@MyOrderActivity)
            TabLayoutMediator(tabMyOrder, vpMyOrder){ tab, position ->
                when(position){
                    0 -> tab.text = "Belum Bayar"
                    1 -> tab.text = "Dikemas"
                    2 -> tab.text = "Dikirim"
                    3 -> tab.text = "Selesai"
                    else -> tab.text = "Dibatalkan"
                }
            }.attach()


        }
    }

    private fun initAction(){
        binding.apply {
            ivBack.setOnClickListener {
                finish()
            }
        }
    }
}