package com.finalproject.thrifthouse.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.marginTop
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.PaymentResponse
import com.finalproject.thrifthouse.databinding.ActivityPaymentBinding
import com.finalproject.thrifthouse.utilities.CopyTextHelper
import com.finalproject.thrifthouse.utilities.ImageProcess
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.PaymentActivityViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.serialization.json.JsonObject
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class PaymentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPaymentBinding
    private val REQUEST_CODE_PERMISSION = 100
    private var orderId = ""
    private val paymentActivityViewModel: PaymentActivityViewModel by viewModel()
    private var imagePath = ""
    private var numberBank = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        fetchDetailUser()
        fetchPayment()
        initAction()
        initObserver()

    }

    private fun init() {
        orderId = intent.getStringExtra(CheckoutActivity.KEY_PAYMENT_ID).toString()
    }

    private fun fetchDetailUser() {
        paymentActivityViewModel.getUserDetail()
    }

    private fun fetchPayment() {
        binding.pbPayment.visibility = View.VISIBLE
        paymentActivityViewModel.getPayment(orderId)
    }

    private fun initAction() {
        binding.apply {
            rlChooseImage.setOnClickListener {
                checkingPermissions()
            }
            ivClose.setOnClickListener {
                binding.llEmptyImage.visibility = View.VISIBLE
                binding.rlContainImage.visibility = View.GONE
            }
            ivCopyNumberBank.setOnClickListener {
                if (numberBank.isNotEmpty() || numberBank.isNotBlank()) {
                    CopyTextHelper.copyFileText(this@PaymentActivity, numberBank)
                    ToastMessage.showToast(this@PaymentActivity, "Copy Nomor Rekening")
                }
            }
            llToPaymentGuide.setOnClickListener {
                val intent = Intent(this@PaymentActivity, PaymentGuideActivity::class.java)
                startActivity(intent)
            }
            btnBackToHome.setOnClickListener {
                val intent = Intent(this@PaymentActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
            btnSend.setOnClickListener {
                postReceipt()
            }
        }
    }

    private fun initObserver() {
        paymentActivityViewModel.getUserDetail.observe(this@PaymentActivity) {
            binding.tvName.text = " ${it.data.username}"
        }

        paymentActivityViewModel.getOrderResponse.observe(this@PaymentActivity) {
            binding.pbPayment.visibility = View.GONE
            if (it.data.orderCode.isNotEmpty()) {
                binding.apply {

                    tvStatusOrder.text = it.data.status

                    tvOrderCode.text = it.data.orderCode
                    tvBankNumber.text = it.data.bank.bankNumber
                    numberBank = it.data.bank.bankNumber

                    val totalPayment = it.data.shippingCost + it.data.productsPrice
                    tvTotalPayment.text =
                        NumberConversion.conversionToRupiah(totalPayment.toDouble())

                    Glide
                        .with(this@PaymentActivity)
                        .load(it.data.bank.bankLogo)
                        .centerCrop()
                        .into(ivBank)
                    if (it.data.receipt.isNotEmpty()
                        || it.data.receipt.isNotBlank()
                        || it.data.receipt.length != 0
                    ) {
                        llEmptyImage.visibility = View.GONE
                        rlContainImage.visibility = View.VISIBLE
                        ivClose.visibility = View.GONE
                        btnSend.visibility = View.GONE
                        Glide
                            .with(this@PaymentActivity)
                            .load(it.data.receipt)
                            .centerCrop()
                            .into(ivProofPayment)
                        tvOne.text = "Status Pembayaran"
                        binding.tvTimeExpired.text = "Berhasil"
                    } else {
                        startTimer(it)
                    }


                }
            }
        }

        paymentActivityViewModel.isSucces.observe(this@PaymentActivity) {
            binding.pbPayment.visibility = View.GONE
            if (it) {
                ToastMessage.showToast(this, "Berhasil upload bukti pembayaran")
                val intent = Intent(this@PaymentActivity, MyOrderActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                ToastMessage.showToast(this, "Gagal upload bukti pembayaran")
            }
        }
    }

    private fun startTimer(it: PaymentResponse) {
        //expiredDate
        val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val formatter = SimpleDateFormat("dd MM yyyy HH:mm:ss")
        val outputDateExpired: String = formatter.format(parser.parse(it.data.expiredAt))
        val dateTimeExpired = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LocalDateTime.parse(
                outputDateExpired,
                DateTimeFormatter.ofPattern("dd MM yyyy HH:mm:ss")
            )
        } else {
            TODO("VERSION.SDK_INT < O")
        }


        val thread: Thread = object : Thread() {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun run() {
                try {
                    while (!this.isInterrupted) {
                        sleep(1000)
                        runOnUiThread {
                            //date now
                            val current = LocalDateTime.now()
                            val formatterDateTime =
                                DateTimeFormatter.ofPattern("dd MM yyyy HH:mm:ss")
                            val outputDateNow = current.format(formatterDateTime)
                            val dateTimeNow = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                LocalDateTime.parse(
                                    outputDateNow,
                                    DateTimeFormatter.ofPattern("dd MM yyyy HH:mm:ss")
                                )
                            } else {
                                TODO("VERSION.SDK_INT < O")
                            }
                            val diff = Duration.between(dateTimeNow, dateTimeExpired)
                            val formattedDiff: String =
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                                    java.lang.String.format(
                                        Locale.ENGLISH,
                                        "%d jam %d menit %d detik",
                                        diff.toHoursPart(),
                                        diff.toMinutesPart(),
                                        diff.toSecondsPart()
                                    )
                                } else {
                                    TODO("VERSION.SDK_INT < S")
                                }
                            binding.tvTimeExpired.text = formattedDiff
                        }
                    }
                } catch (e: InterruptedException) {
                }
            }
        }
        thread.start()
    }

    private fun checkingPermissions() {
        if (isGranted(
                this, android.Manifest.permission.CAMERA,
                arrayOf(
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.INTERNET,
                    android.Manifest.permission.ACCESS_NETWORK_STATE,
                    android.Manifest.permission.READ_PHONE_STATE,
                ),
                REQUEST_CODE_PERMISSION
            )
        ) {
            chooseImageDialog()
        } else {
            chooseImageDialog()
        }
    }

    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.title_denied)
            .setMessage(R.string.message_denied)
            .setPositiveButton(
                R.string.pass_setting
            ) { _, _ ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            }.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.show()

    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(this)
            .setMessage(R.string.choose_image)
            .setPositiveButton(R.string.gallery) { _, _ -> openGallery() }
            .setNegativeButton(R.string.camera) { _, _ -> openCamera() }
            .show()
    }

    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let {
                binding.ivProofPayment.setImageURI(it)
                val fileName = File(it.path as String).name
                imagePath = ImageProcess.compressGetImageFilePath(this, it)
                binding.tvNameImgProofPayment.text = fileName.toString()
                binding.llEmptyImage.visibility = View.GONE
                binding.rlContainImage.visibility = View.VISIBLE
            }
        }

    private fun openGallery() {
        intent.type = "image/*"
        intent.setAction(Intent.ACTION_PICK)
        galleryResult.launch("image/*")
    }

    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                handleCameraImage(result.data)
                binding.llEmptyImage.visibility = View.GONE
                binding.rlContainImage.visibility = View.VISIBLE
            }
        }

    private fun handleCameraImage(intent: Intent?) {
        val bitmap = intent?.extras?.get("data") as Bitmap
        binding.ivProofPayment.setImageBitmap(bitmap)
        val tempUri: Uri = ImageProcess.getImageUri(applicationContext, bitmap)
        val finalFile: File = File(ImageProcess.getRealPathFromURI(applicationContext, tempUri))
        imagePath = finalFile.path
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraResult.launch(cameraIntent)
    }

    private fun postReceipt() {
        Log.d("TAG", "image path: $imagePath")
        Log.d("TAG", "order id: $orderId")
        if (imagePath.isNotEmpty() || imagePath.isNotBlank()) {
            binding.pbPayment.visibility = View.VISIBLE
            val file = File(imagePath)
            val mediaType = "image/*"
            val reqFile = RequestBody.create(mediaType.toMediaType(), file)
            val imageRequest = MultipartBody.Part.createFormData("photo", file.name, reqFile)
            paymentActivityViewModel.postReceipt(orderId, imageRequest)
        } else {
            ToastMessage.showToast(applicationContext, "Lampirkan bukti pembayaran")
        }
    }
}