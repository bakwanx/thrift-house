package com.finalproject.thrifthouse.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.adapter.*
import com.finalproject.thrifthouse.databinding.ActivityProductBinding
import com.finalproject.thrifthouse.fragment.HomeFragment
import com.finalproject.thrifthouse.fragment.category.ProductFragment
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.utilities.DataDummy
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.ProductActivityViewModel
import com.she.sehatq.base.utils.EndlessScrollListener
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProductActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductBinding
    private lateinit var productAdapter: ProductAdapter
    private lateinit var filterCategoryAdapter: FilterCategoryAdapter
    private lateinit var filterBrandAdapter: FilterBrandAdapter
    private lateinit var filterSizeAdapter: FilterSizeAdapter
    private lateinit var filterConditionAdapter: FilterConditionAdapter
    private val productViewModel: ProductActivityViewModel by viewModel()
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    var page = 0
    var totalPage = 0
    var isLoading = false
    private lateinit var scrollListener: EndlessScrollListener
    private var keyword: String? = ""
    private var category: String? = ""
    private var productList = mutableListOf<ProductModel>()
    private var isFilter = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        initAction()
        initScrollListener()
        initData()
        fetchData()
        initObserver()

    }

    private fun init() {
        binding.apply {
            etProduct.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    val keyword = etProduct.text.toString()
                    tvResultSearch.text = "Hasil Pencarian Untuk : \"$keyword\""
                    binding.tvResultSearch.visibility = View.VISIBLE
                    fetchSpecificData(keyword.toString(), null)
                    return@setOnKeyListener true
                }
                return@setOnKeyListener false
            }
        }
        productAdapter = ProductAdapter("vertical")
        filterCategoryAdapter = FilterCategoryAdapter(object : FilterCategoryAdapter.Listener {
            override fun selectedCategory(category: String?) {
                productViewModel.setCategoryFilter(category.toString())
            }
        })
        filterBrandAdapter = FilterBrandAdapter(object : FilterBrandAdapter.Listener {
            override fun selectedBrand(brand: String?) {
                productViewModel.setBrandFilter(brand.toString())
            }

        })
        filterSizeAdapter = FilterSizeAdapter(object : FilterSizeAdapter.Listener {
            override fun selectedSize(size: String?) {
                productViewModel.setSizeFilter(size.toString())
            }

        })
        filterConditionAdapter = FilterConditionAdapter(object : FilterConditionAdapter.Listener {
            override fun selectedCondition(condition: String?) {
                productViewModel.setConditionFilter(condition.toString())
            }

        })
        binding.apply {
            rvProduct.adapter = productAdapter
            includeFilter.rvCategory.adapter = filterCategoryAdapter
            includeFilter.rvBrand.adapter = filterBrandAdapter
            includeFilter.rvSize.adapter = filterSizeAdapter
            includeFilter.rvCondition.adapter = filterConditionAdapter
        }

        setFilter()

        productViewModel.getPagination.observe(this@ProductActivity) {
            totalPage = it.totalPages.toInt()
        }
    }

    private fun initData() {

        keyword = intent.getStringExtra(HomeFragment.KEY_KEYWORD)
        category = intent.getStringExtra(ProductFragment.KEY_CATEGORY)

        if (keyword != null) {
            binding.etProduct.text = Editable.Factory.getInstance().newEditable(keyword)
        } else {
            binding.tvResultSearch.visibility = View.GONE
        }

    }

    private fun initObserver() {
        productViewModel.getIsLoading.observe(this@ProductActivity) {
            if (it) {
                binding.pbProduct.visibility = View.VISIBLE
            } else {
                binding.pbProduct.visibility = View.GONE
            }
        }
        productViewModel.getProducts.observe(this) {
            productList.addAll(it)
            productAdapter.setItem(
                favoriteViewModel,
                productList.toCollection(ArrayList()),
                CheckAuth().checkAuth(this.application)
            )
            productAdapter.notifyDataSetChanged()
            productViewModel.isLoading(false)
        }
    }

    private fun initAction() {

        binding.apply {
            ivBag.setOnClickListener {
                if (CheckAuth().checkAuth(application)) {
                    val intent = Intent(this@ProductActivity, CartActivity::class.java)
                    startActivity(intent)

                } else {
                    val intent = Intent(this@ProductActivity, LoginActivity::class.java)
                    startActivity(intent)
                }
            }

            ivBack.setOnClickListener {
                finish()
            }

            llSort.setOnClickListener {
                if (isFilter) {
                    rlShadow.visibility = View.VISIBLE
                    clFilterOrder.visibility = View.VISIBLE
                    clFilterFilter.visibility = View.GONE
                } else {
                    rlShadow.visibility = View.GONE
                    clFilterOrder.visibility = View.GONE
                    clFilterFilter.visibility = View.GONE
                }
                isFilter = !isFilter
            }

            llFilter.setOnClickListener {
                if (isFilter) {
                    rlShadow.visibility = View.VISIBLE
                    clFilterFilter.visibility = View.VISIBLE
                    clFilterOrder.visibility = View.GONE
                } else {
                    rlShadow.visibility = View.GONE
                    clFilterFilter.visibility = View.GONE
                    clFilterOrder.visibility = View.GONE
                }
                isFilter = !isFilter
            }

            includeFilter.btnApply.setOnClickListener {
                page = 0
                productList.clear()
                rlShadow.visibility = View.GONE
                clFilterFilter.visibility = View.GONE
                clFilterOrder.visibility = View.GONE
                isFilter = !isFilter
                fetchData()
            }

            rlShadow.setOnClickListener {
                rlShadow.visibility = View.GONE
                clFilterFilter.visibility = View.GONE
                clFilterOrder.visibility = View.GONE
                isFilter = !isFilter
            }

            includeOrder.rlHighest.setOnClickListener {
                includeOrder.ivCheckHighest.visibility = View.VISIBLE
                includeOrder.ivCheckLowest.visibility = View.GONE
                includeOrder.ivCheckLatest.visibility = View.GONE
                if (isFilter) {
                    rlShadow.visibility = View.VISIBLE
                    clFilterFilter.visibility = View.VISIBLE
                    clFilterOrder.visibility = View.GONE
                } else {
                    rlShadow.visibility = View.GONE
                    clFilterFilter.visibility = View.GONE
                    clFilterOrder.visibility = View.GONE
                }
                isFilter = !isFilter
                val sortBy = "highest"
                fetchSpecificData(keyword.toString(), sortBy)
            }
            includeOrder.rlLowest.setOnClickListener {
                includeOrder.ivCheckHighest.visibility = View.GONE
                includeOrder.ivCheckLowest.visibility = View.VISIBLE
                includeOrder.ivCheckLatest.visibility = View.GONE
                if (isFilter) {
                    rlShadow.visibility = View.VISIBLE
                    clFilterFilter.visibility = View.VISIBLE
                    clFilterOrder.visibility = View.GONE
                } else {
                    rlShadow.visibility = View.GONE
                    clFilterFilter.visibility = View.GONE
                    clFilterOrder.visibility = View.GONE
                }
                isFilter = !isFilter
                val sortBy = "lowest"
                fetchSpecificData(keyword.toString(), sortBy)
            }
            includeOrder.rlLatest.setOnClickListener {
                includeOrder.ivCheckHighest.visibility = View.GONE
                includeOrder.ivCheckLowest.visibility = View.GONE
                includeOrder.ivCheckLatest.visibility = View.VISIBLE
                if (isFilter) {
                    rlShadow.visibility = View.VISIBLE
                    clFilterFilter.visibility = View.VISIBLE
                    clFilterOrder.visibility = View.GONE
                } else {
                    rlShadow.visibility = View.GONE
                    clFilterFilter.visibility = View.GONE
                    clFilterOrder.visibility = View.GONE
                }
                isFilter = !isFilter
                val sortBy = "latest"
                fetchSpecificData(keyword.toString(), sortBy)
            }
        }

    }

    private fun fetchSpecificData(keyword: String, sortBy: String?) {
        page = 0
        productList.clear()
        productViewModel.getProduct(category = category, search = keyword, page = page, sortBy = sortBy)
    }

    private fun initScrollListener() {
        scrollListener =
            object : EndlessScrollListener(binding.rvProduct.layoutManager as GridLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    if (page <= totalPage) {
                        productViewModel.isLoading(true)
                        fetchData()
                    } else {
                        productViewModel.isLoading(false)
                    }
                }

            }
        binding.rvProduct.addOnScrollListener(scrollListener)
    }

    private fun setFilter() {
        filterCategoryAdapter.setItem(DataDummy().dataFilterCategory)
        filterBrandAdapter.setItem(DataDummy().dataFilterBrand)
        filterSizeAdapter.setItem(DataDummy().dataFilterUkuran)
        filterConditionAdapter.setItem(DataDummy().dataFilterKondisi)
    }

    private fun fetchData() {
        productViewModel.isLoading(true)

        if (category.equals("pria")) {
            productViewModel.getProduct(category = "pria", search = keyword, page = page, sortBy = null)
        } else if (keyword.equals("wanita")) {
            productViewModel.getProduct(category = "wanita", search = keyword, page = page, sortBy = null)
        } else if (keyword.equals("anak")) {
            productViewModel.getProduct(category = "anak", search = keyword, page = page, sortBy = null)
        } else {
            productViewModel.getProduct(category = category, search = keyword, page = page, sortBy = null)
            binding.tvResultSearch.text = "Hasil Pencarian Untuk : \"$keyword\""
        }
        page += 1
        binding.rvProduct.post {
            productAdapter.notifyDataSetChanged()
        }
    }


}