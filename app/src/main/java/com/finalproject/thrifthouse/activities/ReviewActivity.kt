package com.finalproject.thrifthouse.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.DetailTransactionAdapter
import com.finalproject.thrifthouse.databinding.ActivityDetailTransactionBinding
import com.finalproject.thrifthouse.databinding.ActivityReviewBinding
import com.finalproject.thrifthouse.utilities.ImageProcess
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.DetailTransactionActivityViewModel
import com.finalproject.thrifthouse.viewmodel.ReviewActivityViewModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File

class ReviewActivity : AppCompatActivity() {

    private val REQUEST_CODE_PERMISSION = 100
    private lateinit var binding: ActivityReviewBinding
    private lateinit var detailTransactionAdapter: DetailTransactionAdapter
    private val productViewModel: ReviewActivityViewModel by viewModel()
    private var orderId: String? = ""
    private var imagePath = ""
    private var isAnonim = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        initData()
        initObserver()
        initAction()
    }

    private fun initAction(){
        binding.apply {
            btnImage.setOnClickListener {
                checkingPermissions()
            }
            btnSendReview.setOnClickListener {
                finish()
                sendReview()
            }
            ivClose.setOnClickListener {
                binding.btnImage.visibility = View.VISIBLE
                binding.rlContainImage.visibility = View.GONE
            }
            btnCancel.setOnClickListener {
                finish()
            }
            ivBackNavigation.setOnClickListener {
                finish()
            }
        }
    }

    private fun sendReview(){
        binding.apply {
            val rating = rbReview.rating.toInt()
            val review = etReview.text.toString()
            if(review.isNotEmpty() || review.isNotBlank() || rating != 0){
                val ratingBody: RequestBody = RequestBody.create(
                    "text/plain".toMediaTypeOrNull(),
                    rating.toString()
                )
                val reviewBody: RequestBody = RequestBody.create(
                    "text/plain".toMediaTypeOrNull(),
                    review.toString()
                )
            }else{
                ToastMessage.showToast(this@ReviewActivity, "Harap isi rating dan ulasan")
            }
        }
    }

    private fun init(){
        detailTransactionAdapter = DetailTransactionAdapter()
        binding.rvProductReview.adapter = detailTransactionAdapter
    }

    private fun initData() {
        orderId = intent.getStringExtra(KEY_ID_ORDER)
        productViewModel.getDetailTransaction(orderId.toString(), null)
        binding.pbReview.visibility = View.VISIBLE
    }

    private fun initObserver() {
        productViewModel.getDetailTransactions.observe(this@ReviewActivity) {
            binding.apply {
                binding.pbReview.visibility = View.GONE
                detailTransactionAdapter.setItem(it.data.store.get(0).products)
                detailTransactionAdapter.notifyDataSetChanged()
            }
        }
    }

    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let {
                binding.ivProduct.setImageURI(it)
                val fileName = File(it.path as String).name
                imagePath = ImageProcess.compressGetImageFilePath(this, it)
                binding.tvNameImgProduct.text = fileName.toString()
                binding.btnImage.visibility = View.GONE
                binding.rlContainImage.visibility = View.VISIBLE
            }
        }

    private fun openGallery() {
        intent.type = "image/*"
        intent.setAction(Intent.ACTION_PICK)
        galleryResult.launch("image/*")
    }

    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                handleCameraImage(result.data)
                binding.btnImage.visibility = View.GONE
                binding.rlContainImage.visibility = View.VISIBLE
            }
        }

    private fun handleCameraImage(intent: Intent?) {
        val bitmap = intent?.extras?.get("data") as Bitmap
        binding.ivProduct.setImageBitmap(bitmap)
        val tempUri: Uri = ImageProcess.getImageUri(applicationContext, bitmap)
        val finalFile: File = File(ImageProcess.getRealPathFromURI(applicationContext, tempUri))
        imagePath = finalFile.path
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraResult.launch(cameraIntent)
    }


    private fun checkingPermissions() {
        if (isGranted(
                this, android.Manifest.permission.CAMERA,
                arrayOf(
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.INTERNET,
                    android.Manifest.permission.ACCESS_NETWORK_STATE,
                    android.Manifest.permission.READ_PHONE_STATE,
                ),
                REQUEST_CODE_PERMISSION
            )
        ) {
            chooseImageDialog()
        } else {
            chooseImageDialog()
        }
    }

    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.title_denied)
            .setMessage(R.string.message_denied)
            .setPositiveButton(
                R.string.pass_setting
            ) { _, _ ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            }.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.show()

    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(this)
            .setMessage(R.string.choose_image)
            .setPositiveButton(R.string.gallery) { _, _ -> openGallery() }
            .setNegativeButton(R.string.camera) { _, _ -> openCamera() }
            .show()
    }

    companion object {
        val KEY_ID_ORDER = " key id order"
    }

    fun isAnonim(view: View) {
        if (binding.cbReview.isChecked) {
            isAnonim = true
        }else{
           isAnonim = false
        }
    }
}