package com.finalproject.thrifthouse.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.finalproject.thrifthouse.adapter.DetailTransactionAdapter
import com.finalproject.thrifthouse.databinding.ActivityDetailTransactionBinding
import com.finalproject.thrifthouse.databinding.ActivityPaymentBinding
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.viewmodel.CartActivityViewModel
import com.finalproject.thrifthouse.viewmodel.DetailTransactionActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailTransactionActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailTransactionBinding
    private var orderId: String? = ""
    private val detailTransactionActivityViewModel: DetailTransactionActivityViewModel by viewModel()
    private lateinit var detailTransactionAdapter: DetailTransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        initData()
        initAction()
        initObserver()

    }

    private fun init(){
        detailTransactionAdapter = DetailTransactionAdapter()
        binding.rvProductDetailTransaction.adapter = detailTransactionAdapter
    }

    private fun initData() {
        orderId = intent.getStringExtra(KEY_ID_ORDER)
        detailTransactionActivityViewModel.getDetailTransaction(orderId.toString(), null)
        binding.pbDetailTransaction.visibility = View.VISIBLE
    }

    private fun initAction(){
        binding.ivBackNavigation.setOnClickListener {
            finish()
        }
    }

    private fun initObserver() {
        detailTransactionActivityViewModel.getDetailTransactions.observe(this@DetailTransactionActivity) {
            binding.apply {
                binding.pbDetailTransaction.visibility = View.GONE
                tvIdPayment.text = it.data.orderCode
                tvCourier.text = it.data.store.get(0).shippingService
                tvStatusOrder.text = it.data.status
                tvPredictionArrived.text = "Akan diterima pada tanggal ${it.data.store.get(0).estimatedTimeOfDeparture} hari"
                tvNameRecipient.text = it.data.address.recipientName
                tvDetailAddress.text = it.data.address.detail
                tvCheckoutPurchaseAmount.text = "${it.data.store.get(0).products.size} pcs"
                var totalPriceProduct = 0
                it.data.store.get(0).products.forEachIndexed { index, productDetailTransaction ->
                    totalPriceProduct += totalPriceProduct + productDetailTransaction.price.toInt()
                }
                tvCheckoutPurchasePrice.text = NumberConversion.conversionToRupiah(totalPriceProduct.toDouble())
                tvCheckoutDeliveryFee.text = NumberConversion.conversionToRupiah(it.data.shippingCost.toDouble())
                val totalPay = totalPriceProduct + it.data.shippingCost
                tvCheckoutTotalPay.text = NumberConversion.conversionToRupiah(totalPay.toDouble())
                detailTransactionAdapter.setItem(it.data.store.get(0).products)
                detailTransactionAdapter.notifyDataSetChanged()
            }
        }
    }

    companion object {
        val KEY_ID_ORDER = "key id order"
        val KEY_ID_STATUS = "key status"
    }
}