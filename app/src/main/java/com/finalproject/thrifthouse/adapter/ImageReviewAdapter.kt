package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R

class ImageReviewAdapter: RecyclerView.Adapter<ImageReviewAdapter.ViewHolder>() {
    private var images: List<String> = ArrayList()
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_image_product)
        fun bind(imageString: String){
            Glide
                .with(itemView.context)
                .load(imageString)
                .centerCrop()
                .into(ivProduct)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_image_product, parent, false)
        return ImageReviewAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(imageString = images.get(position))
    }

    override fun getItemCount(): Int = images.size

    fun setImage(images: List<String>){
        this.images = images
    }
}