package com.finalproject.thrifthouse.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.DetailProductActivity
import com.finalproject.thrifthouse.api.response.ListProduct
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel

class ProductStoreAdapter(private val type: String): RecyclerView.Adapter<ProductStoreAdapter.ViewHolder>() {
    private var productStore: List<ProductModel> = ArrayList()
    private lateinit var favoriteViewModel: FavoriteViewModel

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_item_product)
        val tvBrand: TextView = itemView.findViewById(R.id.tv_item_brand)
        val tvName: TextView = itemView.findViewById(R.id.tv_item_name)
        val tvCondition: TextView = itemView.findViewById(R.id.tv_item_condition)
        val tvSize: TextView = itemView.findViewById(R.id.tv_item_size)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_item_price)
        val cdProduct: CardView = itemView.findViewById(R.id.cd_product)
        val ivFavorite: ImageView = itemView.findViewById(R.id.iv_favorite)

        fun bind(productStore: ProductModel, type: String, favoriteViewModel: FavoriteViewModel){
            val param = cdProduct.layoutParams as ViewGroup.MarginLayoutParams
            if (type.equals("vertical")){
                param.width = ViewGroup.LayoutParams.MATCH_PARENT
                param.height = ViewGroup.LayoutParams.WRAP_CONTENT

                param.width = 450

                cdProduct.setLayoutParams(param)

                param.setMargins(16,40,16,10)
            }else{
                param.setMargins(0,0,16,1)
            }

            Glide
                .with(itemView.context)
                .load(productStore.photos)
                .centerCrop()
                .into(ivProduct)
            tvBrand.text = productStore.brand
            tvName.text = productStore.name
            tvCondition.text = productStore.condition
            tvSize.text = productStore.size
            tvPrice.text = NumberConversion.conversionToRupiah(productStore.price.toDouble())
            ivFavorite.setOnClickListener {
                favoriteViewModel.postProductFavorite(productStore.id)
            }
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, DetailProductActivity::class.java)
                intent.putExtra(KEY_ID_PRODUCT, productStore.id)
                itemView.context.startActivity(intent)
            }
        }
    }


    @NonNull
    @Override
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductStoreAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductStoreAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productStore.size
    }

    fun setItem(favoriteViewModel: FavoriteViewModel, productModels: List<ProductModel>){
        this.productStore = productModels
        this.favoriteViewModel = favoriteViewModel
        notifyDataSetChanged()
    }

    companion object{
        val KEY_ID_PRODUCT = "key_id_product"
    }

    override fun onBindViewHolder(holder: ProductStoreAdapter.ViewHolder, position: Int) {
        holder.bind(productStore.get(position), type, favoriteViewModel)
    }
}