package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ProductOrderList
import com.finalproject.thrifthouse.utilities.NumberConversion
import org.w3c.dom.Text

class ProductOrderAdapter: RecyclerView.Adapter<ProductOrderAdapter.ViewHolder>() {

    private var productOrderList: List<ProductOrderList> = ArrayList()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_product_photo_cart)
        val tvBrand: TextView = itemView.findViewById(R.id.tv_brand)
        val tvName: TextView = itemView.findViewById(R.id.tv_name)
        val tvCondition: TextView = itemView.findViewById(R.id.tv_tag_condition)
        val tvSize: TextView = itemView.findViewById(R.id.tv_tag_size)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_price)

        fun bind(productOrderList: ProductOrderList){
            Glide
                .with(itemView.context)
                .load(productOrderList.photo)
                .centerCrop()
                .into(ivProduct)
            tvBrand.text = productOrderList.brand
            tvName.text = productOrderList.name
            tvCondition.text = productOrderList.condition
            tvSize.text = productOrderList.size
            tvPrice.text = NumberConversion.conversionToRupiah(productOrderList.price.toDouble())
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductOrderAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_checkout, parent, false)
        return ProductOrderAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductOrderAdapter.ViewHolder, position: Int) {
        holder.bind(productOrderList.get(position))
    }

    override fun getItemCount(): Int = productOrderList.size

    fun setItem(productOrderList: List<ProductOrderList>){
        this.productOrderList = productOrderList
    }
}