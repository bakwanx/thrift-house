package com.finalproject.thrifthouse.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.utilities.DataDummy

class SubCategoryAdapter internal constructor(private val listener: Listener):
    RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {
    private var subCategory: List<String> = ArrayList()
    private var category = ""
    var isSelected = true

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSubCategory: TextView = itemView.findViewById(R.id.tv_sub_category)
        val ivSubCategory: ImageView = itemView.findViewById(R.id.iv_sub_category_arrow)
        lateinit var subCategoryTwoAdapter: SubCategoryTwoAdapter
        val rvSubCategoryTwo: RecyclerView = itemView.findViewById(R.id.rv_sub_category_two)

        fun bind(subCategory: String) {
            tvSubCategory.text = subCategory
        }

        fun bindSubCategoryTwo(subCategory: String, category: String, position: Int, listener: Listener) {
            subCategoryTwoAdapter = SubCategoryTwoAdapter(object : SubCategoryTwoAdapter.Listener{
                override fun selectedItem(subCategoryTwo: String) {
                    listener.selectedItem(subCategory, subCategoryTwo)
                }
            })
            rvSubCategoryTwo.adapter = subCategoryTwoAdapter
            if (category.equals("male") && position == 0) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataAtasanPria)
            } else if (category.equals("male") && position == 1) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataBawahanPria)
            } else if (category.equals("male") && position == 2) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataLuaranPria)
            } else if (category.equals("male") && position == 3) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataSepatuPria)
            } else if (category.equals("male") && position == 4) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataAksesorisPria)
            }

            if (category.equals("female") && position == 0) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataAtasanWanita)
            } else if (category.equals("female") && position == 1) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataBawahanWanita)
            } else if (category.equals("female") && position == 2) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataLuaranWanita)
            } else if (category.equals("female") && position == 3) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataSepatuWanita)
            } else if (category.equals("female") && position == 4) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataAksesorisWanita)
            }

            if (category.equals("children") && position == 0) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataAtasanAnak)
            } else if (category.equals("children") && position == 1) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataBawahanAnak)
            } else if (category.equals("children") && position == 2) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataLuaranAnak)
            } else if (category.equals("children") && position == 3) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataSepatuAnak)
            } else if (category.equals("children") && position == 4) {
                subCategoryTwoAdapter.setItem(subCategory, DataDummy().dataAksesorisAnak)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubCategoryAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_sub_category, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SubCategoryAdapter.ViewHolder, position: Int) {
        holder.bind(subCategory.get(position))
        holder.bindSubCategoryTwo(subCategory.get(position), category, position, listener)
        holder.itemView.setOnClickListener {
            if (isSelected) {
                holder.ivSubCategory.setImageResource(R.drawable.ic_keyboard_arrow_down_24)
                holder.rvSubCategoryTwo.visibility = View.GONE
                isSelected = !isSelected
            } else {
                holder.ivSubCategory.setImageResource(R.drawable.ic_keyboard_arrow_up_24)
                holder.rvSubCategoryTwo.visibility = View.VISIBLE
                isSelected = !isSelected
            }
        }
    }

    interface Listener{
        fun selectedItem(subCategory: String, subCategoryTwo: String)
    }

    fun setItem(subCategory: List<String>, category: String){
        this.subCategory = subCategory
        this.category = category
    }

    override fun getItemCount(): Int {
        return subCategory.size
    }
}
