package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R

class SubCategoryTwoAdapter internal constructor(private val listener: Listener) :

    RecyclerView.Adapter<SubCategoryTwoAdapter.ViewHolder>() {

    private var subCategoryTwo: List<String> = ArrayList()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSubCategory: TextView = itemView.findViewById(R.id.tv_sub_category_two)

        fun bind(subCategoryTwo: String, listener: Listener) {
            tvSubCategory.text = subCategoryTwo
            itemView.setOnClickListener {
                listener.selectedItem(subCategoryTwo)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubCategoryTwoAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_sub_category_two, parent, false)
        return SubCategoryTwoAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SubCategoryTwoAdapter.ViewHolder, position: Int) {
        holder.bind(subCategoryTwo.get(position), listener)
    }

    override fun getItemCount(): Int = subCategoryTwo.size

    fun setItem(subCategory: String, subCategoryTwo: List<String>) {
        this.subCategoryTwo = subCategoryTwo
    }

    interface Listener {
        fun selectedItem(subCategoryTwo: String)
    }
}