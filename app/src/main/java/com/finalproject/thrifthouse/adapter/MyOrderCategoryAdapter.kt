package com.finalproject.thrifthouse.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.finalproject.thrifthouse.activities.DetailStoreActivity
import com.finalproject.thrifthouse.activities.MyOrderActivity
import com.finalproject.thrifthouse.fragment.category.ChildrenFragment
import com.finalproject.thrifthouse.fragment.category.FemaleFragment
import com.finalproject.thrifthouse.fragment.category.MaleFragment
import com.finalproject.thrifthouse.fragment.category.ProductFragment
import com.finalproject.thrifthouse.fragment.order.*
import com.finalproject.thrifthouse.fragment.store.*

class MyOrderCategoryAdapter(activity: MyOrderActivity): FragmentStateAdapter(activity) {

    private val pages = listOf(
        UnpaidFragment(),
        PackedFragment(),
        SentFragment(),
        DoneFragment(),
        CancelFragment()
    )

    override fun getItemCount(): Int  = pages.size

    override fun createFragment(position: Int): Fragment {

        return when (position) {
            0 -> pages[0]
            1 -> pages[1]
            2 -> pages[2]
            3 -> pages[3]
            else -> pages[4]
        }
    }


}