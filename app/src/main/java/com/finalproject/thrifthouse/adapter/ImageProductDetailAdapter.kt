package com.finalproject.thrifthouse.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.DetailProductActivity

class ImageProductDetailAdapter: RecyclerView.Adapter<ImageProductDetailAdapter.ViewHolder>() {
    private var images: List<String> = ArrayList()
    var checkedPosition = 0

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_image_product)
        val rlItem: RelativeLayout = itemView.findViewById(R.id.rl_img_item_product)

        fun bind(image: String, checkedPosition: Int){
            Glide
                .with(itemView.context)
                .load(image)
                .centerCrop()
                .into(ivProduct)
            if (checkedPosition != -1) {
                rlItem.setBackgroundResource(R.drawable.selected_item_image_product_default)
            }else{
                if (checkedPosition == adapterPosition) {
                    rlItem.setBackgroundResource(R.drawable.selected_item_image_product)
                }else{
                    rlItem.setBackgroundResource(R.drawable.selected_item_image_product_default)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ImageProductDetailAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_image_product, parent, false)
        return ImageProductDetailAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageProductDetailAdapter.ViewHolder, position: Int) {
        holder.bind(image = images.get(position), checkedPosition = checkedPosition)
        holder.itemView.setOnClickListener {
            holder.rlItem.setBackgroundResource(R.drawable.selected_item_image_product)
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position
                (holder.itemView.context as DetailProductActivity).setImage(images.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return images.size
    }

    fun setItem(images: List<String>){
        this.images = images
    }
}