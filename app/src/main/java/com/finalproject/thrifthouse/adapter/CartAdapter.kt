package com.finalproject.thrifthouse.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.api.response.StoreCart
import com.finalproject.thrifthouse.model.CartModel
import de.hdodenhof.circleimageview.CircleImageView

class CartAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    private var cartModels: List<CartModel> = ArrayList()
    private var isAllChecked: Boolean = false
    class ViewHolder(itemView: View, listener: Listener): RecyclerView.ViewHolder(itemView) {
        val ivStore: CircleImageView = itemView.findViewById(R.id.iv_store_logo)
        val rvCartProduct: RecyclerView = itemView.findViewById(R.id.rv_cart_product)
        val tvStore: TextView = itemView.findViewById(R.id.tv_store_name_cart)
        val cbStore: CheckBox = itemView.findViewById(R.id.cb_store)

        fun bind(cartModel: CartModel, isAllChecked: Boolean, listener: Listener){
            var productCartAdapter = ProductCartAdapter(object: ProductCartAdapter.Listener{
                override fun onSelectProduct(productCart: ProductCart) {
                    listener.selectedProduct(cartModel, productCart)
                }

                override fun removeSelectedProduct(productCart: ProductCart) {
                    listener.unselectedProduct(productCart)
                }
            })

            tvStore.text = cartModel.name
            Glide
                .with(itemView.context)
                .load(cartModel.photo)
                .centerCrop()
                .into(ivStore)

            productCartAdapter.setItemProductCart(cartModel.product)
            rvCartProduct.adapter = productCartAdapter

            if(isAllChecked){
                cbStore.isChecked = true
                listener.selectedStore(cartModel)
                productCartAdapter.setAllChecked(true)
                productCartAdapter.notifyDataSetChanged()
            }else{
                cbStore.isChecked = false
                listener.unselectedStore(cartModel)
                productCartAdapter.setAllChecked(false)
                productCartAdapter.notifyDataSetChanged()
            }

            cbStore.setOnClickListener {
                if(cbStore.isChecked){
                    listener.selectedStore(cartModel)
                    productCartAdapter.setAllChecked(true)
                } else{
                    listener.unselectedStore(cartModel)
                    productCartAdapter.setAllChecked(false)
                }
                productCartAdapter.notifyDataSetChanged()
            }
            productCartAdapter.notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_store, parent, false)
        return CartAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(cartModels.get(position), isAllChecked, listener)
    }

    interface Listener{
        fun selectedStore(cartModel: CartModel)
        fun unselectedStore(cartModel: CartModel)
        fun selectedProduct(cartModel: CartModel, productCart: ProductCart)
        fun unselectedProduct(productCart: ProductCart)
    }

    fun setAllChecked(isAllChecked: Boolean){
        this.isAllChecked = isAllChecked
    }

    override fun getItemCount(): Int = cartModels.size

    fun setItem(cartModels: List<CartModel>){
        this.cartModels = cartModels
    }
}