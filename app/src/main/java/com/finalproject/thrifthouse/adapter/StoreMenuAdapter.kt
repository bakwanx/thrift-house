package com.finalproject.thrifthouse.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.model.MenuStoreModel

class StoreMenuAdapter(private val menuStoreModels: List<MenuStoreModel>): RecyclerView.Adapter<StoreMenuAdapter.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivMenu: ImageView = itemView.findViewById(R.id.iv_menu)
        val tvTotalMenu: TextView = itemView.findViewById(R.id.tv_total_menu)
        val tvDescMenu: TextView = itemView.findViewById(R.id.tv_desc_menu)
        val vLine: View = itemView.findViewById(R.id.v_line)
        fun bind(menuStoreModel: MenuStoreModel){
            ivMenu.setImageResource(menuStoreModel.icImg)
            tvTotalMenu.text = menuStoreModel.number
            tvDescMenu.text = menuStoreModel.desc

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreMenuAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_store_menu, parent, false)
        return StoreMenuAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int = menuStoreModels.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(menuStoreModels.get(position))
        if(position==(getItemCount()-1)){
           holder.vLine.visibility = View.GONE
        }
    }
}