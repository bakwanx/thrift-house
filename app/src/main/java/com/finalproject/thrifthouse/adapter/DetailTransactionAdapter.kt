package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ProductDetailTransaction
import com.finalproject.thrifthouse.utilities.NumberConversion

class DetailTransactionAdapter: RecyclerView.Adapter<DetailTransactionAdapter.ViewHolder>()  {
    private var productList: List<ProductDetailTransaction> = ArrayList()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val tvBrand: TextView = itemView.findViewById(R.id.tv_brand)
        val tvName: TextView = itemView.findViewById(R.id.tv_name)
        val tvCondition: TextView = itemView.findViewById(R.id.tv_tag_condition)
        val tvSize: TextView = itemView.findViewById(R.id.tv_tag_size)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_tag_size)

        fun bind(productDetailTransaction: ProductDetailTransaction){
            tvBrand.text = productDetailTransaction.brand
            tvName.text = productDetailTransaction.name
            tvCondition.text = productDetailTransaction.condition
            tvSize.text = productDetailTransaction.size
            tvPrice.text = NumberConversion.conversionToRupiah(productDetailTransaction.price.toDouble())
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailTransactionAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_checkout, parent, false)
        return DetailTransactionAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: DetailTransactionAdapter.ViewHolder, position: Int) {
        holder.bind(productList.get(position))
    }

    override fun getItemCount(): Int = productList.size

    fun setItem(productList: List<ProductDetailTransaction>){
        this.productList = productList
    }
}