package com.finalproject.thrifthouse.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.finalproject.thrifthouse.fragment.category.ChildrenFragment
import com.finalproject.thrifthouse.fragment.category.FemaleFragment
import com.finalproject.thrifthouse.fragment.category.MaleFragment
import com.finalproject.thrifthouse.fragment.category.ProductFragment
import com.finalproject.thrifthouse.fragment.favorite.ProductFavoriteFragment
import com.finalproject.thrifthouse.fragment.favorite.StoreFavoriteFragment

class FavoriteAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

    private val pages = listOf(
        ProductFavoriteFragment(),
        StoreFavoriteFragment()
    )

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when(position){
            0 -> "Produk Favorit"
            else -> "Toko Favorit"
        }
    }

}