package com.finalproject.thrifthouse.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.DetailProductActivity
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.utilities.ToastMessage
import de.hdodenhof.circleimageview.CircleImageView

class ProductCartAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<ProductCartAdapter.ViewHolder>() {
    private var productCarts: List<ProductCart> = ArrayList()
    private var isAllChecked: Boolean = false
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val ivProduct: ImageView = itemView.findViewById(R.id.iv_product_photo_cart)
        val tvBrand: TextView = itemView.findViewById(R.id.tv_brand)
        val tvName: TextView = itemView.findViewById(R.id.tv_name)
        val tvCondition: TextView = itemView.findViewById(R.id.tv_tag_condition)
        val tvSize: TextView = itemView.findViewById(R.id.tv_tag_size)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_price)
        val cbProduct: CheckBox = itemView.findViewById(R.id.cb_item)

        fun bind(productCart: ProductCart, isAllChecked: Boolean, listener: Listener){
            Glide
                .with(itemView.context)
                .load(productCart.photo)
                .centerCrop()
                .into(ivProduct)
            tvBrand.text = productCart.brand
            tvName.text = productCart.name
            tvCondition.text = productCart.condition
            tvSize.text = productCart.size
            tvPrice.text = NumberConversion.conversionToRupiah(productCart.price.toDouble())
            cbProduct.isChecked = isAllChecked

            cbProduct.setOnClickListener {
                if(cbProduct.isChecked){
                    listener.onSelectProduct(productCart)
                }else{
                    listener.removeSelectedProduct(productCart)
                }
            }
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, DetailProductActivity::class.java)
                intent.putExtra(ProductStoreAdapter.KEY_ID_PRODUCT, productCart.id)
                itemView.context.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductCartAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
        return ProductCartAdapter.ViewHolder(view)
    }

    fun setAllChecked(isAllChecked: Boolean){
        this.isAllChecked = isAllChecked
    }

    override fun onBindViewHolder(holder: ProductCartAdapter.ViewHolder, position: Int) {
        holder.bind(productCarts.get(position), isAllChecked, listener)
    }

    interface Listener{
        fun onSelectProduct(productCart: ProductCart)
        fun removeSelectedProduct(productCart: ProductCart)
    }

    override fun getItemCount(): Int = productCarts.size

    fun setItemProductCart(productCart: List<ProductCart>){
        this.productCarts = productCart
    }
}