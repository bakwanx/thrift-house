package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.RatingItemAdapter.ViewHolder

class RatingItemAdapter: RecyclerView.Adapter<RatingItemAdapter.ViewHolder>() {
    private var ratingLength: Int = 0
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivStar: ImageView = itemView.findViewById(R.id.iv_star)
        fun bind(){
            ivStar.setImageResource(R.drawable.ic_star)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_star, parent, false)
        return RatingItemAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = ratingLength

    fun setTotalRating(ratingLength: Int){
        this.ratingLength = ratingLength
    }
}