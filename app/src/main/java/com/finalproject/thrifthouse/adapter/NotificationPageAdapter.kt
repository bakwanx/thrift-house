package com.finalproject.thrifthouse.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.finalproject.thrifthouse.fragment.notification.AllNotificationFragment
import com.finalproject.thrifthouse.fragment.notification.InfoNotificationFragment
import com.finalproject.thrifthouse.fragment.notification.OrderNotificationFragment

class NotificationPageAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    private val pages = listOf(
        AllNotificationFragment(),
        OrderNotificationFragment(),
        InfoNotificationFragment()
    )

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Semua"
            1 -> "Pesanan"
            else -> "Info"
        }
    }
}