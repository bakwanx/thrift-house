package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.api.response.Province
import com.finalproject.thrifthouse.model.CartModel
import java.util.ArrayList

class ProvinceAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<ProvinceAdapter.ViewHolder>()  {
    private var province: List<Province> = ArrayList()

    class ViewHolder(itemView: View, listener: Listener): RecyclerView.ViewHolder(itemView) {
        val tvLocation: TextView = itemView.findViewById(R.id.tv_address_location)

        fun bind(province: Province, listener: Listener){
            tvLocation.text = province.province
            itemView.setOnClickListener {
                listener.selectedProvince(province)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProvinceAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_province_city, parent, false)
        return ProvinceAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ProvinceAdapter.ViewHolder, position: Int) {
        holder.bind(province.get(position), listener)
    }

    interface Listener{
        fun selectedProvince(province: Province)
    }

    override fun getItemCount(): Int = province.size

    fun setProvince(locations: List<Province>){
        this.province = locations
    }
}