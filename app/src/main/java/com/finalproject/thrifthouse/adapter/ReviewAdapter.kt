package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.Review
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ReviewAdapter: RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {
    private var reviews: List<Review> = ArrayList()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvNameReviewer: TextView = itemView.findViewById(R.id.tv_name_review)
        val tvDateTime: TextView = itemView.findViewById(R.id.tv_date)
        val tvReview: TextView = itemView.findViewById(R.id.tv_text_review)
        val rvRating: RecyclerView = itemView.findViewById(R.id.rv_star_review)
        val ivProfileReview: ImageView = itemView.findViewById(R.id.iv_profile_review)
        val rvImageProduct: RecyclerView = itemView.findViewById(R.id.rv_image_product_review)
        var ratingItemAdapter: RatingItemAdapter = RatingItemAdapter()
        var imageReviewAdapter: ImageReviewAdapter = ImageReviewAdapter()
        fun bind(review: Review){
            tvNameReviewer.text = review.user.username
            tvReview.text = review.description
            val inputPattern = "yyyy-MM-dd'T'HH:mm:ss"
            val outputPattern = "dd MMMM yyyy"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            try {
                val date = inputFormat.parse(review.createdAt)
                val strDate = outputFormat.format(date)
                tvDateTime.text = strDate
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            ratingItemAdapter.setTotalRating(review.rating.toInt())
            rvRating.adapter = ratingItemAdapter

            imageReviewAdapter.setImage(review.photos)
            rvImageProduct.adapter = imageReviewAdapter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_review, parent, false)
        return ReviewAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reviews.get(position))
    }

    override fun getItemCount(): Int = reviews.size

    fun setReviews(reviews: List<Review>){
        this.reviews = reviews
    }
}