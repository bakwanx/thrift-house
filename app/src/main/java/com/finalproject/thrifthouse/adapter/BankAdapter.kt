package com.finalproject.thrifthouse.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.Bank

class BankAdapter internal constructor(private val listener: Listener) :
    RecyclerView.Adapter<BankAdapter.ViewHolder>() {
    private var banks: List<Bank> = ArrayList()
    private var isSelected = true
    var selectedPosition = -1

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivBank: ImageView = itemView.findViewById(R.id.iv_bank)
        val rbBank: RadioButton = itemView.findViewById(R.id.rb_bank)
        val tvBank: TextView = itemView.findViewById(R.id.tv_bank)

        fun bind(bank: Bank) {
            if (bank.bankName.contains("BCA")) {
                ivBank.setImageResource(R.drawable.ic_bank_central_asia)
            } else if (bank.bankName.contains("BNI")) {
                ivBank.setImageResource(R.drawable.ic_bni)
            } else if (bank.bankName.contains("BRI")) {
                ivBank.setImageResource(R.drawable.ic_bank_bri)
            } else if (bank.bankName.contains("Mandiri")) {
                ivBank.setImageResource(R.drawable.ic_bank_mandiri)
            }

            tvBank.text = bank.bankName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_bank, parent, false)
        return BankAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BankAdapter.ViewHolder, position: Int) {
        holder.bind(banks.get(position))

        isSelected = position == selectedPosition
        holder.rbBank.isChecked = isSelected
        holder.rbBank.setOnCheckedChangeListener(
            object : CompoundButton.OnCheckedChangeListener {

                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if (isChecked) {
                        val copyOfLastCheckedPosition: Int = selectedPosition
                        selectedPosition = holder.adapterPosition
                        listener.selectedBank(bank = banks.get(selectedPosition))
                        notifyItemChanged(copyOfLastCheckedPosition)
                        notifyItemChanged(selectedPosition)
                    }
                }
            }
        )
    }

    interface Listener {
        fun selectedBank(bank: Bank)
    }

    override fun getItemCount(): Int = banks.size

    fun setItem(banks: List<Bank>) {
        this.banks = banks
    }
}