package com.finalproject.thrifthouse.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.DetailProductActivity
import com.finalproject.thrifthouse.fragment.store.ReviewStoreFragment

class FilterRatingAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<FilterRatingAdapter.ViewHolder>() {
    private var numberRating: List<String> = ArrayList()
    var checkedPosition = 1

    class ViewHolder(itemView: View, listener: Listener): RecyclerView.ViewHolder(itemView){
        val tvNumberRating: TextView = itemView.findViewById(R.id.tv_rating_filter)
        val llItem: LinearLayout = itemView.findViewById(R.id.ll_item_filter_rating)

        fun bind(number: String, checkedPosition: Int){
            tvNumberRating.text = number
            if (checkedPosition != -1) {
                llItem.setBackgroundResource(R.drawable.selector_filter_rating_default)
            }else{
                if (checkedPosition == adapterPosition) {
                    llItem.setBackgroundResource(R.drawable.selector_filter_rating_selected)
                }else{
                    llItem.setBackgroundResource(R.drawable.selector_filter_rating_default)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterRatingAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_filter_rating, parent, false)
        return FilterRatingAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: FilterRatingAdapter.ViewHolder, position: Int) {
        holder.bind(number = numberRating.get(position), checkedPosition)
        holder.itemView.setOnClickListener {
            holder.llItem.setBackgroundResource(R.drawable.selector_filter_rating_selected)

            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position

                var isSelected = 0
                when (checkedPosition){
                    0 -> isSelected = 5
                    1 -> isSelected = 4
                    2 -> isSelected = 3
                    3 -> isSelected = 2
                    4 -> isSelected = 1
                }
                listener.onItemClicked(isSelected)
            }
        }
    }

    interface Listener {
        fun onItemClicked(isSelected: Int)
    }

    override fun getItemCount(): Int = numberRating.size

    fun setRating(numberRating: List<String>){
        this.numberRating = numberRating
    }



}