package com.finalproject.thrifthouse.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.DetailProductActivity
import com.finalproject.thrifthouse.activities.LoginActivity
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.room.FavoriteProductDatabase
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ProductAdapter(private val type: String) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private var productModels: ArrayList<ProductModel> = ArrayList()
    private lateinit var favoriteViewModel: FavoriteViewModel
    private var isLogin = false

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_item_product)
        val tvBrand: TextView = itemView.findViewById(R.id.tv_item_brand)
        val tvName: TextView = itemView.findViewById(R.id.tv_item_name)
        val tvCondition: TextView = itemView.findViewById(R.id.tv_item_condition)
        val tvSize: TextView = itemView.findViewById(R.id.tv_item_size)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_item_price)
        val cdProduct: CardView = itemView.findViewById(R.id.cd_product)
        val ivFavorite: ImageView = itemView.findViewById(R.id.iv_favorite)
        val ivFavoriteChecked: ImageView = itemView.findViewById(R.id.iv_favorite_checked)

        fun bind(
            productModel: ProductModel,
            type: String,
            favoriteViewModel: FavoriteViewModel,
            isLogin: Boolean
        ) {
            val param = cdProduct.layoutParams as ViewGroup.MarginLayoutParams
            if (type.equals("vertical")) {
                param.width = ViewGroup.LayoutParams.MATCH_PARENT
                param.height = ViewGroup.LayoutParams.WRAP_CONTENT
                param.width = 450
                cdProduct.setLayoutParams(param)
                param.setMargins(16, 40, 16, 10)
            } else {
                param.setMargins(0, 0, 16, 1)
            }

            Glide
                .with(itemView.context)
                .load(productModel.photos)
                .centerCrop()
                .into(ivProduct)
            tvBrand.text = productModel.brand
            tvName.text = productModel.name
            tvCondition.text = productModel.condition
            tvSize.text = productModel.size
            tvPrice.text = NumberConversion.conversionToRupiah(productModel.price)

            if (productModel.isFavorite) {
                ivFavorite.visibility = View.GONE
                ivFavoriteChecked.visibility = View.VISIBLE
            } else {
                ivFavorite.visibility = View.VISIBLE
                ivFavoriteChecked.visibility = View.GONE
            }

            ivFavoriteChecked.setOnClickListener {
                favoriteViewModel.postProductFavorite(productModel.id)
                favoriteViewModel.deleteFavorite(productModel.id)
                ivFavorite.visibility = View.VISIBLE
                ivFavoriteChecked.visibility = View.GONE
            }

            ivFavorite.setOnClickListener {
                if (isLogin) {
                    favoriteViewModel.postProductFavorite(productModel.id)
                    ivFavorite.visibility = View.GONE
                    ivFavoriteChecked.visibility = View.VISIBLE
                } else {
                    val intent = Intent(itemView.context, LoginActivity::class.java)
                    itemView.context.startActivity(intent)
                }
            }

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, DetailProductActivity::class.java)
                intent.putExtra(KEY_ID_PRODUCT, productModel.id)
                itemView.context.startActivity(intent)
            }
        }
    }

    @NonNull
    @Override
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productModels.size
    }

    fun setItem(
        favoriteViewModel: FavoriteViewModel,
        productModels: ArrayList<ProductModel>,
        isLogin: Boolean
    ) {
        this.favoriteViewModel = favoriteViewModel
        this.productModels = productModels
        this.isLogin = isLogin
        notifyDataSetChanged()
    }

    companion object {
        val KEY_ID_PRODUCT = "key_id_product"
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(productModels.get(position), type, favoriteViewModel, isLogin)
    }
}