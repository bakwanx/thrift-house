package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.Bank
import com.finalproject.thrifthouse.api.response.Store
import de.hdodenhof.circleimageview.CircleImageView

class FavoriteStoreAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<FavoriteStoreAdapter.ViewHolder>() {
    private var storeModels: List<Store> = ArrayList()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivStore: CircleImageView = itemView.findViewById(R.id.iv_favorite_store_photo)
        val tvStoreName: TextView = itemView.findViewById(R.id.tv_store_name)
        val tvStoreLocation: TextView = itemView.findViewById(R.id.tv_store_favorite_location)
        val tvRemoveStore: TextView = itemView.findViewById(R.id.tv_store_favorite)

        fun bind(store: Store, listener: Listener){
            Glide
                .with(itemView.context)
                .load(store.logo)
                .centerCrop()
                .into(ivStore)
            tvStoreName.text = store.name
            tvStoreLocation.text = "${store.city}, ${store.province}"
            tvRemoveStore.setOnClickListener {
                listener.unFavorite(store.id)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_store, parent, false)
        return FavoriteStoreAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(storeModels.get(position), listener)
    }

    override fun getItemCount(): Int = storeModels.size

    fun setFavoriteStore(storeModels: List<Store>){
        this.storeModels = storeModels
    }

    interface Listener {
        fun unFavorite(storeId: String)
    }
}