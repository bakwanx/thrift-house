package com.finalproject.thrifthouse.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.Address


class AddressAdapter internal constructor(private val listener: AddressAdapter.Listener): RecyclerView.Adapter<AddressAdapter.ViewHolder>() {
    private var listAddress: List<Address> = ArrayList()
    var checkedPosition = 0
    private var isChecked = false
    var selectedPosition = -1

    class ViewHolder(itemView: View, listener: Listener): RecyclerView.ViewHolder(itemView) {
        val rbAddress: RadioButton = itemView.findViewById(R.id.rb_address)
        val tvPrimary: TextView = itemView.findViewById(R.id.tv_primary_address)
        val tvNameRecipient: TextView = itemView.findViewById(R.id.tv_name_recipient)
        val tvPhone: TextView = itemView.findViewById(R.id.tv_phone_address)
        val tvAddress: TextView = itemView.findViewById(R.id.tv_detail_address)

        fun bind(address: Address, checkedPosition: Int, isChecked: Boolean){
            tvNameRecipient.text = address.recipientName
            tvPhone.text = "+62 ${address.recipientPhone}"
            tvAddress.text = address.fullAddress

            if(isChecked == false){
                rbAddress.isChecked = true
                tvPrimary.text = "(Utama)"
            }

            if (checkedPosition != -1) {
                tvPrimary.text = ""
                rbAddress.isChecked = false
            }else{
                if (checkedPosition == adapterPosition) {
                    tvPrimary.text = "(Utama)"
                    rbAddress.isChecked = true
                }else{
                    tvPrimary.text = ""
                    rbAddress.isChecked = false
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_address, parent, false)
        return AddressAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: AddressAdapter.ViewHolder, position: Int) {
        holder.bind(listAddress.get(position), checkedPosition, isChecked)

        isChecked = position == selectedPosition
        holder.rbAddress.isChecked = isChecked
        holder.rbAddress.setOnCheckedChangeListener(
            object: CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if(isChecked){
                        holder.tvPrimary.text = "(Utama)"
                        val copyOfLastCheckedPosition: Int = selectedPosition
                        selectedPosition = holder.adapterPosition
                        notifyItemChanged(copyOfLastCheckedPosition)
                        notifyItemChanged(selectedPosition)
                        listener.selectedAddress(listAddress.get(position))
                    }
                }
            }
        )
    }

    interface Listener{
        fun selectedAddress(address: Address)
    }

    override fun getItemCount(): Int = listAddress.size

    fun setItem(listAddress: List<Address>){
        this.listAddress = listAddress
    }
}