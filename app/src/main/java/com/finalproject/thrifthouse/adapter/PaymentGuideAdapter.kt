package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R

class PaymentGuideAdapter(val guidePayments: List<String>): RecyclerView.Adapter<PaymentGuideAdapter.ViewHolder>() {

    var selectedItem = 0

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvPaymentGuide: TextView = itemView.findViewById(R.id.tv_sub_category)
        val ivPaymentGuide: ImageView = itemView.findViewById(R.id.iv_sub_category_arrow)
        fun bind(guidePayment: String, selectedItem: Int){

            tvPaymentGuide.text = guidePayment
            if(selectedItem == -1){
                ivPaymentGuide.setImageResource(R.drawable.ic_keyboard_arrow_down_24)
            }else{
                if (selectedItem == adapterPosition){
                    ivPaymentGuide.setImageResource(R.drawable.ic_keyboard_arrow_up_24)
                }else{
                    ivPaymentGuide.setImageResource(R.drawable.ic_keyboard_arrow_down_24)
                }
            }
            ivPaymentGuide.setImageResource(R.drawable.ic_keyboard_arrow_down_24)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PaymentGuideAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sub_category, parent, false)
        return PaymentGuideAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaymentGuideAdapter.ViewHolder, position: Int) {
        holder.bind(guidePayments.get(position), selectedItem)
        holder.itemView.setOnClickListener {
            holder.ivPaymentGuide.setImageResource(R.drawable.ic_keyboard_arrow_up_24)
            if(selectedItem != position){
                notifyItemChanged(selectedItem)
                selectedItem = position
            }
        }
    }

    override fun getItemCount(): Int {
        return guidePayments.size
    }
}