package com.finalproject.thrifthouse.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.model.CartModel

class FilterCategoryAdapter internal constructor(private val listener: Listener) :

    RecyclerView.Adapter<FilterCategoryAdapter.ViewHolder>() {
    private var category: List<String> = ArrayList()
    var checkedPosition = 1

    class ViewHolder(itemView: View, listener: Listener) : RecyclerView.ViewHolder(itemView) {

        val tvProductFilter: TextView = itemView.findViewById(R.id.tv_product_filter)
        val rlOne: RelativeLayout = itemView.findViewById(R.id.rl_one)

        fun bind(category: String, checkedPosition: Int, listener: Listener) {
            tvProductFilter.text = category
            if (checkedPosition != -1) {
                rlOne.setBackgroundResource(R.drawable.border_searchview)
                tvProductFilter.setTextColor(Color.BLACK)
            } else {
                if (checkedPosition == adapterPosition) {
                    rlOne.setBackgroundResource(R.drawable.border_selected)
                    tvProductFilter.setTextColor(Color.WHITE)
                    listener.selectedCategory(category)
                } else {
                    rlOne.setBackgroundResource(R.drawable.border_searchview)
                    tvProductFilter.setTextColor(Color.BLACK)
                }
            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterCategoryAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_filter_product, parent, false)
        return FilterCategoryAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: FilterCategoryAdapter.ViewHolder, position: Int) {
        holder.bind(category.get(position), checkedPosition, listener)
        holder.itemView.setOnClickListener {
            holder.rlOne.setBackgroundResource(R.drawable.border_selected)
            holder.tvProductFilter.setTextColor(Color.WHITE)
            listener.selectedCategory(category.get(position))
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position
            } else {
                holder.rlOne.setBackgroundResource(R.drawable.border_searchview)
                holder.tvProductFilter.setTextColor(Color.BLACK)
                checkedPosition = 1
                listener.selectedCategory("")
            }
        }
    }

    override fun getItemCount(): Int = category.size

    fun setItem(category: List<String>) {
        this.category = category
    }

    interface Listener {
        fun selectedCategory(category: String?)
    }
}

class FilterBrandAdapter internal constructor(private val listener: Listener) :
    RecyclerView.Adapter<FilterBrandAdapter.ViewHolder>() {
    private var brand: List<String> = ArrayList()
    var checkedPosition = 1

    class ViewHolder(itemView: View, listener: Listener) : RecyclerView.ViewHolder(itemView) {

        val tvProductFilter: TextView = itemView.findViewById(R.id.tv_product_filter)
        val rlOne: RelativeLayout = itemView.findViewById(R.id.rl_one)

        fun bind(category: String, checkedPosition: Int, listener: Listener) {
            tvProductFilter.text = category
            if (checkedPosition != -1) {
                rlOne.setBackgroundResource(R.drawable.border_searchview)
                tvProductFilter.setTextColor(Color.BLACK)
            } else {
                if (checkedPosition == adapterPosition) {
                    rlOne.setBackgroundResource(R.drawable.border_selected)
                    tvProductFilter.setTextColor(Color.WHITE)
                    listener.selectedBrand(category)
                } else {
                    rlOne.setBackgroundResource(R.drawable.border_searchview)
                    tvProductFilter.setTextColor(Color.BLACK)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterBrandAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_filter_product, parent, false)
        return FilterBrandAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: FilterBrandAdapter.ViewHolder, position: Int) {
        holder.bind(brand.get(position), checkedPosition, listener)
        holder.itemView.setOnClickListener {
            holder.rlOne.setBackgroundResource(R.drawable.border_selected)
            holder.tvProductFilter.setTextColor(Color.WHITE)
            listener.selectedBrand(brand.get(position))
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position
            } else {
                holder.rlOne.setBackgroundResource(R.drawable.border_searchview)
                holder.tvProductFilter.setTextColor(Color.BLACK)
                checkedPosition = 1
                listener.selectedBrand("")
            }
        }
    }

    override fun getItemCount(): Int = brand.size

    fun setItem(brand: List<String>) {
        this.brand = brand
    }

    interface Listener {
        fun selectedBrand(brand: String?)
    }
}

class FilterSizeAdapter internal constructor(private val listener: Listener) :
    RecyclerView.Adapter<FilterSizeAdapter.ViewHolder>() {
    private var size: List<String> = ArrayList()
    var checkedPosition = 1

    class ViewHolder(itemView: View, listener: Listener) : RecyclerView.ViewHolder(itemView) {
        val tvProductFilter: TextView = itemView.findViewById(R.id.tv_product_filter)
        val rlOne: RelativeLayout = itemView.findViewById(R.id.rl_one)

        fun bind(size: String, checkedPosition: Int, listener: Listener) {
            tvProductFilter.text = size
            if (checkedPosition != -1) {
                rlOne.setBackgroundResource(R.drawable.border_searchview)
                tvProductFilter.setTextColor(Color.BLACK)
            } else {
                if (checkedPosition == adapterPosition) {
                    rlOne.setBackgroundResource(R.drawable.border_selected)
                    tvProductFilter.setTextColor(Color.WHITE)
                    listener.selectedSize(size)
                } else {
                    rlOne.setBackgroundResource(R.drawable.border_searchview)
                    tvProductFilter.setTextColor(Color.BLACK)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterSizeAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_filter_product, parent, false)
        return FilterSizeAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: FilterSizeAdapter.ViewHolder, position: Int) {
        holder.bind(size.get(position), checkedPosition, listener)
        holder.itemView.setOnClickListener {
            holder.rlOne.setBackgroundResource(R.drawable.border_selected)
            holder.tvProductFilter.setTextColor(Color.WHITE)
            listener.selectedSize(size.get(position))
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position
            } else {
                holder.rlOne.setBackgroundResource(R.drawable.border_searchview)
                holder.tvProductFilter.setTextColor(Color.BLACK)
                checkedPosition = 1
                listener.selectedSize("")
            }
        }
    }

    override fun getItemCount(): Int = size.size

    fun setItem(size: List<String>) {
        this.size = size
    }

    interface Listener {
        fun selectedSize(size: String?)
    }
}

class FilterConditionAdapter internal constructor(private val listener: Listener) :
    RecyclerView.Adapter<FilterConditionAdapter.ViewHolder>() {
    private var condition: List<String> = ArrayList()
    var checkedPosition = 1

    class ViewHolder(itemView: View, listener: Listener) : RecyclerView.ViewHolder(itemView) {

        val tvProductFilter: TextView = itemView.findViewById(R.id.tv_product_filter)
        val rlOne: RelativeLayout = itemView.findViewById(R.id.rl_one)

        fun bind(condition: String, checkedPosition: Int, listener: Listener) {
            tvProductFilter.text = condition
            if (checkedPosition != -1) {
                rlOne.setBackgroundResource(R.drawable.border_searchview)
                tvProductFilter.setTextColor(Color.BLACK)
            } else {
                if (checkedPosition == adapterPosition) {
                    rlOne.setBackgroundResource(R.drawable.border_selected)
                    tvProductFilter.setTextColor(Color.WHITE)
                    listener.selectedCondition(condition = condition)
                } else {
                    rlOne.setBackgroundResource(R.drawable.border_searchview)
                    tvProductFilter.setTextColor(Color.BLACK)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterConditionAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_filter_product, parent, false)
        return FilterConditionAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: FilterConditionAdapter.ViewHolder, position: Int) {
        holder.bind(condition.get(position), checkedPosition, listener)
        holder.itemView.setOnClickListener {
            holder.rlOne.setBackgroundResource(R.drawable.border_selected)
            holder.tvProductFilter.setTextColor(Color.WHITE)
            listener.selectedCondition(condition.get(position))
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position
            } else {
                holder.rlOne.setBackgroundResource(R.drawable.border_searchview)
                holder.tvProductFilter.setTextColor(Color.BLACK)
                checkedPosition = 1
                listener.selectedCondition("")
            }
        }
    }

    override fun getItemCount(): Int = condition.size

    fun setItem(condition: List<String>) {
        this.condition = condition
    }

    interface Listener {
        fun selectedCondition(condition: String?)
    }
}

class FilterOrderAdapter internal constructor(private val listener: Listener) :

    RecyclerView.Adapter<FilterOrderAdapter.ViewHolder>() {
    private var category: List<String> = ArrayList()
    var checkedPosition = 1

    class ViewHolder(itemView: View, listener: Listener) : RecyclerView.ViewHolder(itemView) {

        val tvProductFilter: TextView = itemView.findViewById(R.id.tv_product_filter)
        val rlOne: RelativeLayout = itemView.findViewById(R.id.rl_one)

        fun bind(category: String, checkedPosition: Int, listener: Listener) {
            tvProductFilter.text = category
            if (checkedPosition != -1) {
                rlOne.setBackgroundResource(R.drawable.border_searchview)
                tvProductFilter.setTextColor(Color.BLACK)
            } else {
                if (checkedPosition == adapterPosition) {
                    rlOne.setBackgroundResource(R.drawable.border_selected)
                    tvProductFilter.setTextColor(Color.WHITE)
                    listener.selectedCategory(category)
                } else {
                    rlOne.setBackgroundResource(R.drawable.border_searchview)
                    tvProductFilter.setTextColor(Color.BLACK)
                }
            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterOrderAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_filter_product, parent, false)
        return FilterOrderAdapter.ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: FilterOrderAdapter.ViewHolder, position: Int) {
        holder.bind(category.get(position), checkedPosition, listener)
        holder.itemView.setOnClickListener {
            holder.rlOne.setBackgroundResource(R.drawable.border_selected)
            holder.tvProductFilter.setTextColor(Color.WHITE)
            listener.selectedCategory(category.get(position))
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition)
                checkedPosition = position
            } else {
                holder.rlOne.setBackgroundResource(R.drawable.border_searchview)
                holder.tvProductFilter.setTextColor(Color.BLACK)
                checkedPosition = 1
                listener.selectedCategory("")
            }
        }
    }

    override fun getItemCount(): Int = category.size

    fun setItem(category: List<String>) {
        this.category = category
    }

    interface Listener {
        fun selectedCategory(category: String?)
    }
}