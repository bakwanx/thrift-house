package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.utilities.NumberConversion

class CheckoutProductAdapter: RecyclerView.Adapter<CheckoutProductAdapter.ViewHolder>() {
    private var productCarts: List<ProductCart> = ArrayList()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_product_photo_cart)
        val tvBrand: TextView = itemView.findViewById(R.id.tv_brand)
        val tvName: TextView = itemView.findViewById(R.id.tv_name)
        val tvCondition: TextView = itemView.findViewById(R.id.tv_tag_condition)
        val tvSize: TextView = itemView.findViewById(R.id.tv_tag_size)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_price)

        fun bind(productCart: ProductCart){
            Glide
                .with(itemView.context)
                .load(productCart.photo)
                .centerCrop()
                .into(ivProduct)
            tvBrand.text = productCart.brand
            tvName.text = productCart.name
            tvCondition.text = productCart.condition
            tvSize.text = productCart.size
            tvPrice.text = NumberConversion.conversionToRupiah(productCart.price.toDouble())
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CheckoutProductAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_checkout, parent, false)
        return CheckoutProductAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CheckoutProductAdapter.ViewHolder, position: Int) {
        holder.bind(productCarts[position])
    }

    override fun getItemCount(): Int = productCarts.size

    fun setItem(productCarts: List<ProductCart>){
        this.productCarts = productCarts
    }
}