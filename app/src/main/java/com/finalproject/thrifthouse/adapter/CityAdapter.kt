package com.finalproject.thrifthouse.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.AddAddressActivity
import com.finalproject.thrifthouse.api.response.City
import com.finalproject.thrifthouse.api.response.ResultProvinceCity
import java.io.Serializable


class CityAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<CityAdapter.ViewHolder>()  {
    private var city: List<City> = ArrayList()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvLocation: TextView = itemView.findViewById(R.id.tv_address_location)

        fun bind(city: City, listener: Listener){

            val resultProvinceCity = ResultProvinceCity(
                city_id = city.city_id,
                province_id = city.province_id,
                province= city.province,
                type = city.type,
                city_name = city.city_name,
                postal_code = city.postal_code
            )

            tvLocation.text = city.city_name

            itemView.setOnClickListener {
                listener.selectedCity(city)
//                val intent = Intent(itemView.context, AddAddressActivity::class.java)
//                intent.putExtra(KEY_ADDRESS, resultProvinceCity)
//                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CityAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_province_city, parent, false)
        return CityAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CityAdapter.ViewHolder, position: Int) {
        holder.bind(city.get(position), listener)
    }

    interface Listener{
        fun selectedCity(city: City)
    }

    override fun getItemCount(): Int = city.size

    fun setCity(city: List<City>){
        this.city = city
    }

    companion object{
        val KEY_ADDRESS = "address_key"
    }
}