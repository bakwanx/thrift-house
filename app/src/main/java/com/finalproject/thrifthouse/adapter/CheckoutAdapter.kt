package com.finalproject.thrifthouse.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ResultCostDeliveryService
import com.finalproject.thrifthouse.api.response.ResultDeliveryService
import com.finalproject.thrifthouse.model.CheckOutModel
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.viewmodel.CheckoutActivityViewModel
import de.hdodenhof.circleimageview.CircleImageView

class CheckoutAdapter internal constructor(
    private val listener: Listener
) : RecyclerView.Adapter<CheckoutAdapter.ViewHolder>() {

    private var selectedPosition = -1
    private var checkOutModels: List<CheckOutModel> = ArrayList()
    private lateinit var resultCostDeliveryService: ResultCostDeliveryService

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val ivProduct: CircleImageView = itemView.findViewById(R.id.iv_store_logo_checkout)
        val rvProduct: RecyclerView = itemView.findViewById(R.id.rv_product_checkout)
        val tvName: TextView = itemView.findViewById(R.id.tv_store_name_checkout)
        val tvAddress: TextView = itemView.findViewById(R.id.tv_store_address_checkout)
        var checkoutProductAdapter: CheckoutProductAdapter = CheckoutProductAdapter()
        val tvCourier: TextView = itemView.findViewById(R.id.tv_courier)
        val tvDeliveryService: TextView = itemView.findViewById(R.id.tv_delivery_service)
        val tvCostDeliveryService: TextView = itemView.findViewById(R.id.tv_cost_expedition)
        val tvEstimated: TextView = itemView.findViewById(R.id.tv_prediction_arrived)

        fun bind(
            checkOutModel: CheckOutModel
        ) {
            checkOutModel.resultDeliveryService
            Glide
                .with(itemView.context)
                .load(checkOutModel.photo)
                .centerCrop()
                .into(ivProduct)
            tvName.text = checkOutModel.name
            tvAddress.text = "${checkOutModel.city}, ${checkOutModel.province}"
            tvCourier.text = checkOutModel.resultDeliveryService.name
            rvProduct.adapter = checkoutProductAdapter
            checkoutProductAdapter.setItem(checkOutModel.product)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_checkout_store, parent, false)
        return CheckoutAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CheckoutAdapter.ViewHolder, position: Int) {
        holder.bind(checkOutModel = checkOutModels.get(position))

        holder.itemView.setOnClickListener {
            selectedPosition = holder.adapterPosition
            listener.chooseDeliveryService(
                position,
                checkOutModels.get(position).resultDeliveryService
            )
        }

        holder.tvDeliveryService.text = "Pilih service pengiriman"
        holder.tvCostDeliveryService.visibility = View.GONE
        holder.tvEstimated.visibility = View.GONE

        if (position == selectedPosition) {
            holder.tvDeliveryService.text = resultCostDeliveryService.service
            holder.tvCostDeliveryService.text =
                NumberConversion.conversionToRupiah(resultCostDeliveryService.cost.get(0).value.toDouble() as Double)
            holder.tvEstimated.text = "Akan diterima setelah ${resultCostDeliveryService.cost.get(0).etd} hari"
            holder.tvCostDeliveryService.visibility = View.VISIBLE
            holder.tvEstimated.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int = checkOutModels.size

    fun setItem(checkOutModels: List<CheckOutModel>) {
        this.checkOutModels = checkOutModels
    }

    interface Listener {
        fun chooseDeliveryService(position: Int, resultDeliveryService: ResultDeliveryService)
    }

    fun setDeliveryService(
        resultCostDeliveryService: ResultCostDeliveryService
    ) {
        this.resultCostDeliveryService = resultCostDeliveryService
    }
}