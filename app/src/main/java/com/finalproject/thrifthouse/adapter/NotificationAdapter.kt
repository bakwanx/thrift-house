package com.finalproject.thrifthouse.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.Notification
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter

class NotificationAdapter : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    private var notifications: List<Notification> = ArrayList()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val rlInfo: RelativeLayout = itemView.findViewById(R.id.rl_one)
        val cvProduct: CardView = itemView.findViewById(R.id.cv_product)
        val ivProduct: ImageView = itemView.findViewById(R.id.iv_notification_product)
        val ivNotification: ImageView = itemView.findViewById(R.id.iv_notification_info)
        val tvType: TextView = itemView.findViewById(R.id.tv_type_notification)
        val tvTitle: TextView = itemView.findViewById(R.id.tv_title_notification)
        val tvBody: TextView = itemView.findViewById(R.id.tv_body_notification)
        val tvDateTime: TextView = itemView.findViewById(R.id.tv_datetime)

        fun bind(notification: Notification) {

            if (notification.type.equals("info")) {
                cvProduct.visibility = View.GONE
                rlInfo.visibility = View.VISIBLE
                ivNotification.setImageResource(R.drawable.ic_info)
            } else {
                cvProduct.visibility = View.VISIBLE
                rlInfo.visibility = View.GONE
                Glide
                    .with(itemView.context)
                    .load(notification.image)
                    .centerCrop()
                    .into(ivProduct)
            }

            tvType.text = notification.type
            tvTitle.text = notification.title
            tvBody.text = notification.body

            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val outputDate: String = formatter.format(parser.parse(notification.notificationAt))
            tvDateTime.text = outputDate
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return NotificationAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(notification = notifications.get(position))
    }

    override fun getItemCount(): Int = notifications.size

    fun setItem(notifications: List<Notification>) {
        this.notifications = notifications
    }
}