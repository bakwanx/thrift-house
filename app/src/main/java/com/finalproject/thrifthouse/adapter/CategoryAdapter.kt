package com.finalproject.thrifthouse.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.finalproject.thrifthouse.fragment.category.ChildrenFragment
import com.finalproject.thrifthouse.fragment.category.FemaleFragment
import com.finalproject.thrifthouse.fragment.category.MaleFragment
import com.finalproject.thrifthouse.fragment.category.ProductFragment

class CategoryAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

    private val pages = listOf(
        ProductFragment(),
        MaleFragment(),
        FemaleFragment(),
        ChildrenFragment(),
    )

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Semua"
            1 -> "Pria"
            2 -> "Wanita"
            else -> "Anak-anak"
        }
    }

}