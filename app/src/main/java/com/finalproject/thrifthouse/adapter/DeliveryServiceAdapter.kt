package com.finalproject.thrifthouse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.api.response.ResultCostDeliveryService
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.utilities.NumberConversion

class DeliveryServiceAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<DeliveryServiceAdapter.ViewHolder>() {

    private var resultCostDeliveryService: List<ResultCostDeliveryService> = ArrayList()
    private var positionStore = -1

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val tvDeliveryService: TextView = itemView.findViewById(R.id.tv_delivery_service)
        val tvCost: TextView = itemView.findViewById(R.id.tv_cost_delivery_service)
        val tvEstimated: TextView = itemView.findViewById(R.id.tv_estimated)

        fun bind(resultCostDeliveryService: ResultCostDeliveryService, listener: Listener, positionStore: Int){
            tvDeliveryService.text = resultCostDeliveryService.service
            tvCost.text = NumberConversion.conversionToRupiah(resultCostDeliveryService.cost!!.get(0).value.toDouble())
            tvEstimated.text = "Estimasi ${resultCostDeliveryService.cost.get(0).etd} hari"

            itemView.setOnClickListener {
                listener.selectDeliveryService(positionStore, resultCostDeliveryService)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryServiceAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_delivery_service, parent, false)
        return DeliveryServiceAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(resultCostDeliveryService = resultCostDeliveryService.get(position), listener, positionStore)
    }

    override fun getItemCount(): Int = resultCostDeliveryService.size

    interface Listener{
        fun selectDeliveryService(positionStore: Int, resultCostDeliveryService: ResultCostDeliveryService)
    }

    fun setItem(position: Int, resultCostDeliveryService: List<ResultCostDeliveryService>){
        this.resultCostDeliveryService = resultCostDeliveryService
        this.positionStore = position
    }

}