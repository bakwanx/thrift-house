package com.finalproject.thrifthouse.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.CheckoutActivity
import com.finalproject.thrifthouse.activities.DetailTransactionActivity
import com.finalproject.thrifthouse.activities.PaymentActivity
import com.finalproject.thrifthouse.activities.ReviewActivity
import com.finalproject.thrifthouse.api.response.OrderList

class OrderAdapter internal constructor(private val listener: Listener): RecyclerView.Adapter<OrderAdapter.ViewHolder>() {

    private var orderList: List<OrderList> = ArrayList()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvStore: TextView = itemView.findViewById(R.id.tv_store)
        val tvStatusOrder: TextView = itemView.findViewById(R.id.tv_status_order)
        val rvProductOrder: RecyclerView = itemView.findViewById(R.id.rv_product_order)
        val llUnpaid: LinearLayout = itemView.findViewById(R.id.ll_unpaid)
        val llPacked: LinearLayout = itemView.findViewById(R.id.ll_packed)
        val llSent: LinearLayout = itemView.findViewById(R.id.ll_sent)
        val llDone: LinearLayout = itemView.findViewById(R.id.ll_done)
        val llCancel: LinearLayout = itemView.findViewById(R.id.ll_cancel)
        val productOrderAdapter: ProductOrderAdapter = ProductOrderAdapter()
        val btnDetailTransactionUnpaid: Button = itemView.findViewById(R.id.btn_detail_transaction)
        val btnPayment: Button = itemView.findViewById(R.id.btn_continue_payment)
        val btnDetailTransactionPacked: Button =
            itemView.findViewById(R.id.btn_detail_transaction_packed)
        val btnDetailTransactionSent: Button =
            itemView.findViewById(R.id.btn_detail_transaction_sent)
        val btnOrderAccepted: Button = itemView.findViewById(R.id.btn_order_accepted)
        val btnDetailTransactionDone: Button =
            itemView.findViewById(R.id.btn_detail_transaction_done)
        val btnOrderReview: Button = itemView.findViewById(R.id.btn_order_review)
        val btnDetailTransactionCancel: Button =
            itemView.findViewById(R.id.btn_detail_transaction_cancel)

        fun bind(orderList: OrderList) {
            tvStore.text = orderList.store.get(0).name
            tvStatusOrder.text = orderList.packagingStatus

            rvProductOrder.adapter = productOrderAdapter
            productOrderAdapter.setItem(orderList.store.get(0).products)
            Log.d("TAG", "bind status: ${orderList.packagingStatus}")
            if (orderList.status.equals("Menunggu pembayaran")) {
                tvStatusOrder.text = orderList.status
                tvStatusOrder.setBackgroundResource(R.drawable.status_unpaid_order)
                llPacked.visibility = View.GONE
                llSent.visibility = View.GONE
                llDone.visibility = View.GONE
                llCancel.visibility = View.GONE
            } else if(orderList.status.equals("Pembayaran terkonfirmasi")){
                tvStatusOrder.text = orderList.status
                tvStatusOrder.setBackgroundResource(R.drawable.status_unpaid_order)
                llPacked.visibility = View.GONE
                llSent.visibility = View.GONE
                llDone.visibility = View.GONE
                llCancel.visibility = View.GONE
            } else if (orderList.packagingStatus.equals("Pesanan dikemas")) {
                tvStatusOrder.setTextColor(itemView.resources.getColor(R.color.white))
                tvStatusOrder.setBackgroundResource(R.drawable.status_process_order)
                llUnpaid.visibility = View.GONE
                llPacked.visibility = View.VISIBLE
                llSent.visibility = View.GONE
                llDone.visibility = View.GONE
                llCancel.visibility = View.GONE
            } else if (orderList.packagingStatus.equals("Pesanan dikirim")) {
                tvStatusOrder.setTextColor(itemView.resources.getColor(R.color.white))
                tvStatusOrder.setBackgroundResource(R.drawable.status_process_order)
                llUnpaid.visibility = View.GONE
                llPacked.visibility = View.GONE
                llSent.visibility = View.VISIBLE
                llDone.visibility = View.GONE
                llCancel.visibility = View.GONE
            } else if (orderList.packagingStatus.equals("Pesanan diterima")) {
                tvStatusOrder.setTextColor(itemView.resources.getColor(R.color.white))
                tvStatusOrder.text = orderList.status
                tvStatusOrder.setBackgroundResource(R.drawable.status_done_order)
                llUnpaid.visibility = View.GONE
                llPacked.visibility = View.GONE
                llSent.visibility = View.GONE
                llDone.visibility = View.VISIBLE
                llCancel.visibility = View.GONE
            } else {
                tvStatusOrder.setTextColor(itemView.resources.getColor(R.color.white))
                tvStatusOrder.setBackgroundResource(R.drawable.status_cancel_order)
                llUnpaid.visibility = View.GONE
                llPacked.visibility = View.GONE
                llSent.visibility = View.GONE
                llDone.visibility = View.GONE
                llCancel.visibility = View.VISIBLE
            }
        }

        fun action(orderList: OrderList, listener: Listener) {
            btnDetailTransactionUnpaid.setOnClickListener {
                val intent = Intent(itemView.context, DetailTransactionActivity::class.java)
                intent.putExtra(DetailTransactionActivity.KEY_ID_ORDER, orderList.id)
                intent.putExtra(DetailTransactionActivity.KEY_ID_STATUS, orderList.packagingStatus)
                itemView.context.startActivity(intent)
            }
            btnPayment.setOnClickListener {
                val intent = Intent(itemView.context, PaymentActivity::class.java)
                intent.putExtra(CheckoutActivity.KEY_PAYMENT_ID, orderList.id)
                itemView.context.startActivity(intent)
            }
            btnDetailTransactionPacked.setOnClickListener {
                val intent = Intent(itemView.context, DetailTransactionActivity::class.java)
                intent.putExtra(DetailTransactionActivity.KEY_ID_ORDER, orderList.id)
                intent.putExtra(DetailTransactionActivity.KEY_ID_STATUS, orderList.packagingStatus)
                itemView.context.startActivity(intent)
            }
            btnDetailTransactionSent.setOnClickListener {
                val intent = Intent(itemView.context, DetailTransactionActivity::class.java)
                intent.putExtra(DetailTransactionActivity.KEY_ID_ORDER, orderList.id)
                intent.putExtra(DetailTransactionActivity.KEY_ID_STATUS, orderList.packagingStatus)
                itemView.context.startActivity(intent)
            }
            btnOrderAccepted.setOnClickListener {
                listener.onClickAcceptOrder(orderList.id)
            }
            btnDetailTransactionDone.setOnClickListener {
                val intent = Intent(itemView.context, DetailTransactionActivity::class.java)
                intent.putExtra(DetailTransactionActivity.KEY_ID_ORDER, orderList.id)
                intent.putExtra(DetailTransactionActivity.KEY_ID_STATUS, orderList.packagingStatus)
                itemView.context.startActivity(intent)
            }
            btnOrderReview.setOnClickListener {
                val intent = Intent(itemView.context, ReviewActivity::class.java)
                intent.putExtra(ReviewActivity.KEY_ID_ORDER, orderList.id)
                itemView.context.startActivity(intent)
            }
            btnDetailTransactionCancel.setOnClickListener {
                val intent = Intent(itemView.context, DetailTransactionActivity::class.java)
                intent.putExtra(DetailTransactionActivity.KEY_ID_ORDER, orderList.id)
                intent.putExtra(DetailTransactionActivity.KEY_ID_STATUS, orderList.packagingStatus)
                itemView.context.startActivity(intent)
            }
        }
    }

    interface Listener {
        fun onClickAcceptOrder(transactionId: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_order, parent, false)
        return OrderAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: OrderAdapter.ViewHolder, position: Int) {
        holder.bind(orderList.get(position))
        holder.action(orderList.get(position), listener)
    }

    override fun getItemCount(): Int = orderList.size

    fun setItem(orderList: List<OrderList>) {
        this.orderList = orderList
    }
}