package com.finalproject.thrifthouse.model

import java.io.Serializable

data class UserModel (
    val birth: String,
    val email: String,
    val fullname: String,
    val gender: String,
    val id: String,
    val password: String,
    val phone: String,
    val profileImg: String,
    val role: String,
    val username: String
): Serializable
