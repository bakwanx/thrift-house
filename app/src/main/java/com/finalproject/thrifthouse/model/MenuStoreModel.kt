package com.finalproject.thrifthouse.model

import kotlinx.serialization.Serializable

data class MenuStoreModel (
    val icImg: Int,
    val desc: String,
    val number: String
)