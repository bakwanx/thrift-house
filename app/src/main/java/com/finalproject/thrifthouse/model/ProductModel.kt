package com.finalproject.thrifthouse.model

import androidx.room.ColumnInfo

class ProductModel(
    val id: String,
    val name: String,
    val brand: String,
    val size: String,
    val condition: String,
    val price: Double,
    val photos: String,
    val category: String = "",
    var isFavorite: Boolean = false
)