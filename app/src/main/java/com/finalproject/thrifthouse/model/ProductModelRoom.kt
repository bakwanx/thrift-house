package com.finalproject.thrifthouse.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class ProductModelRoom(
    @PrimaryKey var id: String,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "brand") var brand: String,
    @ColumnInfo(name = "size") var size: String,
    @ColumnInfo(name = "condition") var condition: String,
    @ColumnInfo(name = "price") var price: Double,
    @ColumnInfo(name = "photos") var photos: String,
    @ColumnInfo(name = "category") var category: String
): Parcelable