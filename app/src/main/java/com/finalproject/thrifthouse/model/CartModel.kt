package com.finalproject.thrifthouse.model

import com.finalproject.thrifthouse.api.response.ProductCart

data class CartModel(
    val id: String,
    val name: String,
    val province: String,
    val province_id: String,
    val city: String,
    val city_id: String,
    val photo: String,
    val product: List<ProductCart>
): java.io.Serializable


