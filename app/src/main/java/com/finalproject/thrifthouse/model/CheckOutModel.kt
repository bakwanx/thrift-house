package com.finalproject.thrifthouse.model

import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.api.response.ResultDeliveryService

data class CheckOutModel(
    val id: String,
    val name: String,
    val province: String,
    val province_id: String,
    val city: String,
    val city_id: String,
    val photo: String,
    val product: List<ProductCart>,
    val resultDeliveryService: ResultDeliveryService
)

data class PostCheckOutModel (
    val userId: String,
    val addressId: String,
    val bankId: String,
    val storeTransactions: List<StoreTransaction>
)

data class StoreTransaction (
    val productIds: List<String>,
    val deliveryService: String,
    val shippingCost: String,
    val shippingService: String,
    val estimatedTimeOfDeparture: String
)

