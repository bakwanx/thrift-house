package com.finalproject.thrifthouse.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.repository.StoreRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class ProductStoreFragmentViewModel(
    private val repository: StoreRepository,
    private val favoriteDao: FavoriteDao
    ): ViewModel() {

    private val _getStoreProducts = MutableLiveData<ArrayList<ProductModel>>()
    val getStoreProducts: LiveData<ArrayList<ProductModel>> = _getStoreProducts
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()

    fun getStoreProducts(storeId: String,page: Int = 0, size: Int = 20){

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getStoreProducts(storeId, page = page, size = size)
                    if (response.isSuccessful){
                        val result = checkFavoriteProduct(favoriteDao = favoriteDao, listProduct = null, listProductStore = response.body()!!.listProduct)
                        _getStoreProducts.postValue(result)
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean>{
        _isLoading.postValue(isLoading)
        return _isLoading
    }

}