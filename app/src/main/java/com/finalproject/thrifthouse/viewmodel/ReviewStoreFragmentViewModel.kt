package com.finalproject.thrifthouse.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.ReviewResponse
import com.finalproject.thrifthouse.repository.StoreRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class ReviewStoreFragmentViewModel(private val repository: StoreRepository): ViewModel() {
    private val _getAllReview = MutableLiveData<ReviewResponse>()
    val getAllReview: LiveData<ReviewResponse> = _getAllReview
    private val _errorMessage = MutableLiveData<String>()

    fun getAllReview(storeId: String,page: Int = 0, size: Int = 20, rate: Int?, hasPhoto: Boolean = true){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getAllReview(storeId, page = page, size = size, rate, hasPhoto)
                    if(response.isSuccessful){
                        _getAllReview.postValue(response.body())
                    }else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }
}