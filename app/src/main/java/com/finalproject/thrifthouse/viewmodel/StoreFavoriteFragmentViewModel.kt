package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.FavoriteStore
import com.finalproject.thrifthouse.api.response.FavoriteStoreGetResponse
import com.finalproject.thrifthouse.api.response.StoreProductResponse
import com.finalproject.thrifthouse.repository.StoreRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class StoreFavoriteFragmentViewModel(
    private val repository: StoreRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _getFavoriteStore = MutableLiveData<FavoriteStoreGetResponse>()
    val getFavoriteStore: LiveData<FavoriteStoreGetResponse> = _getFavoriteStore
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()

    fun getStoreFavorite(search: String?){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getFavoriteStore(sharedIdUser.toString(), search)
                    if (response.isSuccessful){
                        _getFavoriteStore.postValue(response.body())
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun postFavoriteStore(storeId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val postFavoriteStore = FavoriteStore(
                        storeId = storeId
                    )
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.postFavoriteStore(sharedIdUser.toString(), postFavoriteStore)
                    Log.d("TAG", "store id: ${postFavoriteStore.storeId}")
                    Log.d("TAG", "postFavoriteStore response: ${response.body()}")


                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }
}