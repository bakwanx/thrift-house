package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.DetailTransactionResponse
import com.finalproject.thrifthouse.api.response.FavoriteProduct
import com.finalproject.thrifthouse.repository.OrderRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class DetailTransactionActivityViewModel(
    private val repository: OrderRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _detailTransaction = MutableLiveData<DetailTransactionResponse>()
    val getDetailTransactions: LiveData<DetailTransactionResponse> = _detailTransaction

    fun getDetailTransaction(
        orderId: String,
        category: String?
    ){
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {

                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getDetailOrder(sharedIdUser.toString(), orderId, category)
                    Log.d("postFavorite", "response post favorite 3 : $response")
                    if(response.isSuccessful){
                        _detailTransaction.postValue(response.body())
                    }else{
                        _detailTransaction.postValue(response.body())
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }
}