package com.finalproject.thrifthouse.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.AboutStoreResponse
import com.finalproject.thrifthouse.api.response.StoreProductResponse
import com.finalproject.thrifthouse.repository.StoreRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class AboutStoreFragmentViewModel(private val repository: StoreRepository): ViewModel() {
    private val _getAboutStore = MutableLiveData<AboutStoreResponse>()
    val getAboutStore: LiveData<AboutStoreResponse> = _getAboutStore
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()

    fun getAboutStore(storeId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getAboutStore(storeId)
                    if (response.isSuccessful){
                        _getAboutStore.postValue(response.body())
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }
}