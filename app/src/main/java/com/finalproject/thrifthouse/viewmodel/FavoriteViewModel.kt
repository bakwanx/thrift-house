package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.FavoriteProduct
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class FavoriteViewModel(
    private val repository: ProductRepository,
    private val sharedPreferences: SharedPreferences,
    private val favoriteDao: FavoriteDao
    ): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _isFavorite = MutableLiveData<Boolean>()
    val getIsFavorite: LiveData<Boolean> = _isFavorite

    fun postProductFavorite(productId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val favoriteProduct = FavoriteProduct(
                        productId
                    )
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.postFavoriteProduct(sharedIdUser.toString(),favoriteProduct)
                    Log.d("postFavorite", "response post favorite 3 : $response")
                    if(response.isSuccessful){
                        _isFavorite.postValue(true)
                    }else{
                        _isFavorite.postValue(false)
                    }
                }catch (throwable: Throwable){
                    _isFavorite.postValue(true)
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun deleteFavorite(id: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                favoriteDao.deleteFavoriteProduct(id)
            }
        }

    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

}