package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.City
import com.finalproject.thrifthouse.api.response.CityRajaOnkgirResponse
import com.finalproject.thrifthouse.api.response.Province
import com.finalproject.thrifthouse.api.response.ProvinceRajaOnkgirResponse
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.repository.AddressRepository
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class AddressViewModel(
    private val repository: AddressRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _getProvince = MutableLiveData<ProvinceRajaOnkgirResponse>()
    private val _setProvinceSelected = MutableLiveData<Province>()
    private val _setCitySelected = MutableLiveData<City>()
    private val _getCity = MutableLiveData<CityRajaOnkgirResponse>()
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()
    val getProvince: LiveData<ProvinceRajaOnkgirResponse> = _getProvince
    val getCities: LiveData<CityRajaOnkgirResponse> = _getCity
    val getProvinceSelected: LiveData<Province> = _setProvinceSelected
    val getCitySelected: LiveData<City> = _setCitySelected

    fun getProvince(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getProvince()
                    if (response.isSuccessful){
                        _getProvince.postValue(response.body())
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getCities(provinceId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getCity(provinceId)
                    Log.d("TAG", "province id: ${response.body()}")
                    if (response.isSuccessful){
                        _getCity.postValue(response.body())
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    Log.d("TAG", "province id: ${throwable.message}")
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun setProvince(province: Province){
        _setProvinceSelected.postValue(province)
    }

    fun setCity(city: City){
        _setCitySelected.postValue(city)
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }
}