package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.repository.StoreRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class DetailStoreActivityViewModel(
    private val repository: StoreRepository,
    private val sharedPreferences: SharedPreferences
    ): ViewModel() {
    private val _getDetailStore = MutableLiveData<StoreResponse>()
    private val _errorMessage = MutableLiveData<String>()
    private val _getBanner = MutableLiveData<BannerResponse>()

    val getDetailStore: LiveData<StoreResponse> = _getDetailStore
    val getBanner: LiveData<BannerResponse> = _getBanner

    fun getDetailStore(storeId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getStore(storeId)
                    if (response.isSuccessful){
                        _getDetailStore.postValue(response.body())
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getBanner(storeId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getBanner(storeId)
                    if (response.isSuccessful){
                        _getBanner.postValue(response.body())
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun postFavoriteStore(storeId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val postFavoriteStore = FavoriteStore(
                      storeId = storeId
                    )
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.postFavoriteStore(sharedIdUser.toString(), postFavoriteStore)
                    Log.d("TAG", "store id: ${postFavoriteStore.storeId}")
                    Log.d("TAG", "postFavoriteStore response: ${response.body()}")


                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }
}