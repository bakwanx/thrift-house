package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.GetUserResponse
import com.finalproject.thrifthouse.repository.UserRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class AccountFragmentViewModel(
    private val userRepository: UserRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _getUserDetail = MutableLiveData<GetUserResponse>()
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()
    val getUserDetail: LiveData<GetUserResponse> = _getUserDetail

    fun getUserDetail(){

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = userRepository.getUser(sharedIdUser.toString())
                    Log.d("TAG", "getUserDetail userId: $sharedIdUser")
                    if (response.isSuccessful){
                        _getUserDetail.postValue(response.body())
                    } else {
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun logOut(){
        sharedPreferences.edit().clear().apply()
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

}