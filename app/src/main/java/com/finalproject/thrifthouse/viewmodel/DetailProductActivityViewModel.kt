package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.Prefs
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class DetailProductActivityViewModel(
    private val repository: ProductRepository,
    private val favoriteDao: FavoriteDao,
    private val sharedPreferences: SharedPreferences
) : ViewModel() {

    private val _getDetailProduct = MutableLiveData<ProductDetailResponse>()
    private val _errorMessage = MutableLiveData<String>()
    private val _getIdStore = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _getStoreProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _getRecomendationProducts = MutableLiveData<ArrayList<ProductModel>>()
    val getStoreProducts: LiveData<ArrayList<ProductModel>> = _getStoreProducts
    val getDetailProduct: LiveData<ProductDetailResponse> = _getDetailProduct
    val getRecomendationProducts: LiveData<ArrayList<ProductModel>> = _getRecomendationProducts
    val getIdStore: LiveData<String> = _getIdStore

    fun getDetailProduct(idProduct: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getProductDetail(idProduct)
                    if (response.isSuccessful) {
                        _getDetailProduct.postValue(response.body())
                        Log.d("TAG", "getDetailProduct: $response")
                        _getIdStore.postValue(response.body()!!.data.store.id)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun postProductCard(productId: String) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val postProductCart = PostProductCart(
                        productId = productId
                    )
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val sharedToken = sharedPreferences.getString(Prefs.KEY_TOKEN, "")

                    val response = repository.postCartProduct(
                        sharedIdUser.toString(),
                        "Bearer ${sharedToken.toString()}",
                        postProductCart
                    )
                    Log.d("TAG", "pesan userId: ${sharedIdUser.toString()}")
                    Log.d("TAG", "pesan productId: ${postProductCart.productId}")
                    Log.d("TAG", "pesan response postProductCard: ${response}")
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun postProductFavorite(productId: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val favoriteProduct = FavoriteProduct(
                        productId
                    )
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response =
                        repository.postFavoriteProduct(sharedIdUser.toString(), favoriteProduct)
                    Log.d("postFavorite", "response post favorite 3 : $response")
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun deleteFavorite(id: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                favoriteDao.deleteFavoriteProduct(id)
            }
        }

    }

    fun onError(message: String) {
        _errorMessage.postValue(message)
    }

    fun getStoreProducts(storeId: String, page: Int = 1, size: Int = 6) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getStoreProducts(storeId, page = page, size = size)
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = null,
                            listProductStore = response.body()!!.listProduct
                        )
                        _getStoreProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getRecomendationProducts(category: String, page: Int = 1, size: Int = 6) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getProduct(
                        page = page,
                        size = size,
                        category = category,
                        condition = null,
                        search = null,
                        sizeProduct = null,
                        subCategory1 = null,
                        subCategory2 = null,
                        sortBy = null
                    )
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = response.body()?.data?.products,
                            listProductStore = null
                        )
                        _getRecomendationProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }


}