package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.AddressResponse
import com.finalproject.thrifthouse.repository.AddressRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class AddressActivityViewModel(
    private val repository: AddressRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _getListAddress = MutableLiveData<AddressResponse>()
    val getListAddress: LiveData<AddressResponse> = _getListAddress

    fun getListAddress(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getListAddress(sharedIdUser.toString())
                    if(response.isSuccessful){
                        _getListAddress.postValue(response.body())
                    }else{
                        _errorMessage.postValue(response.message())
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }


    fun onError(message: String){
        _errorMessage.postValue(message)
    }
}