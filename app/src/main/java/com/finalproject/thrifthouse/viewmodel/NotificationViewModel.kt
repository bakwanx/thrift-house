package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.NotificationResponse
import com.finalproject.thrifthouse.model.CheckOutModel
import com.finalproject.thrifthouse.repository.AddressRepository
import com.finalproject.thrifthouse.repository.NotificationRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class NotificationViewModel(
    private val repository: NotificationRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _getNotification = MutableLiveData<NotificationResponse>()
    private val _isError = MutableLiveData<Boolean>()
    val getNotification: LiveData<NotificationResponse> = _getNotification

    fun getAllNotification(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getAllNotification(sharedIdUser.toString())
                    if (response.isSuccessful) {
                        _getNotification.postValue(response.body())
                        _isError.postValue(false)
                    } else {
                        _isError.postValue(true)
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getSpecificNotification(type: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getSpecificNotification(sharedIdUser.toString(), type)
                    if (response.isSuccessful) {
                        _getNotification.postValue(response.body())
                        _isError.postValue(false)
                    } else {
                        _isError.postValue(true)
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String) {
        _isError.postValue(true)
    }
}