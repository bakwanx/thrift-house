package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.PostAddress
import com.finalproject.thrifthouse.api.response.ProvinceRajaOnkgirResponse
import com.finalproject.thrifthouse.repository.AddressRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class AddAddressActivityViewModel(
    private val repository: AddressRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _isSuccess = MutableLiveData<Boolean>()
    val isSuccess: LiveData<Boolean> = _isSuccess

    fun postAddress(postAddress: PostAddress){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {

                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.postAddress(sharedIdUser.toString(), postAddress)

                    if(response.isSuccessful){
                        _isSuccess.postValue(true)
                    }else{
                        _isSuccess.postValue(false)
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }

            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }


}