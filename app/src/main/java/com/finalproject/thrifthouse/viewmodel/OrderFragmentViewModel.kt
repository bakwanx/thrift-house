package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.OrderListResponse
import com.finalproject.thrifthouse.api.response.PaymentResponse
import com.finalproject.thrifthouse.repository.OrderRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class OrderFragmentViewModel(
    private val repository: OrderRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _orderResponse = MutableLiveData<OrderListResponse>()
    private val _isSuccess = MutableLiveData<Boolean>()
    val getOrder: LiveData<OrderListResponse> = _orderResponse
    val isSuccess: LiveData<Boolean> = _isSuccess

    fun getOrder(status: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getListOrder(sharedIdUser.toString(), status)
                    if (response.isSuccessful){
                        _orderResponse.postValue(response.body())
                    }else{
                        _errorMessage.postValue(response.body()?.message)
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }

            }
        }
    }

    fun acceptOrder(transactionId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.acceptOrder(sharedIdUser.toString(), transactionId)
                    Log.d("TAG", "acceptOrder response: $response")
                    if (response.isSuccessful){
                        _isSuccess.postValue(true)
                    }else{
                        _isSuccess.postValue(false)
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }

            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

}