package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.CartResponse
import com.finalproject.thrifthouse.api.response.DeleteProductCart
import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.utilities.GroupingCart
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class CartActivityViewModel(
    private val repository: ProductRepository,
    private val sharedPreferences: SharedPreferences
    ): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _isDelete = MutableLiveData<Boolean>()
    private val _getCartModel = MutableLiveData<List<CartModel>>()
    private val _totalItem = MutableLiveData<List<ProductCart>>()
    private val _totalSelectedItem: ArrayList<ProductCart> = ArrayList()
    val getCartModel: LiveData<List<CartModel>> = _getCartModel
    val getTotal: LiveData<List<ProductCart>> = _totalItem
    val getIsDelete: LiveData<Boolean> = _isDelete

    fun getCart(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val sharedToken = sharedPreferences.getString(Prefs.KEY_TOKEN, "")
                    val response = repository.getCart(sharedIdUser.toString(), "Bearer ${sharedToken.toString()}")
                    if (response.isSuccessful){
                        _getCartModel.postValue(GroupingCart.grouping(response.body()!!.data))
                    } else{
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun setSelected(productCart: ProductCart){
        _totalSelectedItem.add(productCart)
        _totalItem.postValue(_totalSelectedItem)
    }


    fun removeSelected(productCart: ProductCart){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            _totalSelectedItem.removeIf { it.id == productCart.id }
        }
        _totalItem.postValue(_totalSelectedItem)
    }

    fun deleteCart(productIds: ArrayList<String>){

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val deleteProductCart = DeleteProductCart(
                        productIds = productIds
                    )
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val sharedToken = sharedPreferences.getString(Prefs.KEY_TOKEN, "")
                    val response = repository.deleteCartProduct(sharedIdUser.toString(), "Bearer ${sharedToken.toString()}", deleteProductCart)
                    if(response.isSuccessful){
                        _isDelete.postValue(true)
                    }else{
                        _isDelete.postValue(false)
                    }
                }catch (throwable: Throwable){
                    _isDelete.postValue(false)
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }


    fun onError(message: String){
        _errorMessage.postValue(message)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }


}