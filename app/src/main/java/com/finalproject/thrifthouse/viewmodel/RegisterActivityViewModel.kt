package com.finalproject.thrifthouse.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.UserRegister
import com.finalproject.thrifthouse.di.repositoryModule
import com.finalproject.thrifthouse.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class RegisterActivityViewModel(private val userRepository: UserRepository): ViewModel() {
    private val _errorMessage = MutableLiveData<String>()
    private val _isRegister = MutableLiveData<Boolean>()
    val getIsRegister: LiveData<Boolean> = _isRegister

    fun register(
        username: String,
        email: String,
        password: String,
        phone: String,
    ){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val userRegister = UserRegister(
                        username = username,
                        email = email,
                        password = password,
                        phone = phone,
                        role = "ROLE_USER"
                    )
                    val response = userRepository.register(userRegister)
                    if(response.isSuccessful){
                        _isRegister.postValue(true)
                    }else{
                        _isRegister.postValue(false)
                    }
                }catch (throwable: Throwable){
                    _isRegister.postValue(true)
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

}