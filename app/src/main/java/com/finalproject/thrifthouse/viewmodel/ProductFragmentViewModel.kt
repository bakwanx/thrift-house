package com.finalproject.thrifthouse.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.ListProduct
import com.finalproject.thrifthouse.api.response.ProductResponse
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.io.IOException

class ProductFragmentViewModel(
    private val repository: ProductRepository,
    private val favoriteDao: FavoriteDao
) : ViewModel() {

    private val _getMaleProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _getFemaleProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _getChildrenProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _errorMessage = MutableLiveData<String>()
    val getMaleProducts: LiveData<ArrayList<ProductModel>> = _getMaleProducts
    val getFemaleProducts: LiveData<ArrayList<ProductModel>> = _getFemaleProducts
    val getChildrenProducts: LiveData<ArrayList<ProductModel>> = _getChildrenProducts

    fun getMaleProduct(
        page: Int = 1,
        size: Int = 5,
        sizeProduct: String?,
        condition: String?
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getMaleProduct(
                        page = page,
                        size = size,
                        sizeProduct = sizeProduct,
                        condition = condition,
                        null
                    )
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = response.body()!!.data.products,
                            listProductStore = null
                        )

                        _getMaleProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getFemaleProduct(
        page: Int = 1,
        size: Int = 5,
        sizeProduct: String?,
        condition: String?
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {

                    val response = repository.getFemaleProduct(
                        page = page,
                        size = size,
                        sizeProduct,
                        condition,
                        null
                    )
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = response.body()!!.data.products,
                            listProductStore = null
                        )
                        _getFemaleProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getChildrenProduct(
        page: Int = 1,
        size: Int = 5,
        sizeProduct: String?,
        condition: String?
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getChildren(
                        page = page,
                        size = size,
                        sizeProduct,
                        condition,
                        null
                    )
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = response.body()!!.data.products,
                            listProductStore = null
                        )
                        _getChildrenProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun onError(message: String) {
        _errorMessage.postValue(message)
    }

}