package com.finalproject.thrifthouse.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.ProductResponseData
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class ProductCategoryActivityViewModel(
    private val repository: ProductRepository,
    private val favoriteDao: FavoriteDao
) : ViewModel() {
    private val _getPagination = MutableLiveData<ProductResponseData>()
    private val _errorMessage = MutableLiveData<String>()
    private val _getProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _isLoading = MutableLiveData<Boolean>()
    val getPagination: LiveData<ProductResponseData> = _getPagination
    val getProducts: LiveData<ArrayList<ProductModel>> = _getProducts
    val getIsLoading: LiveData<Boolean> = _isLoading

    fun getProduct(
        category: String?,
        subCategory: String?,
        subCategoryTwo: String?,
        page: Int = 1,
        size: Int = 20
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getProduct(
                        category,
                        subCategory,
                        subCategoryTwo,
                        search = null,
                        page = page,
                        size = size,
                        sizeProduct = null,
                        condition = null,
                        sortBy = null
                    )
                    _getPagination.postValue(response.body()?.data)
                    Log.d("TAG", "getProduct response: $response")
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = response.body()!!.data.products,
                            listProductStore = null
                        )
                        _getProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }

    fun onError(message: String) {
        _errorMessage.postValue(message)
    }

}