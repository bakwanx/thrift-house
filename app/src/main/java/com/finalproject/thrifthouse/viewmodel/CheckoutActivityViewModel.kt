package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.model.CheckOutModel
import com.finalproject.thrifthouse.model.PostCheckOutModel
import com.finalproject.thrifthouse.model.StoreTransaction
import com.finalproject.thrifthouse.repository.AddressRepository
import com.finalproject.thrifthouse.repository.BankRepository
import com.finalproject.thrifthouse.repository.DeliveryServiceRepository
import com.finalproject.thrifthouse.repository.OrderRepository
import com.finalproject.thrifthouse.utilities.GroupingCheckout
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class CheckoutActivityViewModel(
    private val repository: AddressRepository,
    private val bankRepository: BankRepository,
    private val orderRepository: OrderRepository,
    private val deliveryServiceRepository: DeliveryServiceRepository,
    private val sharedPreferences: SharedPreferences
) : ViewModel() {

    private val _isError = MutableLiveData<Boolean>()
    private val _getListAddress = MutableLiveData<AddressResponse>()
    private val _getDeliveryService = MutableLiveData<StoreDeliveryServiceResponse>()
    private val _getListBank = MutableLiveData<BankResponse>()
    private val _isSuccessOrder = MutableLiveData<String>()
    private val _getCheckOutModel = MutableLiveData<List<CheckOutModel>>()
    val getCheckOutModel: LiveData<List<CheckOutModel>> = _getCheckOutModel
    val getListAddress: LiveData<AddressResponse> = _getListAddress
    val getIsSuccessOrder: LiveData<String> = _isSuccessOrder
    val getListBank: LiveData<BankResponse> = _getListBank
    val isError: LiveData<Boolean> = _isError
    val getDeliveryService: LiveData<StoreDeliveryServiceResponse> = _getDeliveryService
    private var expedition: ArrayList<String> = ArrayList()

    fun getListAddress() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getListAddress(sharedIdUser.toString())
                    if (response.isSuccessful) {
                        _getListAddress.postValue(response.body())
                        _isError.postValue(false)
                    } else {
                        _isError.postValue(true)
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getBank() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = bankRepository.getBanks()
                    if (response.isSuccessful) {
                        _getListBank.postValue(response.body())
                        _isError.postValue(false)
                    } else {
                        _isError.postValue(true)
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getDeliveryService(storeId: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = deliveryServiceRepository.getDeliveryService(storeId)
                    if (response.isSuccessful) {
                        _getDeliveryService.postValue(response.body())
                        expedition.add(response.body()?.data?.deliveryService.toString())
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getCostDeliveryService(
        expedition: List<String>,
        cartModels: List<CartModel>,
        myCityId: String
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val resultDeliveryServices: ArrayList<ResultDeliveryService> = ArrayList()
                    cartModels.forEachIndexed { i, cartModel ->
                        var totalWeight = 0

                        cartModels.get(i).product.forEach {
                            totalWeight += it.weight.toInt()
                        }

                        val postToGetDeliveryService = PostToGetDeliveryService(
                            courier = expedition.get(i),
                            destination = "255",
                            origin = cartModels.get(i).city_id,
                            weight = totalWeight.toString()
                        )
                        val response = deliveryServiceRepository.postCostDeliveryService(
                            postToGetDeliveryService
                        )
                        Log.d(
                            "TAG",
                            "eksekusi ${expedition.get(i)}, $myCityId, ${cartModels.get(i).city_id}, ${totalWeight.toString()}"
                        )
                        Log.d("TAG", "response ${response.body()}")
                        Log.d("TAG", "eksekusi ${response.body()?.rajaongkir?.results?.size}")
                        resultDeliveryServices.add(response.body()?.rajaongkir?.results?.get(0) as ResultDeliveryService)
                    }
                    _getCheckOutModel.postValue(
                        GroupingCheckout.groupingCheckoutModel(
                            cartModels,
                            resultDeliveryServices
                        )
                    )
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun checkOut(checkOutModels: List<CheckOutModel>, bankId: String, addressId: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val storeTransactions = mutableListOf<StoreTransaction>()
                    val productIds = mutableListOf<String>()
                    checkOutModels.forEachIndexed { index, checkOutModel ->
                        checkOutModel.product.forEachIndexed { index, productCart ->
                            productIds.add(productCart.id)
                        }
                        storeTransactions.add(
                            StoreTransaction(
                                productIds = productIds,
                                deliveryService = checkOutModel.resultDeliveryService.code,
                                shippingCost = checkOutModel.resultDeliveryService.costs.get(0).cost.get(
                                    0
                                ).value.toString(),
                                shippingService = checkOutModel.resultDeliveryService.costs.get(0).service,
                                estimatedTimeOfDeparture = checkOutModel.resultDeliveryService.costs.get(
                                    0
                                ).cost.get(
                                    0
                                ).etd,
                            )
                        )
                    }

                    val postCheckOutModel = PostCheckOutModel(
                        userId = sharedIdUser.toString(),
                        addressId = addressId,
                        bankId = bankId,
                        storeTransactions = storeTransactions
                    )
                    Log.d(
                        "TAG",
                        "data checkout userId: ${postCheckOutModel.userId}, addressId: $addressId, bankId: $bankId"
                    )
                    storeTransactions.forEach {
                        Log.d(
                            "TAG",
                            "data checkout productId: ${it.productIds}, deliveryService: ${it.deliveryService}, shippingCost: ${it.shippingCost}, shippingService: ${it.shippingService}, estimatedTimeOfDeparture: ${it.estimatedTimeOfDeparture}"
                        )
                    }
                    val response = orderRepository.postOrder(postCheckOutModel)
                    if (response.isSuccessful) {
                        _isSuccessOrder.postValue(response.body()?.data?.order_id_1)
                    } else {
                        _isSuccessOrder.postValue("")
                    }

                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }

    }

    fun onError(message: String) {
        _isError.postValue(true)
    }
}