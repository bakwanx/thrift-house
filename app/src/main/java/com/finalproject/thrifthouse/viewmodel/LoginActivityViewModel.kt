package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.UserLogin
import com.finalproject.thrifthouse.api.response.UserResponse
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.repository.UserRepository
import com.finalproject.thrifthouse.utilities.Prefs
import com.finalproject.thrifthouse.utilities.Prefs.Companion.KEY_TOKEN
import com.finalproject.thrifthouse.utilities.Prefs.Companion.KEY_USER_ID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class LoginActivityViewModel(
    private val repository: UserRepository,
    private val sharedPreferences: SharedPreferences
    ): ViewModel() {
    private val _errorMessage = MutableLiveData<String>()
    private val _isLogin = MutableLiveData<Boolean>()
    val getIsLogin: LiveData<Boolean> = _isLogin

    fun login(username:String, password:String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val userLogin = UserLogin(
                        username = username,
                        password = password
                    )
                    val response = repository.login(userLogin)

                    if (response.isSuccessful){
                        _isLogin.postValue(true)
                        savePrefs(response.body()!!)
                    }else{
                        _isLogin.postValue(false)
                    }
                }catch (throwable: Throwable){
                    _isLogin.postValue(false)
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }

            }
        }
    }

    fun savePrefs(userResponse: UserResponse){
        val editor = sharedPreferences.edit()
        editor.putString(KEY_USER_ID, userResponse.id)
        editor.putString(KEY_TOKEN, userResponse.access_token)
        val token = sharedPreferences.getString(Prefs.KEY_TOKEN, "")
        Log.d("TAG", "token: ${userResponse.access_token}")
        editor.apply()
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }

}