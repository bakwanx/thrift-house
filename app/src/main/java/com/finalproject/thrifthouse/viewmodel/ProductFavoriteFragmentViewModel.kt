package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.FavoriteResponse
import com.finalproject.thrifthouse.api.response.ListProduct
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.model.ProductModelRoom
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.Prefs
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class ProductFavoriteFragmentViewModel(
    private val repository: ProductRepository,
    private val sharedPreferences: SharedPreferences,
    private val favoriteDao: FavoriteDao
    ): ViewModel() {
    private val _getProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()
    val getProducts: LiveData<ArrayList<ProductModel>> = _getProducts

    fun getProduct(search: String?){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getFavoriteProduct(sharedIdUser.toString(), search)

                    if (response.isSuccessful){
                        val result = checkFavoriteProduct(favoriteDao = favoriteDao, listProduct = response.body()!!.data.products, listProductStore = null)
                        _getProducts.postValue(result)
                        val listFavoriteProduct = response.body()!!.data.products
                        if(listFavoriteProduct.size != 0){
                            for (item in listFavoriteProduct){
                                setToLocal(item)
                            }
                        }
                    } else {
                        onError("Error: ${response.message()}")
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    private fun setToLocal(product: ListProduct){
        val productModelRoom = ProductModelRoom(
            id = product.id,
            name = product.name,
            brand = product.brand,
            size = product.size,
            condition = product.condition,
            price = product.price,
            photos = product.photos,
            category = product.photos
        )
        favoriteDao.addFavoriteProduct(productModelRoom)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }
}