package com.finalproject.thrifthouse.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.ProductResponse
import com.finalproject.thrifthouse.api.response.ProductResponseData
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.repository.ProductRepository
import com.finalproject.thrifthouse.room.FavoriteDao
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class ProductActivityViewModel(
    private val repository: ProductRepository,
    private val favoriteDao: FavoriteDao
) : ViewModel() {
    private val _getPagination = MutableLiveData<ProductResponseData>()
    private val _getProducts = MutableLiveData<ArrayList<ProductModel>>()
    private val _errorMessage = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _category = MutableLiveData<String>()
    private val _brand = MutableLiveData<String>()
    private var _size = MutableLiveData<String>()
    private var _condition = MutableLiveData<String>()
    val getProducts: LiveData<ArrayList<ProductModel>> = _getProducts
    val getPagination: LiveData<ProductResponseData> = _getPagination
    val getIsLoading: LiveData<Boolean> = _isLoading

    fun getProduct(
        category: String?,
        search: String?,
        page: Int = 1,
        size: Int = 20,
        sortBy: String?
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.getProduct(
                        category,
                        subCategory1 = null,
                        subCategory2 = null,
                        search = search,
                        page = page,
                        size = size,
                        _size.value,
                        _condition.value,
                        sortBy
                    )
                    _getPagination.postValue(response.body()?.data)
                    Log.d("TAG", "getProduct response: $response")
                    if (response.isSuccessful) {
                        val result = checkFavoriteProduct(
                            favoriteDao = favoriteDao,
                            listProduct = response.body()!!.data.products,
                            listProductStore = null
                        )
                        _getProducts.postValue(result)
                    } else {
                        onError("Error: ${response.message()}")
                    }
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun setCategoryFilter(category: String) {
        _category.postValue(category)
    }

    fun setBrandFilter(brand: String) {
        _brand.postValue(brand)
    }

    fun setSizeFilter(size: String) {
        _size.postValue(size)
    }

    fun setConditionFilter(condition: String) {
        _condition.postValue(condition)
    }

    fun isLoading(isLoading: Boolean): LiveData<Boolean> {
        _isLoading.postValue(isLoading)
        return _isLoading
    }

    fun onError(message: String) {
        _errorMessage.postValue(message)
    }


}