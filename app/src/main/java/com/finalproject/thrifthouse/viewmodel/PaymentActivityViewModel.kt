package com.finalproject.thrifthouse.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.finalproject.thrifthouse.api.response.GetUserResponse
import com.finalproject.thrifthouse.api.response.PaymentResponse
import com.finalproject.thrifthouse.api.response.PutOrderResponse
import com.finalproject.thrifthouse.api.response.UserLogin
import com.finalproject.thrifthouse.repository.OrderRepository
import com.finalproject.thrifthouse.repository.UserRepository
import com.finalproject.thrifthouse.utilities.Prefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import retrofit2.HttpException
import java.io.IOException

class PaymentActivityViewModel(
    private val repository: OrderRepository,
    private val sharedPreferences: SharedPreferences
): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    private val _isSucces = MutableLiveData<Boolean>()
    private val _orderResponse = MutableLiveData<PaymentResponse>()
    private val _userDetail = MutableLiveData<GetUserResponse>()
//    private val _paymentResponse = MutableLiveData<PutOrderResponse>()
    val getOrderResponse: LiveData<PaymentResponse> = _orderResponse
//    val getPaymentResponse: LiveData<PutOrderResponse> = _paymentResponse
    val isSucces: LiveData<Boolean> = _isSucces
    val getUserDetail: LiveData<GetUserResponse> = _userDetail

    fun getPayment(orderId: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val response = repository.getPayment(orderId)
                    if (response.isSuccessful){
                        _orderResponse.postValue(response.body())
                    }else{
                        _errorMessage.postValue(response.body()?.message)
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }

            }
        }
    }

    fun postReceipt(
        orderId: String,
        photo: MultipartBody.Part
    ){
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.putReceipt(orderId, photo)
                    Log.d("TAG", "postReceipt response: ${response.body()}")
                    if (response.isSuccessful){
                        _isSucces.postValue(true)
//                        _paymentResponse.postValue(response.body())
                    }else{
                        _isSucces.postValue(false)
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }
            }
        }
    }

    fun getUserDetail(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")
                    val response = repository.getUserDetail(sharedIdUser.toString())
                    if (response.isSuccessful){
                        _userDetail.postValue(response.body())
                    }else{
                        _errorMessage.postValue(response.body()?.message)
                    }
                }catch (throwable: Throwable){
                    when (throwable) {
                        is IOException -> {
                            onError("Network Error")
                        }
                        is HttpException -> {
                            val codeError = throwable.code()
                            val errorMessageResponse = throwable.message()
                            onError("Error $errorMessageResponse : $codeError")
                        }
                        else -> {
                            onError("Uknown error")
                        }
                    }
                }

            }
        }
    }

    fun onError(message: String){
        _errorMessage.postValue(message)
    }
}