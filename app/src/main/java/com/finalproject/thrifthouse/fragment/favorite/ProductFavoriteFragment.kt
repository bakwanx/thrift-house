package com.finalproject.thrifthouse.fragment.favorite

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.LoginActivity
import com.finalproject.thrifthouse.adapter.CategoryAdapter
import com.finalproject.thrifthouse.adapter.ProductAdapter
import com.finalproject.thrifthouse.adapter.ProductStoreAdapter
import com.finalproject.thrifthouse.api.response.ListProduct
import com.finalproject.thrifthouse.api.response.ProductStore
import com.finalproject.thrifthouse.databinding.FragmentAboutStoreBinding
import com.finalproject.thrifthouse.databinding.FragmentProductFavoriteBinding
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct

import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.ProductActivityViewModel
import com.finalproject.thrifthouse.viewmodel.ProductFavoriteFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductFavoriteFragment : Fragment() {

    private var _binding: FragmentProductFavoriteBinding? = null
    private val binding get() = _binding!!
    private val productViewModel: ProductFavoriteFragmentViewModel by sharedViewModel()
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    private lateinit var productAdapter: ProductAdapter
    var page = 0
    var size = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductFavoriteBinding.inflate(inflater, container, false)
        val view = binding.root

        init()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchFavoriteProduct(null)
        initObserver()

    }

    private fun init(){
        productAdapter = ProductAdapter("vertical")
        binding.rvProduct.adapter = productAdapter
    }

    private fun initObserver(){
        binding.apply {

            productViewModel.getProducts.observe(viewLifecycleOwner){

                productAdapter.setItem(favoriteViewModel, it, CheckAuth().checkAuth(application = requireActivity().application))
                productAdapter.notifyDataSetChanged()
                onLoadDataVisibility()

            }

        }
    }


    fun fetchFavoriteProduct(search: String?){
        emptyDataVisibility()
        productViewModel.getProduct(search)
    }

    private fun emptyDataVisibility(){
        binding.apply {
            shimmerProduct.startShimmer()
            shimmerProduct.visibility = View.VISIBLE
        }
    }

    private fun onLoadDataVisibility(){
        binding.apply {
            shimmerProduct.stopShimmer()
            shimmerProduct.visibility = View.GONE
        }
    }

}