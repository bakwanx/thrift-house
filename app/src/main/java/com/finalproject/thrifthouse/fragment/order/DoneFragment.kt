package com.finalproject.thrifthouse.fragment.order

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.OrderAdapter
import com.finalproject.thrifthouse.databinding.FragmentDoneBinding
import com.finalproject.thrifthouse.databinding.FragmentSentBinding
import com.finalproject.thrifthouse.viewmodel.OrderFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class DoneFragment : Fragment() {

    private var _binding: FragmentDoneBinding? = null
    private val binding get() = _binding!!
    private val orderFragmentViewModel: OrderFragmentViewModel by viewModel()
    private lateinit var orderAdapter: OrderAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDoneBinding.inflate(inflater, container, false)
        val view = binding.root
        init()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchData()
        initObserver()
        initSwipeRefresh()
    }

    private fun initSwipeRefresh() {
        binding.apply {
            swipeRefresh.setOnRefreshListener {
                fetchData()
                swipeRefresh.isRefreshing = false
            }
        }
    }

    private fun init(){
        orderAdapter = OrderAdapter(object : OrderAdapter.Listener{
            override fun onClickAcceptOrder(transactionId: String) {
                TODO("Not yet implemented")
            }
        })
        binding.rvDone.adapter = orderAdapter
    }

    private fun fetchData(){
        orderFragmentViewModel.getOrder("selesai")
    }

    private fun initObserver(){
        orderFragmentViewModel.getOrder.observe(viewLifecycleOwner){
            orderAdapter.setItem(it.data)
            orderAdapter.notifyDataSetChanged()
        }
    }

}