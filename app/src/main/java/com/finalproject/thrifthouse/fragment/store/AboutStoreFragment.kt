package com.finalproject.thrifthouse.fragment.store

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.databinding.FragmentAboutStoreBinding
import com.finalproject.thrifthouse.databinding.FragmentChildrenStoreBinding
import com.finalproject.thrifthouse.viewmodel.AboutStoreFragmentViewModel
import com.finalproject.thrifthouse.viewmodel.ChildrenStoreFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AboutStoreFragment(private val storeId: String) : Fragment() {

    private var _binding: FragmentAboutStoreBinding? = null
    private val binding get() = _binding!!
    private val aboutViewModel: AboutStoreFragmentViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAboutStoreBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchAboutStore()
        binding.apply {
            aboutViewModel.getAboutStore.observe(viewLifecycleOwner){
                binding.pbAbout.visibility = View.GONE
                tvDescStore.text = it.data.about
            }
        }
    }


    private fun fetchAboutStore(){
        binding.pbAbout.visibility = View.VISIBLE
        aboutViewModel.getAboutStore(storeId)
    }

}