package com.finalproject.thrifthouse.fragment.category

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.finalproject.thrifthouse.adapter.SubCategoryAdapter
import com.finalproject.thrifthouse.databinding.FragmentFemaleBinding
import com.finalproject.thrifthouse.utilities.DataDummy

class FemaleFragment : Fragment() {

    private var _binding: FragmentFemaleBinding? = null
    private val binding get() = _binding!!
    private lateinit var subCategoryAdapter: SubCategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFemaleBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dataSubCategory = DataDummy().dataSubCategory

        binding.apply {
            subCategoryAdapter = SubCategoryAdapter(object : SubCategoryAdapter.Listener{
                override fun selectedItem(subCategory: String, subCategoryTwo: String) {
                    Log.d("TAG", "subCategory: $subCategory, subCategoryTwo: $subCategoryTwo")
                }
            })
            rvSubCategory.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            rvSubCategory.adapter = subCategoryAdapter
            subCategoryAdapter.setItem(dataSubCategory, "female")
        }
    }

}