package com.finalproject.thrifthouse.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.finalproject.thrifthouse.activities.*
import com.finalproject.thrifthouse.databinding.FragmentAccountBinding
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.AccountFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountFragment : Fragment() {
    private var _binding: FragmentAccountBinding? = null
    private val binding get() = _binding!!
    private val accountFragmentViewModel: AccountFragmentViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAccountBinding.inflate(inflater, container, false)
        val view = binding.root
        checkAuth()
        init()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        initAction()
    }

    private fun initAction() {
        binding.apply {
            llBiodata.setOnClickListener {
                val intent = Intent(requireActivity(), BiodataActivity::class.java)
                startActivity(intent)
            }

            llAddress.setOnClickListener {
                val intent = Intent(requireActivity(), AddressActivity::class.java)
                intent.putExtra(KEY_FROM, "account")
                startActivity(intent)
            }

            llLogOut.setOnClickListener {
                accountFragmentViewModel.logOut()
                val intent = Intent(requireActivity(), LoginActivity::class.java)
                startActivity(intent)
                (activity as MainActivity?)?.finishActivity()
            }

            llUnpaid.setOnClickListener {
                val intent = Intent(requireActivity(), MyOrderActivity::class.java)
                intent.putExtra(KEY_POSITION_ORDER, 0)
                startActivity(intent)
            }

            llPacked.setOnClickListener {
                val intent = Intent(requireActivity(), MyOrderActivity::class.java)
                intent.putExtra(KEY_POSITION_ORDER, 1)
                startActivity(intent)
            }

            llSent.setOnClickListener {
                val intent = Intent(requireActivity(), MyOrderActivity::class.java)
                intent.putExtra(KEY_POSITION_ORDER, 2)
                startActivity(intent)
            }

            llDone.setOnClickListener {
                val intent = Intent(requireActivity(), MyOrderActivity::class.java)
                intent.putExtra(KEY_POSITION_ORDER, 3)
                startActivity(intent)
            }

            llCancel.setOnClickListener {
                val intent = Intent(requireActivity(), MyOrderActivity::class.java)
                intent.putExtra(KEY_POSITION_ORDER, 4)
                startActivity(intent)
            }
        }
    }

    private fun initObserver() {
        binding.apply {
            accountFragmentViewModel.getUserDetail.observe(viewLifecycleOwner) {
                binding.apply {
                    rlHeader.visibility = View.VISIBLE
                    shimmerHeaderAccount.visibility = View.GONE
                }
                Glide
                    .with(requireContext())
                    .load(it.data.profileImg)
                    .centerCrop()
                    .into(ivAccountProfile)
                tvNameAccount.text = it.data.username
                tvEmail.text = it.data.email
            }
        }
    }

    private fun checkAuth() {
        if (!CheckAuth().checkAuth(requireActivity().application)) {
            val intent = Intent(requireContext(), LoginActivity::class.java)
            ToastMessage.showToast(requireContext(), "Login terlebih dahulu")
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun init() {
        accountFragmentViewModel.getUserDetail()
        binding.apply {
            rlHeader.visibility = View.GONE
            shimmerHeaderAccount.visibility = View.VISIBLE
        }
    }

    companion object {
        val KEY_FROM = "key account"
        val KEY_POSITION_ORDER = "key position order"
    }
}