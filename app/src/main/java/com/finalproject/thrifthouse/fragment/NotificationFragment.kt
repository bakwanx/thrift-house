package com.finalproject.thrifthouse.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.finalproject.thrifthouse.activities.LoginActivity
import com.finalproject.thrifthouse.adapter.NotificationPageAdapter
import com.finalproject.thrifthouse.databinding.FragmentNotificationBinding
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.utilities.ToastMessage

class NotificationFragment : Fragment() {

    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkAuth()
        init()

    }

    private fun checkAuth(){
        if(!CheckAuth().checkAuth(requireActivity().application)){
            val intent = Intent(requireContext(), LoginActivity::class.java)
            ToastMessage.showToast(requireContext(), "Login terlebih dahulu")
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun init(){

        binding.apply {
            vpNotification.adapter = NotificationPageAdapter(childFragmentManager)
            tabNotification.setupWithViewPager(vpNotification)

            val tabs = tabNotification.getChildAt(0) as? ViewGroup
            for (i in 0 until tabs!!.childCount) {
                val tab = tabs.getChildAt(i)
                val layoutParams = tab.layoutParams as LinearLayout.LayoutParams
                layoutParams.weight = 0f
                layoutParams.marginEnd = 12
                layoutParams.marginStart = 12
                tab.layoutParams = layoutParams
            }
            vpNotification.requestLayout()
        }
    }

}