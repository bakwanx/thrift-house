package com.finalproject.thrifthouse.fragment.notification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.finalproject.thrifthouse.adapter.NotificationAdapter
import com.finalproject.thrifthouse.databinding.FragmentAllNotificationBinding
import com.finalproject.thrifthouse.viewmodel.NotificationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AllNotificationFragment : Fragment() {

    private var _binding: FragmentAllNotificationBinding? = null
    private val binding get() = _binding!!
    private lateinit var notificationAdapter: NotificationAdapter
    private val notificationViewModel: NotificationViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAllNotificationBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initData()
        initObserver()

    }

    private fun init(){
        notificationAdapter = NotificationAdapter()
        binding.rvNotification.adapter = notificationAdapter
    }

    private fun initData(){
        notificationViewModel.getAllNotification()
    }

    private fun initObserver(){
        notificationViewModel.getNotification.observe(viewLifecycleOwner){
            notificationAdapter.setItem(it.data)
            notificationAdapter.notifyDataSetChanged()
        }
    }

}