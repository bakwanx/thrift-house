package com.finalproject.thrifthouse.fragment.store

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.finalproject.thrifthouse.adapter.ProductStoreAdapter
import com.finalproject.thrifthouse.api.response.ListProduct
import com.finalproject.thrifthouse.api.response.ProductStore
import com.finalproject.thrifthouse.databinding.FragmentHomeBinding
import com.finalproject.thrifthouse.databinding.FragmentProductStoreBinding
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct

import com.finalproject.thrifthouse.viewmodel.DetailStoreActivityViewModel
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.ProductFragmentViewModel
import com.finalproject.thrifthouse.viewmodel.ProductStoreFragmentViewModel
import com.she.sehatq.base.utils.EndlessScrollListener
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductStoreFragment(private val storeId: String) : Fragment() {
    private var _binding: FragmentProductStoreBinding? = null
    private val binding get() = _binding!!
    private val productViewModel: ProductStoreFragmentViewModel by viewModel()
    private lateinit var productStoreAdapter: ProductStoreAdapter
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    private var isLoading = false
    var page = 0
    var totalPage = 4
    private lateinit var scrollListener: EndlessScrollListener
    private var productList = mutableListOf<ProductModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductStoreBinding.inflate(inflater, container, false)
        val view = binding.root
        init()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchProduct()
        initObserver()

    }

    private fun initObserver(){
        productViewModel.getStoreProducts.observe(viewLifecycleOwner){
            productList.addAll(it)
            productStoreAdapter.setItem(favoriteViewModel, productList)
            productStoreAdapter.notifyDataSetChanged()
        }
        initScrollListener()

        productViewModel.isLoading(false).observe(viewLifecycleOwner){
            if(it){
                binding.pbProduct.visibility = View.VISIBLE
            }else{
                binding.pbProduct.visibility = View.INVISIBLE
            }
        }
    }

    private fun fetchProduct() {
        productViewModel.isLoading(true)
        page += 1
        productViewModel.getStoreProducts(storeId = storeId, page = 0)
        binding.rvProduct.post {
            productStoreAdapter.notifyDataSetChanged()
        }
    }

    private fun init(){
        productStoreAdapter = ProductStoreAdapter("vertical")
        binding.rvProduct.adapter = productStoreAdapter
        binding.rvProduct.isNestedScrollingEnabled = false
    }

    private fun initScrollListener() {
        scrollListener =
            object : EndlessScrollListener(binding.rvProduct.layoutManager as GridLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    if (page <= totalPage) {
                        productViewModel.isLoading(true)
                        fetchProduct()
                    } else {
                        productViewModel.isLoading(false)
                    }
                }

            }
        binding.rvProduct.addOnScrollListener(scrollListener)
    }


}