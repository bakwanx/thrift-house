package com.finalproject.thrifthouse.fragment

import android.R
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager.widget.ViewPager
import com.finalproject.thrifthouse.activities.LoginActivity
import com.finalproject.thrifthouse.adapter.FavoriteAdapter
import com.finalproject.thrifthouse.databinding.FragmentFavoriteBinding
import com.finalproject.thrifthouse.fragment.favorite.ProductFavoriteFragment
import com.finalproject.thrifthouse.fragment.favorite.StoreFavoriteFragment
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.utilities.ToastMessage
import com.finalproject.thrifthouse.viewmodel.ProductFavoriteFragmentViewModel
import com.finalproject.thrifthouse.viewmodel.StoreFavoriteFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class FavoriteFragment : Fragment() {
    private var _binding: FragmentFavoriteBinding? = null
    private val binding get() = _binding!!
    private var isFilter = true
    private val storeViewModel: StoreFavoriteFragmentViewModel by sharedViewModel()
    private val productViewModel: ProductFavoriteFragmentViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFavoriteBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkAuth()
        init()
        searchAction()
        initAction()

    }

    private fun checkAuth(){
        if(!CheckAuth().checkAuth(requireActivity().application)){
            val intent = Intent(requireContext(), LoginActivity::class.java)
            ToastMessage.showToast(requireContext(), "Login terlebih dahulu")
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun init(){
        binding.apply {
            vpFavorite.adapter = FavoriteAdapter(childFragmentManager)
            tabFavorite.setupWithViewPager(vpFavorite)
            vpFavorite.requestLayout()
        }
    }

    private fun initAction(){
        binding.apply {
            ivFilterFavorite.setOnClickListener {
                if(isFilter){
                    clFilterFavorite.visibility = View.VISIBLE
                }else{
                    clFilterFavorite.visibility = View.GONE
                }
                isFilter = !isFilter
            }
        }
    }

    private fun searchAction(){
        binding.vpFavorite.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                var searchKeyword = ""


                binding.etFavoriteFindFavorite.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->

                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        searchKeyword = binding.etFavoriteFindFavorite.text.toString()
                        if(position != 0){
                            storeViewModel.getStoreFavorite(searchKeyword)
                        }else{
                            productViewModel.getProduct(searchKeyword)
                        }
                        return@OnEditorActionListener true
                    }

                    false
                })
            }

            override fun onPageSelected(position: Int) {

            }
        })
    }

}