package com.finalproject.thrifthouse.fragment.store

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.ProductStoreAdapter
import com.finalproject.thrifthouse.api.response.ProductStore
import com.finalproject.thrifthouse.databinding.FragmentChildrenStoreBinding
import com.finalproject.thrifthouse.databinding.FragmentFemaleStoreBinding
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.utilities.checkFavoriteProduct

import com.finalproject.thrifthouse.viewmodel.ChildrenStoreFragmentViewModel
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.FemaleStoreFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ChildrenStoreFragment(private val storeId: String) : Fragment() {
    private var _binding: FragmentChildrenStoreBinding? = null
    private val binding get() = _binding!!
    private val productViewModel: ChildrenStoreFragmentViewModel by viewModel()
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    private lateinit var productStoreAdapter: ProductStoreAdapter
    private var isLoading = false
    var page = 0
    var size = 0
    private var productList = mutableListOf<ProductModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChildrenStoreBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        fetchProduct()
        initObserver()
        initScrollListener(productList)

    }

    private fun initObserver(){

        productViewModel.getStoreProducts.observe(viewLifecycleOwner){
            productList.addAll(it)
            productStoreAdapter.setItem(favoriteViewModel,productList)
            productStoreAdapter.notifyDataSetChanged()
        }
        productViewModel.isLoading(false).observe(viewLifecycleOwner){
            if(it){
                binding.pbProduct.visibility = View.VISIBLE
            }else{
                binding.pbProduct.visibility = View.INVISIBLE
            }
        }
    }

    private fun fetchProduct(){
        productViewModel.getStoreProducts(storeId = storeId)
    }

    private fun init(){
        productStoreAdapter = ProductStoreAdapter("vertical")
        binding.rvProduct.adapter = productStoreAdapter
    }

    private fun initScrollListener(listProduct: List<ProductModel>) {
        binding.rvProduct.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val gridLayoutManager = recyclerView.layoutManager as GridLayoutManager?
                if (!isLoading) {
                    if (gridLayoutManager != null && gridLayoutManager.findLastCompletelyVisibleItemPosition() == listProduct.size - 1) {
                        // bottom of list!
                        loadMore(storeId)
                        productViewModel.isLoading(true)
                        isLoading = true
                    }
                }
            }
        })
    }

    private fun loadMore(storeId: String) {
        val handler = Handler()
        handler.postDelayed(Runnable {
            isLoading = false
            productViewModel.isLoading(false)
        }, 500)
        page += 1
        size += 20
        productViewModel.getStoreProducts(storeId = storeId, page = page, size = size)
        binding.rvProduct.post {
            productStoreAdapter.notifyDataSetChanged()
        }
    }



}