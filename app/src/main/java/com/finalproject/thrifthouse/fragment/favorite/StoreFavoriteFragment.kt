package com.finalproject.thrifthouse.fragment.favorite


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.activities.LoginActivity
import com.finalproject.thrifthouse.adapter.FavoriteStoreAdapter
import com.finalproject.thrifthouse.adapter.ProductAdapter
import com.finalproject.thrifthouse.databinding.FragmentProductFavoriteBinding
import com.finalproject.thrifthouse.databinding.FragmentStoreFavoriteBinding
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.ProductFavoriteFragmentViewModel
import com.finalproject.thrifthouse.viewmodel.StoreFavoriteFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class StoreFavoriteFragment : Fragment() {
    private var _binding: FragmentStoreFavoriteBinding? = null
    private val binding get() = _binding!!
    private val storeViewModel: StoreFavoriteFragmentViewModel by sharedViewModel()
    private lateinit var favoriteStoreAdapter: FavoriteStoreAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStoreFavoriteBinding.inflate(inflater, container, false)
        val view = binding.root
        init()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchFavoriteStore(null)
        initObserver()

    }

    private fun initObserver(){
        storeViewModel.getFavoriteStore.observe(viewLifecycleOwner){
            favoriteStoreAdapter.setFavoriteStore(it.data.stores)
            favoriteStoreAdapter.notifyDataSetChanged()
            onLoadDataVisibility()
        }
    }

    private fun init(){
        favoriteStoreAdapter = FavoriteStoreAdapter(object : FavoriteStoreAdapter.Listener{
            override fun unFavorite(storeId: String) {
                if(storeId.isNotBlank() || storeId.isNotEmpty()){
                    storeViewModel.postFavoriteStore(storeId)
                    fetchFavoriteStore(null)
                }
            }
        })
        binding.rvStore.adapter = favoriteStoreAdapter
    }


    fun fetchFavoriteStore(search: String?){
        emptyDataVisibility()
        storeViewModel.getStoreFavorite(search)
    }

    private fun emptyDataVisibility(){
        if(isVisible){
            binding.apply {
                binding.rvStore.visibility = View.GONE
                shimmerFavoriteStore.startShimmer()
                shimmerFavoriteStore.visibility = View.VISIBLE
            }
        }
    }

    private fun onLoadDataVisibility(){
        binding.apply {
            shimmerFavoriteStore.stopShimmer()
            shimmerFavoriteStore.visibility = View.GONE
            binding.rvStore.visibility = View.VISIBLE
        }
    }

}