package com.finalproject.thrifthouse.fragment.category

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.denzcoskun.imageslider.models.SlideModel
import com.finalproject.thrifthouse.activities.ProductActivity
import com.finalproject.thrifthouse.adapter.ProductAdapter
import com.finalproject.thrifthouse.databinding.FragmentProductBinding
import com.finalproject.thrifthouse.utilities.CheckAuth

import com.finalproject.thrifthouse.viewmodel.FavoriteViewModel
import com.finalproject.thrifthouse.viewmodel.ProductFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProductFragment : Fragment() {
    private var _binding: FragmentProductBinding? = null
    private val binding get() = _binding!!
    private val favoriteViewModel: FavoriteViewModel by viewModel()
    private val productViewModel: ProductFragmentViewModel by viewModel()
    private lateinit var productAdapter: ProductAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        val view = binding.root
        bannerSlider()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchProduct()

        binding.apply {

            shimmerMale.startShimmer()
            shimmerFemale.startShimmer()
            shimmerChildren.startShimmer()

            shimmerMale.visibility = View.VISIBLE
            shimmerFemale.visibility = View.VISIBLE
            shimmerChildren.visibility = View.VISIBLE

            productAdapter = ProductAdapter("horizontal")
            val layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            rvProductMale.layoutManager = layoutManager


            productViewModel.getMaleProducts.observe(viewLifecycleOwner, Observer {

                productAdapter.setItem(
                    favoriteViewModel,
                    it,
                    CheckAuth().checkAuth(application = requireActivity().application)
                )
                rvProductMale.adapter = productAdapter

                shimmerMale.stopShimmer()
                shimmerMale.visibility = View.GONE
            })


            productViewModel.getFemaleProducts.observe(viewLifecycleOwner, Observer {

                productAdapter.setItem(
                    favoriteViewModel,
                    it,
                    CheckAuth().checkAuth(application = requireActivity().application)
                )
                rvProductFemale.adapter = productAdapter

                shimmerFemale.stopShimmer()
                shimmerFemale.visibility = View.GONE
            })

            productViewModel.getChildrenProducts.observe(viewLifecycleOwner, Observer {

                productAdapter.setItem(
                    favoriteViewModel,
                    it,
                    CheckAuth().checkAuth(application = requireActivity().application)
                )
                rvProductChildren.adapter = productAdapter

                shimmerChildren.stopShimmer()
                shimmerChildren.visibility = View.GONE
            })
        }

        onClickToProduct()
    }

    private fun bannerSlider() {
        val imageList = ArrayList<SlideModel>()

        imageList.add(SlideModel(com.finalproject.thrifthouse.R.drawable.banner1))
        imageList.add(SlideModel(com.finalproject.thrifthouse.R.drawable.banner2))
        imageList.add(SlideModel(com.finalproject.thrifthouse.R.drawable.banner3))

        binding.bannerSlider.setImageList(imageList)
    }

    private fun onClickToProduct() {
        binding.apply {
            llMaleToProduct.setOnClickListener {
                val intent =
                    Intent(requireContext().applicationContext, ProductActivity::class.java)
                intent.putExtra(KEY_CATEGORY, "pria")
                requireContext().startActivity(intent)
            }

            llFemaleToProduct.setOnClickListener {
                val intent =
                    Intent(requireContext().applicationContext, ProductActivity::class.java)
                intent.putExtra(KEY_CATEGORY, "wanita")
                requireContext().startActivity(intent)
            }

            llChildrenToProduct.setOnClickListener {
                val intent =
                    Intent(requireContext().applicationContext, ProductActivity::class.java)
                intent.putExtra(KEY_CATEGORY, "anak")
                requireContext().startActivity(intent)
            }
        }
    }

    private fun fetchProduct() {
        productViewModel.getMaleProduct(sizeProduct = null, condition = null)
        productViewModel.getFemaleProduct(sizeProduct = null, condition = null)
        productViewModel.getChildrenProduct(sizeProduct = null, condition = null)
    }

    companion object {
        val KEY_CATEGORY = "key_kategory"
    }
}

