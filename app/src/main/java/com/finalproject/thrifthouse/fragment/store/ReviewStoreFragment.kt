package com.finalproject.thrifthouse.fragment.store

import android.R.string
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.finalproject.thrifthouse.R
import com.finalproject.thrifthouse.adapter.FilterRatingAdapter
import com.finalproject.thrifthouse.adapter.ReviewAdapter
import com.finalproject.thrifthouse.api.response.DataReview
import com.finalproject.thrifthouse.databinding.FragmentReviewStoreBinding
import com.finalproject.thrifthouse.utilities.DataDummy
import com.finalproject.thrifthouse.utilities.NumberConversion
import com.finalproject.thrifthouse.viewmodel.ReviewStoreFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ReviewStoreFragment(private val storeId: String) : Fragment() {

    private var _binding: FragmentReviewStoreBinding? = null
    private val binding get() = _binding!!
    private val reviewViewModel: ReviewStoreFragmentViewModel by viewModel()
    private lateinit var filterRatingAdapter: FilterRatingAdapter
    private lateinit var reviewAdapter: ReviewAdapter
    private var hasPhoto: Boolean = true
    private var rate: Int? = 5
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentReviewStoreBinding.inflate(inflater, container, false)
        val view = binding.root

        init()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchReview(null, hasPhoto)

        initObserver()
        binding.apply {
            cbHasPhoto.isChecked = true
            cbHasPhoto.setOnCheckedChangeListener{buttonView, isChecked ->
                hasPhoto = isChecked
                fetchReview(rate, isChecked)
            }
        }
    }

    private fun initObserver(){
        reviewViewModel.getAllReview.observe(viewLifecycleOwner){
            setData(it.data)
        }
    }

    fun init(){
        filterRatingAdapter = FilterRatingAdapter(object : FilterRatingAdapter.Listener{
            override fun onItemClicked(isSelected: Int) {
                rate = isSelected
                fetchReview(rate, hasPhoto)
            }
        })
        binding.rvRating.adapter = filterRatingAdapter
        filterRatingAdapter.setRating(DataDummy().dataRating)
        reviewAdapter = ReviewAdapter()
        binding.rvReview.adapter = reviewAdapter
    }

    fun fetchReview(rate: Int?, hasPhoto: Boolean){
        if(rate != null){
            reviewViewModel.getAllReview(storeId, rate = rate, hasPhoto = hasPhoto)
            binding.apply {
                llOne.visibility = View.GONE
                pbReview.visibility = View.VISIBLE
            }
            reviewAdapter.notifyDataSetChanged()
        }else{
            reviewViewModel.getAllReview(storeId, rate = null, hasPhoto = hasPhoto)
            binding.apply {
                llOne.visibility = View.GONE
                pbReview.visibility = View.VISIBLE
            }
        }
    }

    private fun setData(dataReview: DataReview){
        binding.apply {
            llOne.visibility = View.VISIBLE
            pbReview.visibility = View.GONE
        }

        binding.apply {
            tvAverageRating.text = NumberConversion.conversionDecimalToTwoDigitNumber(dataReview.totalRate)
            tvTotalCostumer.text = "${dataReview.totalSelling} ${getString(R.string.text_total_costumer_store)}"
            tvRating.text = "${dataReview.totalRated.toString()} rating"
            tvReview.text = "${dataReview.totalReview.toString()} ulasan"
        }

        reviewAdapter.setReviews(dataReview.review)
    }

}