package com.finalproject.thrifthouse.fragment

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.finalproject.thrifthouse.activities.CartActivity
import com.finalproject.thrifthouse.activities.LoginActivity
import com.finalproject.thrifthouse.activities.ProductActivity
import com.finalproject.thrifthouse.adapter.CategoryAdapter
import com.finalproject.thrifthouse.databinding.FragmentHomeBinding
import com.finalproject.thrifthouse.fragment.category.ProductFragment
import com.finalproject.thrifthouse.utilities.CheckAuth
import com.finalproject.thrifthouse.viewmodel.HomeFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val userViewModel: HomeFragmentViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initData()
        initAction()
        initObserver()
        initListener()

    }

    private fun initListener() {
        binding.apply {

            vpCategory.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                }

                override fun onPageSelected(position: Int) {
                    if (position != 0) {
                        binding.headerOne.visibility = View.GONE
                        binding.etProductOne.visibility = View.GONE
                        binding.headerTwo.visibility = View.VISIBLE
                    } else {
                        binding.headerOne.visibility = View.VISIBLE
                        binding.etProductOne.visibility = View.VISIBLE
                        binding.headerTwo.visibility = View.GONE
                    }
                }
            })

            etProductOne.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    val keyword = etProductOne.text.toString()
                    val intent = Intent(requireActivity(), ProductActivity::class.java)
                    intent.putExtra(KEY_KEYWORD, keyword)
                    requireContext().startActivity(intent)

                    return@setOnKeyListener true
                }
                return@setOnKeyListener false
            }

            etProductOne.addTextChangedListener {
                if (it != null) {
                    etProductOne.setCompoundDrawables(null, null, null, null)
                }
            }

            etProductTwo.setOnKeyListener { v, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    val keyword = etProductTwo.text.toString()
                    val intent = Intent(requireActivity(), ProductActivity::class.java)
                    intent.putExtra(KEY_KEYWORD, keyword)
                    requireContext().startActivity(intent)

                    return@setOnKeyListener true
                }
                return@setOnKeyListener false
            }

            etProductTwo.addTextChangedListener {
                if (it != null) {
                    etProductOne.setCompoundDrawables(null, null, null, null)
                }
            }

        }
    }

    private fun initData() {
        userViewModel.getUserDetail()
    }

    private fun init() {
        binding.apply {
            vpCategory.adapter = CategoryAdapter(childFragmentManager)
            tabCategory.setupWithViewPager(vpCategory)

            val tabs = tabCategory.getChildAt(0) as? ViewGroup
            for (i in 0 until tabs!!.childCount) {
                val tab = tabs.getChildAt(i)
                val layoutParams = tab.layoutParams as LinearLayout.LayoutParams
                layoutParams.weight = 0f
                layoutParams.marginEnd = 12
                layoutParams.marginStart = 12
                tab.layoutParams = layoutParams
            }
            vpCategory.requestLayout()
        }
    }

    private fun initObserver() {
        userViewModel.getUserDetail.observe(viewLifecycleOwner) {
            binding.tvWelcomeName.text = "Hi, ${it.data.username}"
        }
    }

    private fun initAction() {
        binding.apply {
            ivBag.setOnClickListener {
                if (CheckAuth().checkAuth(requireActivity().application)) {
                    val intent = Intent(requireActivity(), CartActivity::class.java)
                    startActivity(intent)

                } else {
                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    startActivity(intent)
                }
            }

            ivBagTwo.setOnClickListener {
                if (CheckAuth().checkAuth(requireActivity().application)) {
                    val intent = Intent(requireActivity(), CartActivity::class.java)
                    startActivity(intent)

                } else {
                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }

    companion object {
        val KEY_KEYWORD = "key_keyword"
    }


}