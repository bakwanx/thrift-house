package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService
import com.finalproject.thrifthouse.api.response.PostAddress

class AddressRepository(private val apiService: ApiService) {

    suspend fun getProvince() = apiService.getListProvince()
    suspend fun getCity(provinceId: String) = apiService.getListCity(provinceId)
    suspend fun postAddress(userId: String, postAddress: PostAddress) = apiService.postAddress(userId, postAddress)
    suspend fun getListAddress(userId: String) = apiService.getListAddress(userId)

}