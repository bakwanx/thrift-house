package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService
import com.finalproject.thrifthouse.api.response.PostToGetDeliveryService
import com.finalproject.thrifthouse.model.PostCheckOutModel
import okhttp3.MultipartBody
import okhttp3.RequestBody

class OrderRepository(private val apiService: ApiService) {

    suspend fun postOrder(postCheckOutModel: PostCheckOutModel) =
        apiService.postOrder(postCheckOutModel)

    suspend fun getPayment(orderId: String) = apiService.getPayment(orderId)
    suspend fun getListOrder(userId: String, status: String) =
        apiService.getOrderList(userId, status)

    suspend fun putReceipt(orderId: String, photo: MultipartBody.Part) =
        apiService.putReceipt(orderId, photo)

    suspend fun getDetailOrder(userId: String, orderId: String, status: String?) =
        apiService.getDetailOrder(userId = userId, orderId = orderId, status = status)

    suspend fun getUserDetail(userId: String) = apiService.getUserDetail(userId)
    suspend fun acceptOrder(userId: String, transactionId: String) =
        apiService.acceptOrder(userId, transactionId)

    suspend fun postReview(
        storeId: String,
        anonim: RequestBody,
        description: RequestBody,
        rating: RequestBody,
        userId: RequestBody,
        photo: MultipartBody.Part
    ) = apiService.postReview(
        storeId,
        anonim,
        description,
        rating,
        userId,
        photo
    )

}