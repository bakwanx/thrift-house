package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService
import com.finalproject.thrifthouse.api.response.FavoriteStore

class StoreRepository(private val apiService: ApiService) {
    suspend fun getStore(storeId: String) = apiService.getDetailStore(storeId)
    suspend fun getBanner(storeId: String) = apiService.getStoreBanner(storeId)
    suspend fun getStoreProducts(storeId: String,page: Int, size: Int) = apiService.getStoreProducts(storeId, page, size)
    suspend fun getStoreProductsMale(storeId: String,page: Int, size: Int) = apiService.getStoreProductsCategory(storeId, page, size, "pria")
    suspend fun getStoreProductsFemale(storeId: String,page: Int, size: Int) = apiService.getStoreProductsCategory(storeId, page, size, "wanita")
    suspend fun getStoreProductsChildren(storeId: String,page: Int, size: Int) = apiService.getStoreProductsCategory(storeId, page, size, "anak")
    suspend fun getAllReview(storeId: String,page: Int, size: Int, rate: Int?, hasPhoto: Boolean) = apiService.getAllReviewStore(storeId, page, size, rate, hasPhoto)
    suspend fun getAboutStore(storeId: String) = apiService.getAboutStore(storeId)
    suspend fun postFavoriteStore(userId: String, favoriteStore: FavoriteStore) = apiService.postFavoriteStore(userId,favoriteStore)
    suspend fun getFavoriteStore(userId: String, search: String?) = apiService.getFavoriteStore(userId, search)
}