package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService
import com.finalproject.thrifthouse.api.response.PostAddress
import com.finalproject.thrifthouse.api.response.PostToGetDeliveryService

class DeliveryServiceRepository(private val apiService: ApiService) {
    suspend fun postCostDeliveryService(postToGetDeliveryService: PostToGetDeliveryService) =
        apiService.postCostDeliveryService(postToGetDeliveryService)

    suspend fun getDeliveryService(storeId: String) = apiService.getDeliveryService(storeId)
}