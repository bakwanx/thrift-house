package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService

class BankRepository(private val apiService: ApiService) {

    suspend fun getBanks() = apiService.getBank()

}