package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService

class NotificationRepository(private val apiService: ApiService) {

    suspend fun getAllNotification(userId: String) = apiService.getNotification(userId, null)
    suspend fun getSpecificNotification(userId: String, type: String) = apiService.getNotification(userId, type)

}