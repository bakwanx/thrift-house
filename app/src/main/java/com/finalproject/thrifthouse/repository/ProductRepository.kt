package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService
import com.finalproject.thrifthouse.api.response.DeleteProductCart
import com.finalproject.thrifthouse.api.response.FavoriteProduct
import com.finalproject.thrifthouse.api.response.PostProductCart


class ProductRepository(private val apiService: ApiService) {

    suspend fun getMaleProduct(page: Int, size: Int, sizeProduct: String?, condition: String?, sortBy: String?) =
        apiService.getProducts(
            "pria",
            subCategory1 = null,
            subCategory2 = null,
            search = null,
            page,
            size,
            sizeProduct,
            condition,
            false,
            sortBy
        )

    suspend fun getFemaleProduct(page: Int, size: Int, sizeProduct: String?, condition: String?, sortBy: String?) =
        apiService.getProducts(
            "wanita",
            subCategory1 = null,
            subCategory2 = null,
            search = null,
            page,
            size,
            sizeProduct,
            condition,
            false,
            sortBy
        )

    suspend fun getChildren(page: Int, size: Int, sizeProduct: String?, condition: String?, sortBy: String?) =
        apiService.getProducts(
            "anak",
            subCategory1 = null,
            subCategory2 = null,
            search = null,
            page,
            size,
            sizeProduct,
            condition,
            false,
            sortBy
        )

    suspend fun getProductDetail(idProduct: String) = apiService.getProductDetail(idProduct)
    suspend fun getProduct(
        category: String?,
        subCategory1: String?,
        subCategory2: String?,
        search: String?,
        page: Int,
        size: Int,
        sizeProduct: String?,
        condition: String?,
        sortBy: String?
    ) = apiService.getProducts(
        category,
        subCategory1 = subCategory1,
        subCategory2 = subCategory2,
        search,
        page,
        size,
        sizeProduct,
        condition,
        false,
        sortBy
    )

    suspend fun getStoreProducts(storeId: String, page: Int, size: Int) =
        apiService.getStoreProducts(storeId, page, size)

    suspend fun postFavoriteProduct(userId: String, favoriteProduct: FavoriteProduct) =
        apiService.postFavoriteProduct(userId, favoriteProduct)

    suspend fun getFavoriteProduct(userId: String, search: String?) =
        apiService.getFavoriteProduct(userId, search)

    suspend fun getCart(userId: String, token: String) =
        apiService.getListCart(userId = userId, token = token)

    suspend fun postCartProduct(userId: String, token: String, postProductCart: PostProductCart) =
        apiService.postCart(userId = userId, token = token, postProductCart = postProductCart)

    suspend fun deleteCartProduct(
        userId: String,
        token: String,
        deleteProductCart: DeleteProductCart
    ) = apiService.deleteCart(userId = userId, token = token, deleteProductCart = deleteProductCart)
}