package com.finalproject.thrifthouse.repository

import com.finalproject.thrifthouse.api.ApiService
import com.finalproject.thrifthouse.api.response.UserLogin
import com.finalproject.thrifthouse.api.response.UserRegister

class UserRepository(private val apiService: ApiService) {

    suspend fun login(userLogin: UserLogin) = apiService.login(userLogin)

    suspend fun register(userRegister: UserRegister) = apiService.register(userRegister)

    suspend fun getUser(userId: String) = apiService.getUserDetail(userId)
}