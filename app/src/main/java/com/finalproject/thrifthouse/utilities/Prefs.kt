package com.finalproject.thrifthouse.utilities

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class Prefs {
    companion object{
        const val KEY_USER_ID = "key_id"
        const val KEY_TOKEN = "key_token"
        fun getSharedPrefs(androidApplication: Application): SharedPreferences {
            return androidApplication.getSharedPreferences("kotlinsharedpreference", Context.MODE_PRIVATE)
        }
    }
}