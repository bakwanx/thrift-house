/*
 * Copyright (C) 2019. PT SehatQ Harsana Emedika - All Rights Reserved
 */

package com.she.sehatq.base.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


abstract class EndlessScrollListener(private val layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {

    private var visibleThreshold = 2
    private var currentPage = 1
    private var previousTotalItemCount = 0
    private var loading = true


    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        val totalItemCount = layoutManager.itemCount
        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()

        if (loading && totalItemCount >= previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        if (!loading && lastVisibleItemPosition + visibleThreshold >= totalItemCount) {
            currentPage++
            onLoadMore(currentPage, totalItemCount, view)
            loading = true
        }
    }

    fun resetScrolling(){
        loading =false
        currentPage = 1
        previousTotalItemCount = 0
    }

    abstract fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView)
}