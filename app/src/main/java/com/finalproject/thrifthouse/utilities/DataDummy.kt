package com.finalproject.thrifthouse.utilities

import com.finalproject.thrifthouse.R

class DataDummy {
    val dataSubCategory = listOf("Atasan", "Bawahan", "Luaran", "Sepatu", "Aksesoris")
    val dataGuidePayment = listOf("Internet Banking BNI", "Mobile Banking BNI", "SMS Banking BNI", "ATM BNI")
    val dataMenuStore = listOf("Total Ulasan", "Rata-rata Ulasan", "Total Produk", "Favorit Toko")
    val dataIcMenuStore = listOf(R.drawable.ic_review, R.drawable.ic_star, R.drawable.ic_shirt, R.drawable.ic_favorite)
    val dataRating = listOf("5","4","3","2","1")
    val dataAtasanPria = listOf("Kaos lengan pendek", "Kaos lengan panjang","Kemeja","Polo")
    val dataBawahanPria = listOf("Celana pendek", "Celana panjang", "Casual", "Jeans")
    val dataLuaranPria = listOf("Crewneck", "Hoodie", "Jaket", "Jas", "Sweater")
    val dataSepatuPria = listOf("Boots", "Formal", "Sendal", "Slip on", "Sneakers", "Olahraga")
    val dataAksesorisPria = listOf("Ikat pinggang", "Jam", "Kacamata", "Topi")
    val dataAtasanWanita = listOf("Kaos lengan pendek & 3/4", "Kaos lengan panjang", "Kemeja & Blus", "Gaun")
    val dataBawahanWanita = listOf("Celana pendek", "Celana panjang", "Rok pendek & 3/4", "Rok panjang")
    val dataLuaranWanita = listOf("Cardigan", "Crewneck", "Hoodie", "Jaket", "Jas", "Mantel", "Sweater")
    val dataSepatuWanita = listOf("Boots", "Heels & Wedges", "Sendal", "Slip on", "Sneakers", "Olahraga")
    val dataAksesorisWanita = listOf("Ikat pinggang", "Jam", "Kacamata", "Topi")
    val dataAtasanAnak = listOf("Kaos lengan pendek", "Kaos lengan panjang","Kemeja & Blus", "Gaun", "Polo")
    val dataBawahanAnak = listOf("Celana pendek", "Celana panjang", "Rok pendek & 3/4", "Rok panjang")
    val dataLuaranAnak = listOf("Cardigan", "Crewneck", "Hoodie", "Jaket", "Jas", "Mantel", "Sweater")
    val dataSepatuAnak = listOf("Boots", "Formal", "Heels & Wedges", "Sendal", "Slip on", "Sneakers", "Olahraga")
    val dataAksesorisAnak = listOf("Kacamata", "Topi")
    val dataFilterCategory = listOf("Pria", "Wanita", "Anak")
    val dataFilterBrand = listOf("Adidas", "Nike", "Puma", "Dior", "Kenzo", "Pull&Bear")
    val dataFilterUkuran = listOf("S", "M", "L", "XL")
    val dataFilterKondisi = listOf("Baru Dengan Tag", "Seperti Baru", "Baik", "Cukup")
}
