package com.finalproject.thrifthouse.utilities

import android.util.Log
import com.finalproject.thrifthouse.api.response.ResultDeliveryService
import com.finalproject.thrifthouse.model.CartModel
import com.finalproject.thrifthouse.model.CheckOutModel

class GroupingCheckout {
    companion object{
        fun groupingCheckoutModel(
            cartModels: List<CartModel>,
            resultDeliveryServices: MutableList<ResultDeliveryService>
        ):MutableList<CheckOutModel> {
            val checkOutModels = mutableListOf<CheckOutModel>()

            resultDeliveryServices.forEach {
                Log.d("TAG", "groupingCheckoutModel data: ${it.name}")
            }

            cartModels.forEachIndexed { i, cartModel ->
                val checkoutModel = CheckOutModel(
                    id = cartModel.id,
                    name = cartModel.name,
                    province = cartModel.province,
                    province_id = cartModel.province_id,
                    city = cartModel.city,
                    city_id = cartModel.city_id,
                    photo = cartModel.photo,
                    product = cartModel.product,
                    resultDeliveryService = resultDeliveryServices.get(0)
                )
                checkOutModels.add(checkoutModel)
            }


            return checkOutModels
        }
    }
}