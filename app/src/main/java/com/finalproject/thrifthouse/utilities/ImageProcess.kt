package com.finalproject.thrifthouse.utilities

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.res.AssetFileDescriptor
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.MediaStore.Images
import java.io.*


class ImageProcess {

    companion object{
        fun getImageUri(context: Context, inImage: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes)
            val path = Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null)
            return Uri.parse(path)
        }

        fun getRealPathFromURI(context: Context, uri: Uri): String {
            val cursor: Cursor = context.getContentResolver().query(uri, null, null, null, null) as Cursor
            cursor.moveToFirst()
            val idx: Int = cursor.getColumnIndex(Images.ImageColumns.DATA)
            return cursor.getString(idx)
        }

        fun compressGetImageFilePath(
            mContext: Context,
            imageSelectedUri: Uri,
        ): String {
            //region Getting the photo to process it
            val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ImageDecoder.decodeBitmap(ImageDecoder.createSource(mContext.contentResolver, imageSelectedUri))
            } else {
                mContext.contentResolver.openInputStream(imageSelectedUri)?.use { inputStream ->
                    BitmapFactory.decodeStream(inputStream)
                }
            }
            // endregion

            //region save photo to gallery using Scoped storage
            val values = ContentValues().apply {
                put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
                put(MediaStore.Images.Media.IS_PENDING, 1)
            }

            val collection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
            val imageUri = mContext.contentResolver.insert(collection, values)
            //endregion


            //region save image
            mContext.contentResolver.openOutputStream(imageUri!!).use { out ->
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, out)
            }

            values.clear()
            values.put(MediaStore.Images.Media.IS_PENDING, 0)
            mContext.contentResolver.update(imageUri, values, null, null)
            //endregion


            //region get file path of content uri
            val file = File(mContext.cacheDir, "Bukti_TF.png")
            try {
                val assetFileDescriptor: AssetFileDescriptor = mContext.contentResolver.openAssetFileDescriptor(imageUri, "r")!!

                val inputStream = FileInputStream(assetFileDescriptor.fileDescriptor)
                val outputStream = FileOutputStream(file)
                inputStream.copyTo(outputStream)

                inputStream.close()
                outputStream.close()


            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            //endregion

            return file.path

        }
    }
}