package com.finalproject.thrifthouse.utilities

import com.finalproject.thrifthouse.api.response.ProductCart
import com.finalproject.thrifthouse.model.CartModel

class GroupingCart {
    companion object{
        fun grouping(productCarts: List<ProductCart>): ArrayList<CartModel>{

            val cartList: ArrayList<CartModel> = ArrayList()
            if(cartList.isNotEmpty()){
                cartList.clear()
            }
            val grouping = productCarts.groupBy  {
                it.store
            }.mapValues {
                    item -> item.value
            }
            grouping.forEach { (k, v) ->
                val cartModel = CartModel(
                    id = k.id,
                    name= k.name,
                    province= k.province,
                    province_id= k.province_id,
                    city= k.city,
                    city_id= k.city_id,
                    photo= k.photo,
                    product= v
                )
                cartList.add(cartModel)
            }

            return cartList
        }
    }
}