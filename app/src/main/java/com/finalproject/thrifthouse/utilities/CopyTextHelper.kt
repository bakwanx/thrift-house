package com.finalproject.thrifthouse.utilities

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context

class CopyTextHelper {
    companion object{
        fun copyFileText(context: Context, text: String?) {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(android.R.attr.label.toString(), text)
            clipboard.setPrimaryClip(clip)
        }
    }
}