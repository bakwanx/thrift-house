package com.finalproject.thrifthouse.utilities

import android.app.Application
import android.content.SharedPreferences

class CheckAuth {
    fun checkAuth(application: Application): Boolean{
        val sharedPreferences: SharedPreferences = Prefs.getSharedPrefs(application)
        val sharedIdUser = sharedPreferences.getString(Prefs.KEY_USER_ID, "")

        if(sharedIdUser.equals("")){
            return false
        }else{
            return true
        }
    }
}