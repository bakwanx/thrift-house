package com.finalproject.thrifthouse.utilities

class GlobalConfig {

    companion object{
        val baseUrl = "https://thrifthouse.herokuapp.com/"
    }
}