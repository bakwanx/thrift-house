package com.finalproject.thrifthouse.utilities

import java.text.DecimalFormat

class NumberConversion {
    companion object{
        fun conversionDecimalToTwoDigitNumber(number: Double): String{
            val number3digits:Double = String.format("%.3f", number).toDouble()
            val number2digits:Double = String.format("%.2f", number3digits).toDouble()
            val result:Double = String.format("%.1f", number2digits).toDouble()
            return result.toString()
        }

        fun conversionToRupiah(number: Double): String{
            val formatter = DecimalFormat("###,###,##0.00")
            val result = "Rp. ${formatter.format(number)}"
            return result.substring(0, result.length - 3)
        }

    }
}