package com.finalproject.thrifthouse.utilities

import android.content.Context
import android.widget.Toast

class ToastMessage {
    companion object{
        fun showToast(context: Context, message: String){
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}