package com.finalproject.thrifthouse.utilities
import com.finalproject.thrifthouse.api.response.ListProduct
import com.finalproject.thrifthouse.api.response.ProductStore
import com.finalproject.thrifthouse.model.ProductModel
import com.finalproject.thrifthouse.room.FavoriteDao


fun checkFavoriteProduct(
    favoriteDao: FavoriteDao,
    listProduct: List<ListProduct>?,
    listProductStore: List<ProductStore>?
): ArrayList<ProductModel> {
    val productModels: ArrayList<ProductModel> = ArrayList()

    val result = favoriteDao.getAllFavoriteProduct()

    if(listProduct != null){
        for (item in listProduct){
            val productModel = ProductModel(
                id = item.id,
                name = item.name,
                brand = item.brand,
                size =  item.size,
                condition = item.condition,
                price = item.price,
                photos = item.photos,
                category = item.category
            )
            productModels.add(productModel)
        }
    }

    for(i in productModels.indices){
        for (itemLocal in result){
            if(productModels.get(i).id == itemLocal.id){
                productModels.get(i).isFavorite = true
            }
        }
    }

    if(listProductStore != null){
        for (item in listProductStore){
            val productModel = ProductModel(
                id = item.productId,
                name = item.name,
                brand = item.brand,
                size =  item.size,
                condition = item.condition,
                price = item.price.toDouble(),
                photos = item.photos,
                category = item.category
            )
            productModels.add(productModel)
        }
    }

    for(i in productModels.indices){
        for (itemLocal in result){
            if(productModels.get(i).id == itemLocal.id){
                productModels.get(i).isFavorite = true
            }
        }
    }

    /*
    if(listProduct != null){
        if(result.isEmpty()){
            for (item in listProduct){
                val productModel = ProductModel(
                    id = item.id,
                    name = item.name,
                    brand = item.brand,
                    size =  item.size,
                    condition = item.condition,
                    price = item.price,
                    photos = item.photos,
                    category = item.category,
                    isFavorite = false
                )
                productModels.add(productModel)
            }
        } else {
            for (item in listProduct){
                for (itemLocal in result){
                    if(item.id == itemLocal.id){
                        val productModel = ProductModel(
                            id = item.id,
                            name = item.name,
                            brand = item.brand,
                            size =  item.size,
                            condition = item.condition,
                            price = item.price,
                            photos = item.photos,
                            category = item.category,
                            isFavorite = true
                        )
                        productModels.add(productModel)
                    }
                }

                for (filterItem in productModels){
                    if(filterItem.id != item.id){

                        val productModel = ProductModel(
                            id = item.id,
                            name = item.name,
                            brand = item.brand,
                            size =  item.size,
                            condition = item.condition,
                            price = item.price,
                            photos = item.photos,
                            category = item.category,
                            isFavorite = false
                        )
                        productModels.add(productModel)
                    }
                }
            }
            Log.d("TAG", "pesan ada data ${productModels[0].brand}")
        }
    }


    */
    return productModels
}
