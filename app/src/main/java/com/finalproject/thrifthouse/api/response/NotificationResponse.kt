package com.finalproject.thrifthouse.api.response


data class NotificationResponse (
    val data: List<Notification>,
    val statusCode: Long,
    val message: String
)

data class Notification (
    val id: String,
    val userId: String,
    val title: String,
    val body: String,
    val type: String,
    val image: String,
    val status: String,
    val notificationAt: String,
    val readAt: String,
    val updatedAt: String
)
