package com.finalproject.thrifthouse.api.response
import com.google.gson.annotations.SerializedName
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ReviewResponse (
    val data: DataReview,
    val statusCode: Long,
    val message: String
)

@Serializable
data class DataReview (
    val totalRate: Double,
    val totalSelling: Long,
    val totalReview: Long,
    val totalRated: Long,
    val review: List<Review>
)

@Serializable
data class Review (
    val description: String? = null,
    val rating: Double,
    val photos: List<String>,
    val createdAt: String,
    val user: User
)

@Serializable
data class User (
    val id: String,
    val username: String
)
