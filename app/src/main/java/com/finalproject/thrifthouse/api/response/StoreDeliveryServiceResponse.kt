package com.finalproject.thrifthouse.api.response

data class StoreDeliveryServiceResponse (
    val data: DeliveryService,
    val statusCode: Long,
    val message: String
): java.io.Serializable


data class DeliveryService (
    val deliveryService: String
): java.io.Serializable
