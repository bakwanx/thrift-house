package com.finalproject.thrifthouse.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ProductResponse (
    val data: ProductResponseData,
    val statusCode: Long,
    val message: String
)

@Serializable
data class ProductResponseData (
    val totalItems: Long,
    val products: List<ListProduct>,
    val totalPages: Long,
    val currentPage: Long
)

data class ListProduct(
    @SerializedName("id")
    val id :String,
    @SerializedName("name")
    val name : String,
    @SerializedName("brand")
    val brand : String,
    @SerializedName("size")
    val size:  String,
    @SerializedName("condition")
    val condition : String,
    @SerializedName("price")
    val price : Double,
    @SerializedName("photos")
    val photos :String,
    @SerializedName("category")
    val category : String
)

@Serializable
data class ProductDetailResponse (
    val data: DataProductDetail,
    val statusCode: Long,
    val message: String
)

@Serializable
data class DataProductDetail (
    val store: Store,
    val product: DetailProduct
)

@Serializable
data class DetailProduct (
    val id: String,
    val name: String,
    val brand: String,
    val size: String,
    val condition: String,
    val price: Double,
    val height: Long,
    val width: Long,
    val material: String,
    val description: String,
    val photos: List<String>,
    val category: String
)



@Serializable
data class BannerResponse (
    val data: Banner,
    val message: String,
    val statusCode: Long
)

@Serializable
data class Banner (
    val banner: String
)













