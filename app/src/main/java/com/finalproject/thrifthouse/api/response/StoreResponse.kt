package com.finalproject.thrifthouse.api.response

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class StoreProductResponse (
    @SerializedName("data") val listProduct :List<ProductStore>,
    @SerializedName("statusCode") val statusCode : Long,
    @SerializedName("message") val message : String
)

@Serializable
data class StoreResponse (
    val data: Store,
    val message: String,
    val statusCode: Long
)

data class Store (
    val id: String,
    val name: String,
    val province: String,
    val province_id: String,
    val city: String,
    val city_id: String,
    val logo: String,
    val totalProduct: Long,
    val totalReview: Long,
    val averageReview: Double,
    val favoriteStore: Long
): java.io.Serializable


data class ProductStore (
    val storeId: String,
    val productId: String,
    val name: String,
    val brand: String,
    val size: String,
    val condition: String,
    val price: Long,
    val photos: String,
    val category: String
): java.io.Serializable

@Serializable
data class AboutStoreResponse (
    val data: AboutStore,
    val statusCode: Long,
    val message: String
)

@Serializable
data class AboutStore (
    val about: String
)

@Serializable
data class FavoriteStorePostResponse (
    val data: FavoriteDataStoreResponse,
    val statusCode: Long,
    val message: String
)

@Serializable
data class FavoriteDataStoreResponse (
    @SerialName("storeId")
    val storeID: String
)

class FavoriteStore(
    val storeId: String
)


@Serializable
data class FavoriteStoreGetResponse (
    val data: FavoriteStoreGetResponseData,
    val statusCode: Long,
    val message: String
)

@Serializable
data class FavoriteStoreGetResponseData (
    val totalItems: Long,
    val stores: List<Store>,
    val totalPages: Long,
    val currentPage: Long
)
