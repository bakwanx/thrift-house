package com.finalproject.thrifthouse.api.response
import kotlinx.serialization.Serializable

class PostAddress(
    val addressLabel: Boolean,
    val city: String,
    val detail: String,
    val district: String,
    val fullAddress: String,
    val idCity: Long,
    val idProvince: Long,
    val postalCode: String,
    val province: String,
    val recipientName: String,
    val recipientPhone: String,
    val village: String
)

@Serializable
data class AddressResponse (
    val data: List<Address>,
    val statusCode: Long,
    val message: String
)

@Serializable
data class Address (
    val id: String,
    val recipientName: String,
    val recipientPhone: String,
    val addressLabel: Boolean,
    val idProvince: Long,
    val province: String,
    val idCity: Long,
    val city: String,
    val district: String,
    val village: String,
    val fullAddress: String,
    val postalCode: String,
    val detail: String
): java.io.Serializable
