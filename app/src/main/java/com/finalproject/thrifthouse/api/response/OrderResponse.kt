package com.finalproject.thrifthouse.api.response

import java.io.Serializable


data class PostOrderResponse (
    val data: PostOrderId,
    val statusCode: Long,
    val message: String
): Serializable

data class PostOrderId (
    val order_id_1: String
): Serializable

data class OrderListResponse (
    val data: List<OrderList>,
    val statusCode: Long,
    val message: String
): Serializable


data class OrderList (
    val orderCode: String,
    val id: String,
    val store: List<StoreOrderList>,
    val packagingStatus: String,
    val status: String
): Serializable


data class StoreOrderList (
    val province: String,
    val province_id: String,
    val city: String,
    val name: String,
    val photo: String,
    val id: String,
    val city_id: String,
    val products: List<ProductOrderList>
): Serializable


data class ProductOrderList (
    val id: String,
    val name: String,
    val brand: String,
    val size: String,
    val condition: String,
    val photo: String,
    val price: Long,
    val weight: Long
): Serializable


data class PutOrderResponse (
    val data: PutOrder,
    val statusCode: Long,
    val message: String
): Serializable

data class PutOrder (
    val id: String,
    val url: String
): Serializable

