package com.finalproject.thrifthouse.api.response

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserResponse (
    val id: String,
    val username: String,
    val email: String,
    val phone: String,
    val profileImg: String,
    val access_token: String,
    val role: String
)

class UserLogin(
    val username: String,
    val password: String,
)

class UserRegister(
    val username: String,
    val email: String,
    val password: String,
    val phone: String,
    val role: String
)

@Serializable
data class GetUserResponse (
    val data: GetUserResponseData,
    val statusCode: Long,
    val message: String
)

@Serializable
data class GetUserResponseData (
    val id: String,
    val username: String,
    val password: String,
    val email: String,
    val phone: String,
    val profileImg: String,
    val role: String,
    val fullname: String,
    val gender: String,
    val birth: String
)
