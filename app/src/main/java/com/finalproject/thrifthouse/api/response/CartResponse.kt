package com.finalproject.thrifthouse.api.response


data class CartResponse (
    val data: List<ProductCart>,
    val statusCode: Long,
    val message: String
): java.io.Serializable


data class ProductCart (
    val id: String,
    val name: String,
    val brand: String,
    val size: String,
    val condition: String,
    val photo: String,
    val price: Long,
    val weight: Long,
    val store: StoreCart
): java.io.Serializable

data class StoreCart (
    val id: String,
    val name: String,
    val province: String,
    val province_id: String,
    val city: String,
    val city_id: String,
    val photo: String
): java.io.Serializable

class PostProductCart(
    val productId: String
)

class DeleteProductCart(
    val productIds: List<String>
)
