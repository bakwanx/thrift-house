package com.finalproject.thrifthouse.api.response

data class PostToGetDeliveryService (
    val courier: String,
    val destination: String,
    val origin: String,
    val weight: String
)

data class DeliveryServiceResponse (
    val rajaongkir: RajaongkirDeliveryService
)


data class RajaongkirDeliveryService (
    val query: QueryDeliveryService,
    val status: StatusDeliveryService,
    val origin_details: NDetailsDeliveryService,
    val destination_details: NDetailsDeliveryService,
    val results: List<ResultDeliveryService>
)


data class NDetailsDeliveryService (
    val city_id: String,
    val province_id: String,
    val province: String,
    val type: String,
    val city_name: String,
    val postal_code: String
)


data class QueryDeliveryService (
    val origin: String,
    val destination: String,
    val weight: Long,
    val courier: String
)


data class ResultDeliveryService (
    val code: String,
    val name: String,
    val costs: List<ResultCostDeliveryService>
)


data class ResultCostDeliveryService (
    val service: String,
    val description: String,
    val cost: List<CostCost>
)


data class CostCost (
    val value: Long,
    val etd: String,
    val note: String
)


data class StatusDeliveryService (
    val code: Long,
    val description: String
)
