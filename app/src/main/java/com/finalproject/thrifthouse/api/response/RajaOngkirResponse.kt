package com.finalproject.thrifthouse.api.response
import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class ProvinceRajaOnkgirResponse (
    val rajaongkir: RajaongkirProvince
)

@Serializable
data class RajaongkirProvince (
    val query: JsonArray,
    val status: Status,
    val results: List<Province>
)

@Serializable
data class Province (
    val province_id: String,
    val province: String
)

@Serializable
data class Status (
    val code: Long,
    val description: String
)


@Serializable
data class CityRajaOnkgirResponse (
    val rajaongkir: RajaOngkirCity
)

@Serializable
data class RajaOngkirCity (
    val query: QueryProvince,
    val status: Status,
    val results: List<City>
)

@Serializable
data class QueryProvince (
    val province: String
)

@Serializable
data class City (
    val city_id: String,
    val province_id: String,
    val province: String,
    val type: String,
    val city_name: String,
    val postal_code: String
)

class ResultProvinceCity(
    var city_id: String? = null,
    var province_id: String? = null,
    var province: String? = null,
    var type: String? = null,
    var city_name: String? = null,
    var postal_code: String? = null
): java.io.Serializable


