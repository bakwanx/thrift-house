package com.finalproject.thrifthouse.api.response

import java.io.Serializable

data class DetailTransactionResponse (
    val data: DataDetailTransaction,
    val statusCode: Long,
    val message: String
): Serializable


data class DataDetailTransaction (
    val shippingCost: Long,
    val address: AddressDetailTransaction,
    val productsPrice: Long,
    val packagingStatus: String,
    val orderCode: String,
    val id: String,
    val store: List<StoreDetailTransaction>,
    val status: String
): Serializable


data class AddressDetailTransaction (
    val id: String,
    val recipientName: String,
    val recipientPhone: String,
    val addressLabel: Boolean,
    val idProvince: Long,
    val province: String,
    val idCity: Long,
    val city: String,
    val district: String,
    val village: String,
    val fullAddress: String,
    val postalCode: String,
    val detail: String
): Serializable


data class StoreDetailTransaction (
    val deliveryService: String,
    val province: String,
    val province_id: String,
    val city: String,
    val name: String,
    val photo: String,
    val id: String,
    val shippingService: String,
    val estimatedTimeOfDeparture: String,
    val city_id: String?,
    val products: List<ProductDetailTransaction>
): Serializable

data class ProductDetailTransaction (
    val id: String,
    val name: String,
    val brand: String,
    val size: String,
    val condition: String,
    val photo: String,
    val price: Long,
    val weight: Long,
    val category: String,
    val subcategory1: String,
    val subcategory2: String
): Serializable
