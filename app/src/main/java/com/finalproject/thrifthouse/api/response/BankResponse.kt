package com.finalproject.thrifthouse.api.response

import java.io.Serializable

data class BankResponse(
    val data: List<Bank>,
    val message: String,
    val statusCode: Int
): Serializable

data class Bank(
    val bankHolder: String,
    val bankLogo: String,
    val bankName: String,
    val bankNumber: String,
    val id: String
): Serializable