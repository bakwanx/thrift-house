package com.finalproject.thrifthouse.api.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class FavoriteResponse (
    val data: FavoriteResponseData,
    val statusCode: Long,
    val message: String
)

@Serializable
data class FavoriteResponseData (
    val totalItems: Long,
    val products: List<ListProduct>,
    val totalPages: Long,
    val currentPage: Long
)

@Serializable
data class FavoriteProductPostResponse (
    val data: FavoriteDataProductResponse,
    val message: String,
    val statusCode: Long
)

@Serializable
data class FavoriteDataProductResponse (
    @SerialName("productId")
    val productID: String
)

class FavoriteProduct(
    val productId: String
)