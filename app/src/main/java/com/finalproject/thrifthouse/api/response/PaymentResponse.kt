package com.finalproject.thrifthouse.api.response

data class PaymentResponse (
    val data: Order,
    val statusCode: Long,
    val message: String
)

data class Order (
    val order_id: String,
    val user_id: String,
    val store_id: String,
    val orderCode: String,
    val bank: BankPayment,
    val status: String,
    val packagingStatus: String,
    val expiredAt: String,
    val productsPrice: Long,
    val deliveryService: String,
    val shippingCost: Long,
    val shippingService: String,
    val estimatedTimeOfDeparture: String,
    val receipt: String,
    val productIds: List<String>
)

data class BankPayment (
    val id: String,
    val bankName: String,
    val bankNumber: String,
    val bankHolder: String,
    val bankLogo: String
)
