package com.finalproject.thrifthouse.api

import com.finalproject.thrifthouse.api.response.*
import com.finalproject.thrifthouse.model.PostCheckOutModel
import com.finalproject.thrifthouse.utilities.GlobalConfig
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiService {
    @GET("api/v1/products/")
    suspend fun getProducts(
        @Query("category") category: String?,
        @Query("subcategory1") subCategory1: String?,
        @Query("subcategory2") subCategory2: String?,
        @Query("search") search:String?,
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("sizeProduct") sizeProduct: String?,
        @Query("condition") condition: String?,
        @Query("sold") isSold: Boolean,
        @Query("sortBy") sortBy: String?
    ):Response<ProductResponse>

    @GET("api/v1/products/{id}")
    suspend fun getProductDetail(
        @Path("id") id: String
    ):Response<ProductDetailResponse>

    @GET("api/v1/stores/{store_id}/products")
    suspend fun getStoreProducts(
        @Path("store_id") storeId: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ):Response<StoreProductResponse>

    @GET("api/v1/stores/{store_id}/products")
    suspend fun getStoreProductsCategory(
        @Path("store_id") storeId: String,
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("category") category: String
    ):Response<StoreProductResponse>


    @GET("api/v1/stores/{id}/banner")
    suspend fun getStoreBanner(
        @Path("id") storeId: String,
    ):Response<BannerResponse>

    @GET("api/v1/stores/{id}/informations")
    suspend fun getDetailStore(
        @Path("id") storeId: String
    ):Response<StoreResponse>

    @POST("login")
    suspend fun login(
        @Body userLogin: UserLogin
    ):Response<UserResponse>

    @POST("register")
    suspend fun register(
        @Body userRegister: UserRegister
    ):Response<Void>

    @GET("api/v1/stores/{storeId}/reviews")
    suspend fun getAllReviewStore(
        @Path("storeId") storeId: String,
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("rate") rate: Int?,
        @Query("hasPhoto") hasPhoto: Boolean
    ):Response<ReviewResponse>

    @POST("api/v1/users/{userId}/products/favorites")
    suspend fun postFavoriteProduct(
        @Path("userId") userId: String,
        @Body favoriteProduct: FavoriteProduct
    ):Response<FavoriteProductPostResponse>

    @GET("api/v1/stores/{id}/about")
    suspend fun getAboutStore(
        @Path("id") storeId: String,
    ):Response<AboutStoreResponse>

    @GET("api/v1/users/{userId}/products/favorites")
    suspend fun getFavoriteProduct(
        @Path("userId") storeId: String,
        @Query("search") category: String?,
//        @Query("page") page: Int?,
//        @Query("size") size: Int?,
    ):Response<FavoriteResponse>

    @POST("api/v1/users/{userId}/stores/favorites")
    suspend fun postFavoriteStore(
        @Path("userId") userId: String,
        @Body favoriteStore: FavoriteStore
    ):Response<FavoriteStorePostResponse>

    @GET("api/v1/users/{userId}/stores/favorites")
    suspend fun getFavoriteStore(
        @Path("userId") storeId: String,
        @Query("search") category: String?,
    ):Response<FavoriteStoreGetResponse>

    @GET("api/v1/user/{id}")
    suspend fun getUserDetail(
        @Path("id") userId: String,
    ):Response<GetUserResponse>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("api/v1/users/{id}/cart")
    suspend fun getListCart(
        @Path("id") userId: String,
        @Header("Authorization") token: String
    ):Response<CartResponse>

    @POST("api/v1/users/{id}/cart")
    suspend fun postCart(
        @Path("id") userId: String,
        @Header("Authorization") token: String,
        @Body postProductCart: PostProductCart
    ):Response<Void>

    @HTTP(method = "DELETE", path = "api/v1/users/{id}/cart", hasBody = true)
    suspend fun deleteCart(
        @Path("id") userId: String,
        @Header("Authorization") token: String,
        @Body deleteProductCart: DeleteProductCart
    ):Response<Void>


    @GET("api/v1/rajaongkir/province")
    suspend fun getListProvince():Response<ProvinceRajaOnkgirResponse>

    @GET("api/v1/rajaongkir/city")
    suspend fun getListCity(
        @Query("province") province: String,
    ):Response<CityRajaOnkgirResponse>

    @POST("api/v1/users/{userId}/addresses")
    suspend fun postAddress(
        @Path("userId") userId: String,
        @Body postAddress: PostAddress
    ):Response<Void>

    @GET("api/v1/users/{userId}/addresses")
    suspend fun getListAddress(
        @Path("userId") userId: String,
    ):Response<AddressResponse>

    @GET("api/v1/bank")
    suspend fun getBank():Response<BankResponse>

    @GET("api/v1/stores/{id}/delivery-service")
    suspend fun getDeliveryService(
        @Path("id") storeId: String,
    ):Response<StoreDeliveryServiceResponse>

    @POST("api/v1/rajaongkir/cost")
    suspend fun postCostDeliveryService(
        @Body postToGetDeliveryService: PostToGetDeliveryService
    ):Response<DeliveryServiceResponse>

    @POST("api/v1/orders")
    suspend fun postOrder(
        @Body postCheckOutModel: PostCheckOutModel
    ):Response<PostOrderResponse>

    @GET("api/v1/orders/{id}/payment")
    suspend fun getPayment(
        @Path("id") orderId: String,
    ):Response<PaymentResponse>

    @GET("api/v1/users/{userId}/orders")
    suspend fun getOrderList(
        @Path("userId") userId: String,
        @Query("status") status: String,
    ):Response<OrderListResponse>

    @Multipart
    @PUT("api/v1/orders/{id}/payment/receipt")
    suspend fun putReceipt(
        @Path("id") orderId: String,
        @Part photo: MultipartBody.Part,
    ):Response<PutOrderResponse>

    @GET("api/v1/users/{userId}/notifications")
    suspend fun getNotification(
        @Path("userId") userId: String,
        @Query("type") type: String?,
    ):Response<NotificationResponse>

    @GET("api/v1/users/{userId}/orders/{orderId}")
    suspend fun getDetailOrder(
        @Path("userId") userId: String,
        @Path("orderId") orderId: String,
        @Query("status") status: String?,
    ):Response<DetailTransactionResponse>

    @PUT("api/v1/users/{userId}/orders/{transaction_id}")
    suspend fun acceptOrder(
        @Path("userId") userId: String,
        @Path("transaction_id") transactionId: String,
    ):Response<Void>

    @Multipart
    @POST("/api/v1/stores/{storeId}/reviews")
    suspend fun postReview(
        @Path("storeId") storeId: String,
        @Part("anonim") anonim: RequestBody,
        @Part("description") description: RequestBody,
        @Part("rating") rating: RequestBody,
        @Part("userId") userId: RequestBody,
        @Part photo: MultipartBody.Part,
    ):Response<Void>

}