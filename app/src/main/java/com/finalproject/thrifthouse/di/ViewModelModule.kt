package com.finalproject.thrifthouse.di

import com.finalproject.thrifthouse.utilities.Prefs
import com.finalproject.thrifthouse.viewmodel.*
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ProductFragmentViewModel(get(), get()) }
    viewModel {
        DetailProductActivityViewModel(
            get(),
            get(),
            Prefs.getSharedPrefs(androidApplication())
        )
    }
    viewModel { LoginActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { RegisterActivityViewModel(get()) }
    viewModel { ProductActivityViewModel(get(), get()) }
    viewModel { DetailStoreActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { ProductStoreFragmentViewModel(get(), get()) }
    viewModel { MaleStoreFragmentViewModel(get(), get()) }
    viewModel { FemaleStoreFragmentViewModel(get(), get()) }
    viewModel { ChildrenStoreFragmentViewModel(get(), get()) }
    viewModel { ReviewStoreFragmentViewModel(get()) }
    viewModel { FavoriteViewModel(get(), Prefs.getSharedPrefs(androidApplication()), get()) }
    viewModel { AboutStoreFragmentViewModel(get()) }
    viewModel {
        ProductFavoriteFragmentViewModel(
            get(),
            Prefs.getSharedPrefs(androidApplication()),
            get()
        )
    }
    viewModel { StoreFavoriteFragmentViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { HomeFragmentViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { CartActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { AddressViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { AddAddressActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { AddressActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel {
        CheckoutActivityViewModel(
            get(),
            get(),
            get(),
            get(),
            Prefs.getSharedPrefs(androidApplication())
        )
    }
    viewModel { AccountFragmentViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { BiodataActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { PaymentActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { OrderFragmentViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { NotificationViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { DetailTransactionActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
    viewModel { ProductCategoryActivityViewModel(get(), get()) }
    viewModel { ReviewActivityViewModel(get(), Prefs.getSharedPrefs(androidApplication())) }
}