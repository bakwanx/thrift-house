package com.finalproject.thrifthouse.di

import androidx.room.Room
import com.finalproject.thrifthouse.room.FavoriteProductDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

//import org.koin.dsl.module
//import org.koin.dsl.module.module
//

val localDBModule = module {

    single {
        Room.databaseBuilder(
            androidApplication(),
            FavoriteProductDatabase::class.java,
            "FavoriteProduct"
        ).build()
    }

    factory {
        val database = get<FavoriteProductDatabase>()
        database.favoriteDao()
    }

}
