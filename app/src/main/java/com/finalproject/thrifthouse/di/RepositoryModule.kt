package com.finalproject.thrifthouse.di

import com.finalproject.thrifthouse.repository.*
import com.finalproject.thrifthouse.utilities.Prefs
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val repositoryModule = module {
    single { ProductRepository(get()) }
    single { UserRepository(get()) }
    single { StoreRepository(get()) }
    single { AddressRepository(get()) }
    single { BankRepository(get()) }
    single { DeliveryServiceRepository(get()) }
    single { OrderRepository(get()) }
    single { NotificationRepository(get()) }
}