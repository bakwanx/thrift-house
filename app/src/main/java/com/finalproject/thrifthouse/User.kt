package com.finalproject.thrifthouse

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserRegister(
    var username: String?,
    var email: String?,
    var phoneNumber: String?,
    var password: String?,
): Parcelable

@Parcelize
data class UserLogin(
    var username: String?,
    var password: String?
): Parcelable

