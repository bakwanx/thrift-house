package com.finalproject.thrifthouse

import android.app.Application
import androidx.viewbinding.BuildConfig
import com.finalproject.thrifthouse.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApp: Application() {
    companion object{
        lateinit var instance: MyApp
            private set
    }
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@MyApp)
            modules(listOf(
                //databaseModule,
                localDBModule,
                viewModelModule,
                apiModule,
                repositoryModule,
                retrofitModule,

            ))

        }
        instance = this
    }
}